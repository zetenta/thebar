(function ($) {

    var tooltipTexts = ['I hate it', 'I don\'t like it', 'It\'s ok', 'I like it', 'I love it'];
    var haventVotedText = 'You haven\'t voted yet';
    var spinnerOptions = {color: '#000000', position: 'relative'};
    var dataFilled = 'glyphicon glyphicon-star glyph-goldenrod';
    var dataEmpty = 'glyphicon glyphicon-star-empty glyph-black';
    var hoverColor = 'olive';
    var msgVotes = "Votes";
    var msgAverage = "out of 5 stars";

    $.fn.toRating = function (options, caller) {
        var parentDiv = this;
        var divId = this.attr('id');

        if (options.spinnerOptions) {
            spinnerOptions = options.spinnerOptions;
        }
        if (options.tooltipTexts) {
            tooltipTexts = options.tooltipTexts;
        }
        if (options.dataFilled) {
            dataFilled = options.dataFilled;
        }
        if (options.dataEmpty) {
            dataEmpty = options.dataEmpty;
        }
        if (options.hoverColor) {
            hoverColor = options.hoverColor;
        }
        if (options.msgVotes) {
            msgVotes = options.msgVotes;
        }
        if (options.msgAverage) {
            msgAverage = options.msgAverage;
        }
        if (options.haventVotedText) {
            haventVotedText = options.haventVotedText;
        }

        var spinner = new Spinner(spinnerOptions).spin(document.getElementById(divId));
        var baseUrl = 'https://api.diageoapi.com/ratings/';
        var ratingUrl = baseUrl + options.contentId + '?appid=' + options.appId;
        var ajaxRequest = {url: ratingUrl, xhrFields: {withCredentials: true}, crossDomain:true};
        $.ajax(ajaxRequest)
            .done(function (ratingResponse) {
                var totalRatingsGetResponse = ratingResponse['totalratings'] ? ratingResponse['totalratings'] : '0';
                parentDiv.css('margin-top', 5);
                parentDiv.css('margin-bottom', 5);
                parentDiv.append('<div class="row row-eq-height"><div id="starsDiv" class="col-xs-12" style="width: 50%; margin: 0 auto; text-align: center;"><input type="hidden" id="rating_' + divId + '" value="' + ratingResponse['avgRating'] + '" data-filled="' + dataFilled + '" data-empty="' + dataEmpty + '" style="display: inline-block; padding: 5px;"><span style="display: inline-block; padding: 5px;" id="totalratings_' + divId + '"><b>' + totalRatingsGetResponse + ' ' + msgVotes + '</b></span></div></div>');
                parentDiv.append('<div id="avgDiv" class="row row-eq-height" style="margin-top: 10px"><div class="col-xs-12 text-center"><b id="avgRating_' + divId + '">' + ratingResponse['avgRating'] + msgAverage + '</b></div></div>');
                parentDiv.append('<div id="yourRatingDiv" class="row row-eq-height"><div class="col-xs-12 text-center"><b id="yourRating_' + divId + '">Your rating is ' + ratingResponse['yourRating'] + '</b></div></div>');

                var ratingInput = $('#rating_' + divId);


                var avgRatingB = $('#avgRating_' + divId);
                var totalRatingsSpan = $('#totalratings_' + divId);
                var yourRatingB = $('#yourRating_' + divId);

                if (ratingResponse['yourRating'] == 0) {
                    yourRatingB.html(haventVotedText);
                } else {
                    yourRatingB.html('Your rating is ' + ratingResponse['yourRating']);
                }

                if (options.showAverage === false) {
                    avgRatingB.hide();
                }
                if (options.showVotes === false) {
                    totalRatingsSpan.hide();
                }
                if (options.showStars === false) {
                    ratingInput.css('disabled', true);
                } else {
                    spinner.stop();
                    caller.show();
                    ratingInput.rating({
                        start: 0,
                        stop: 5,
                        extendSymbol: function () {
                            var title;
                            $(this).tooltip({
                                container: 'body',
                                placement: 'top',
                                trigger: 'manual',
                                title: function () {
                                    return title;
                                }
                            });
                            $(this).on('rating.rateenter', function (e, rate) {
                                title = getTooltipText(rate);
                                $(this).tooltip('show');
                            }).on('rating.rateleave', function () {
                                $(this).tooltip('hide');
                            });
                        }
                    }).on('change', function () {
                        var newRating = $(this).val();
                        if (newRating > 0) {
                            spinner.spin(document.getElementById(divId));

                            var rateUrl = baseUrl + options.contentId + '/' + newRating + '?appid=' + options.appId;
                            var ajaxRequest = {url: rateUrl, xhrFields: {withCredentials: true}, crossDomain:true};
                            $.post(ajaxRequest)
                                .done(function (rateResponse) {
                                    avgRatingB.html(rateResponse['avgRating'] + ' ' + msgAverage);
                                    totalRatingsSpan.html('<b>' + rateResponse['totalRatings'] + '</b>');
                                    yourRatingB.html('Your rating is ' + rateResponse['yourRating']);
                                    $('.tooltip').hide();
                                    ratingInput.rating('rate', rateResponse['avgRating']);
                                    //ratingInput.data('readonly', true);
                                    $('#' + divId + ' .glyphicon-star').css('color', '');
                                    $('#' + divId + ' div .rating-symbol-foreground').unbind('mouseenter mouseleave')
                                    $('#' + divId + ' .glyphicon-star').css('color', '');
                                }).fail(function () {
                                console.log('Error requesting ' + rateUrl);
                            }).always(function () {
                                spinner.stop();
                            });
                        }
                    });
                    ratingInput.rating('rate', ratingResponse['avgRating']);

                    $('div .rating-symbol-foreground').hover(
                        function () {
                            $('#' + divId + ' .glyphicon-star').css('color', hoverColor);
                        }, function () {
                            $('#' + divId + ' .glyphicon-star').css('color', '');
                        }
                    );
                }

            }).fail(function () {
            console.log('Error requesting ' + ratingUrl);
        }).always(function () {
            spinner.stop();
            caller.show();
        });
        return this;
    }

    function getTooltipText(rate) {
        return tooltipTexts[rate - 1];
    }

}(jQuery));