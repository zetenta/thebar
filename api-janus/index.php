<?php
error_reporting(0);
#clean config.js and make it object
//$needle = array('config = ', '};', ' }', '{ ');
//$replace = array('', '}', '}', '{');
//$config = json_decode(str_replace($needle, $replace, file_get_contents(dirname(dirname(__FILE__)) . '/config.js')));
//die(var_dump($config->canonicalHosts));

$users = false;
$userProfile = false;
$isCached = false;
$addToCache = true;
$cacheFolder = dirname(__FILE__) . "/cache/json";
// HH MM SS DD
$cacheTTL = 24 * 60 * 60 * 2;

// API Authentication Values
$apiDomain = "https://recipe.diageoapi.com/";

// Remove the following folder from the slug, so we
// know the actual method name
$localFolder = "api-janus/";

// Get the URL
$slug = $_SERVER["REQUEST_URI"];

$slug = str_replace($localFolder, "", $slug);

// Does it need to refrsh ?
if (substr($slug, -7, 7) == 'refresh') {
    $cacheTTL = 0;
    $slug = substr($slug, 0, strlen($slug) - 8);
}

// According to slug, I may need to add a querystring
$querystring = "";

// Define the API Call

$apiParameters = 'v1';
if ($slug == "/") {
    $cacheFile = "index.txt";
} else {
    $apiParameters .= $slug;
    $cacheFile = $slug . ".txt";
}
// Checks Cache
$filename = $cacheFolder . str_replace(array(' ', ':', '/', '?', '='), '-', $cacheFile);

if (file_exists($filename)) {
    $fileDate = filemtime($filename);

    date_default_timezone_set('Europe/Berlin');
    $now = strtotime("now");
//    file_put_contents('access.log', 'from cache: '. $filename . PHP_EOL, FILE_APPEND);
    $lived = $now - $fileDate;
    if ($lived < $cacheTTL) {
        $isCached = true;
        $apiReturn = loadCache($filename);
        $addToCache = false;
    }

}

// If NOT cached, I'll cache the results
if (!$isCached) {
    // Initiate CURL
    $url = $apiDomain . $apiParameters . $querystring;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $apiReturn = curl_exec($ch);
    $error = curl_error($ch);
    $info = curl_getinfo($ch);
   //echo $url;
//    die(var_dump($error));
    curl_close($ch);
    
       
    if ($info["http_code"] == 500) {
        $apiReturn = loadCache($filename);
        $addToCache = false;
    } else {
        // Saves the API Response into the cacheing system

    }

}


// Is the response empty? If so, it is a 404
if ($apiReturn == "[]") {
    header("HTTP/1.0 404 Not Found");
    echo "<h1>404 Page Not Found</h1>";
    $addToCache = false;
    die();
}

// Save it to Cache?
if ($addToCache) {
    addToCache($filename, $apiReturn);
}

// Parses JSON and creates output
header('Content-Type: application/json');

echo $apiReturn;

function loadCache($fn)
{
    if (file_exists($fn)) {
        $apiCache = fopen($fn, "r") or die("Unable to open file!");
        $fs = filesize($fn);
        if ($fs == 0) return "";
        $apiReturn = fread($apiCache, $fs);
        fclose($apiCache);
        return $apiReturn;
    }
    return "";
}

function addToCache($fn, $text)
{
    //$fn = str_replace('/','\\',$fn);
    $apiCache = fopen($fn, "w");
    fwrite($apiCache, $text);
    fclose($apiCache);
}

function getUrl($url) {
  $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $apiReturn = curl_exec($ch);
    $error = curl_error($ch);
    $info = curl_getinfo($ch);
//    echo $apiReturn;
//    die(var_dump($error));
    curl_close($ch);

    return $apiReturn;
}

?>