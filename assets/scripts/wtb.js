(function ($) {

    // Initialization
    var settings = {
        appid: "thebar-us",
        diageo_api_url: "https://api.diageoapi.com/wtb/",
        locale: "EN-US",
        defaultLat: 0,
        defaultLon: 0,
        defaultZoom: 8,
        refreshOnDrag: false,
        animation: false,
        updateOnAddressChange: true,
        locations: {}
    };

    var brands = [{
        brand: "",
        brandSlug: ""
    }];

    var variants = [{
        variant: "",
        variantSlug: ""
    }];

    var lastSearch;

    var map;
    var markers = [];
    var infoWindows = [];

    // Initialize
    $.fn.wtbInitialize = function (par) {

        return this.each(function () {
            //settings = par;
            settings.appid = (par.appid != undefined) ? par.appid : "";
            settings.locale = (par.locale != undefined) ? par.locale : "";
            settings.defaultLat = (par.defaultLat != undefined) ? par.defaultLat : 40.7554058;
            settings.defaultLon = (par.defaultLon != undefined) ? par.defaultLon : -73.9816544;
            settings.defaultZoom = (par.defaultZoom != undefined) ? par.defaultZoom : 15;
            settings.appid = (par.appid != undefined) ? par.appid : "";
            settings.eComm = (par.eComm != undefined) ? par.eComm : false;
            settings.refreshOnDrag = (par.refreshOnDrag != undefined) ? par.refreshOnDrag : true;
            settings.animation = (par.animation != undefined) ? par.animation : false;
            settings.markerURL = (par.markerURL != undefined) ? par.markerURL : '';
            settings.markerActiveURL = (par.markerActiveURL != undefined) ? par.markerActiveURL : '';
            settings.updateOnAddressChange = (par.updateOnAddressChange != undefined) ? par.updateOnAddressChange : true;
            settings.numbers = (par.numbers != undefined) ? par.numbers : false;

            settings.locations = par.locations;

            var mapOptions = {
                center: {
                    lat: settings.defaultLat,
                    lng: settings.defaultLon
                },
                zoom: settings.defaultZoom,
                scrollwheel: false,
                styles: [{
                    "featureType": "administrative",
                    "elementType": "labels.text.fill",
                    "stylers": [{"color": "#444444"}]
                }, {
                    "featureType": "landscape",
                    "elementType": "all",
                    "stylers": [{"color": "#f2f2f2"}]
                }, {
                    "featureType": "poi",
                    "elementType": "all",
                    "stylers": [{"visibility": "off"}]
                }, {
                    "featureType": "road",
                    "elementType": "all",
                    "stylers": [{"saturation": -100}, {"lightness": 45}]
                }, {
                    "featureType": "road.highway",
                    "elementType": "all",
                    "stylers": [{"visibility": "simplified"}]
                }, {
                    "featureType": "road.arterial",
                    "elementType": "labels.icon",
                    "stylers": [{"visibility": "off"}]
                }, {
                    "featureType": "transit",
                    "elementType": "all",
                    "stylers": [{"visibility": "off"}]
                }, {
                    "featureType": "water",
                    "elementType": "all",
                    "stylers": [{"color": "#c1c8d5"}, {"visibility": "on"}]
                }]
            };

            // Initializes Google Maps
            // Create map object
            map = new google.maps.Map(document.getElementById('map'), mapOptions);

            // Listens the map's drag function
            google.maps.event.addListener(map, 'dragend', function () {
                if ((settings.refreshOnDrag)) {
                    //console.log("Got here")
                    var center = map.getCenter();
                    $(settings.locations.locationsList).readFromAPI();
                    //lastSearch.lat = center.lat();
                    //lastSearch.lon = center.lng();

                    //$(lastSearch.object).getLocationsFromAPI(lastSearch);
                }
            });

            // Checks if geolocalization is available
            if ((settings.defaultLat != 0) && (settings.defaultLon != 0)) {
                handleNoGeolocation(true);
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function (position) {
                        var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                        fromlat = position.coords.latitude;
                        fromlong = position.coords.longitude;
                        map.setCenter(pos);
                    }, function () {
                        handleNoGeolocation(true);
                    });
                } else {
                    // Browser doesn't support Geolocation
                    handleNoGeolocation(false);
                }
            }

            // Initalizes Google Search
            var AutoComplete = {
                init: function () {
                    /*--
                     Autocomplete Functionality
                     --*/
                    var input = document.getElementById('searchTextField');
                    var autocomplete = new google.maps.places.Autocomplete(input);
                    autocomplete.bindTo('bounds', map);
                    //var infowindow = new google.maps.InfoWindow();
                    //Add listener on place_changed event
                    google.maps.event.addListener(autocomplete, 'place_changed', function () {
                        //Save the user selection
                        var placeSelected = $('#searchTextField').val();
                        // Initially the filter search boxes are disabled
                        var locationsField = $('#searchTextField'),
                            locationsFound = $('#locationsFound h3');
                        locationsFound.fadeOut();
                        $('#loading').fadeIn();
                        var geocoder = new google.maps.Geocoder();
                        geocoder.geocode({
                            'address': locationsField.val()
                        }, function (results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                var location = results[0].geometry.location;
                                var pos = new google.maps.LatLng(location.lat(), location.lng());
                                map.setCenter(pos);
                                fromlat = location.lat()
                                fromlong = location.lng()
                                /*   $(settings.locations.locationsList).readFromAPI();*/
                            } else {
                                locationsFound.html(status).fadeIn();
                                $('#loading, #locationsResults, .scrollbar').fadeOut();
                                Map.resetMap();
                                /*$(settings.locations.locationsList).readFromAPI();*/
                            }
                        });
                        var place = autocomplete.getPlace();
                        /*  if (settings.updateOnAddressChange) {
                         $(settings.locations.locationsList).readFromAPI();
                         }*/
                    });
                }
            };
            AutoComplete.init();

            function handleNoGeolocation(errorFlag) {
                if (errorFlag) {
                    jQuery.ajax({
                        url: 'https://freegeoip.net/json/',
                        success: function (response) {
                            // Tries to get a lat/lon for the IP address
                            //console.log('Got default lat/lon')
                            //console.log(response)
                            settings.defaultLat = parseFloat(response.latitude)
                            settings.defaultLon = parseFloat(response.longitude)

                            map = new google.maps.Map(document.getElementById('map'), mapOptions);

                            google.maps.event.addListener(map, 'dragend', function () {
                                if ((settings.refreshOnDrag)) {
                                    var center = map.getCenter();
                                    $(settings.locations.locationsList).readFromAPI();
                                }
                            });
                        }
                    });
                } else {
                    var content = 'Error: Your browser doesn\'t support geolocation.';
                }
                var options = {
                    map: map,
                    position: new google.maps.LatLng(60, 105),
                    content: content
                };
                var infowindow = new google.maps.InfoWindow(options);
                map.setCenter(options.position);
            }


        });

    };

    // BRANDS related methos

    $.fn.setBrands = function (par) {
        // sets up the brands JSON
        brands = par;
    };

    $.fn.getBrands = function () {
        // Creates the select options based on the brands JSON (local)
        return this.each(function () {
            var ret = "";
            brands.forEach(function (item) {
                ret += "<option value=" + item.brandSlug + ">" + item.brand + "";

            });
            $(this).html(ret);
        });

    };

    $.fn.getBrandsFromAPI = function (par) {
        // Calls the API and returns
        // Parameters are:
        //      loader: the id of the DOM object that has the loader spin

        return this.each(function () {
            // Initializes the API call
            var callParameters = {
                method: 'brands',
                loader: par.loader
            };

            // Identifies the DOM objects
            var loader = par.loader; // Loader associated to the event
            var object = "#" + $(this).attr('id'); // Object to fill with the options

            // Composes the api call
            var url = createCall(callParameters);

            // shows the loader
            $(loader).css('visibility', 'visible');
            jQuery.ajax({
                url: url,
                success: function (response) {
                    var ret = '';
                    var ret = "<option value=''>Please Choose A Brand</option>";
                    // Processes the API Response
                    tmpBrandSlug = $('#tmpBrand').text();
                    response.forEach(function (item) {
                        var selected = '';
                        if (tmpBrandSlug == item.brandSlug) {
                            selected = 'selected';
                        }
                        ret += "<option value=" + item.brandSlug + " " + selected + ">" + item.brandName + "</option>";
                    });
                    // Aplies the brands to the destination object
                    $(object).html(ret);
                    $(object + ' option:eq(0)').prop('selected', true)
                    $('#selectBrands').trigger('change');
                    // Hides the loader gif
                    $(loader).css('visibility', 'hidden');
                }
            });

        });

    };


    // VARIANTS related methods

    $.fn.setVariants = function (par) {
        // sets up the brands JSON
        variants = par;
    };

    $.fn.getVariants = function () {
        // Creates the select options based on the brands JSON (local)
        return this.each(function () {
            var ret = "";
            var i = 0;
            var slug;
            variants.forEach(function (item) {
                ret += "<option value=" + item.variantSlug + ">" + item.variant + "";
                if (i == 0) {
                    slug = item.variantSlug;
                }
                i++;
            });
            $(settings.locations.locationsList).readFromAPI({variant: slug});
            $(this).html(ret);
        });

    };

    $.fn.getVariantsFromAPI = function (par) {
        // Calls the API and returns
        // Parameters are:
        //      loader: the id of the DOM object that has the loader spin
        //      brand: sapL3 of the brand

        return this.each(function () {
            // Initializes the API call
            var callParameters = {
                method: 'variants',
                loader: par.loader,
                brand: par.brand
            };
            if (par.brand != null) {
                // Identifies the DOM objects
                var loader = par.loader; // Loader associated to the event
                var object = "#" + $(this).attr('id'); // Object to fill with the options

                // Composes the api call
                var url = createCall(callParameters);
                // shows the loader
                $(loader).css('visibility', 'visible');
                jQuery.ajax({
                    url: url,
                    success: function (response) {

                        var ret = "<option value=''>Please Choose A Variant</option>";
                        // Processes the API Response
                        response.forEach(function (item) {
                            ret += "<option value=" + item.variantSlug + ">" + item.variantName + "</option>";
                        });
                        // Aplies the brands to the destination object
                        $(object).html(ret);
                        // Hides the loader gif
                        $(loader).css('visibility', 'hidden');
                    }
                });
            }

        });
    };

    $.fn.readFromAPI = function (par) {
        return this.each(function () {
            var query = settings.locations;
            if (par != undefined) {
                if (par.distance != undefined) query.distance = par.distance;
                if (par.loctype != undefined) query.loctype = par.loctype;
                if (par.variant != undefined) query.variant = par.variant;
            }
            $(this).getLocationsFromAPI(query);
        });
    };

    // LOCATIONS related methods

    // GOOGLE MAPS Functions

    //Sets the map on all markers in the array.


    function setMapOnAll(map) {

        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }

        /*  map.fitBounds(bounds);*/
        markers = [];
        infoWindows = [];
    }

    $.fn.openInfoWindow = function () {
        return this.each(function () {
            id = $(this).attr('id');
            markers.forEach(function (item) {
                infoWindows[item.id].close();
                item.setIcon(settings.markerURL)
            });
            infoWindows[id].open(map, markers[id]);
            markers[id].setIcon(settings.markerActiveURL)
        });
    };

    $.fn.getLocationsFromAPI = function (par) {
        // Calls the API and returns
        // Parameters are:
        //      loader: the id of the DOM object that has the loader spin
        //      brand: sapL3 of the brand
        // variant = [];
        return this.each(function () {
            // Initializes the API call
            //console.log(par.variant);
            if ((par.variant == "") || (par.variant == undefined)) {
                par.variant = settings.variant;
                $('.btn.wtb-button').addClass('inactive');
                // console.log(par.variant+'aaaaaaaaaaaaaaaaaa');
            } else {
                $('.btn.wtb-button').removeClass('inactive');
            }

            // console.log(par.variant+'-------------');

            var callParameters = {
                method: 'locations',
                loader: par.loader,
                variant: par.variant,
                lat: map.getCenter().lat(),
                lon: map.getCenter().lng(),
                distance: par.distance,
                uom: par.uom,
                loctype: par.loctype,
                object: par.object
            };

            // update settings so system remembers the last query parameters

            settings.distance = par.distance;
            settings.variant = par.variant;
            settings.loctype = par.loctype;

            lastSearch = callParameters;

            // Identifies the DOM objects
            var loader = par.loader; // Loader associated to the event
            var object = '';
            var timeOut = 0;
            var animation;


            if (settings.animation) {
                timeOut = 10;
                animation = google.maps.Animation.DROP;
            }

            if (par.object == undefined) {
                object = "#" + $(this).attr('id'); // Object to fill with the options
                lastSearch.object = object;
            } else {
                object = par.object;
            }


            // Clean the map
            map.panTo({
                lat: map.getCenter().lat(),
                lng: map.getCenter().lng()
            });

            // Composes the api call
            var url = createCall(callParameters);

            // shows the loader
            $(loader).css('visibility', 'visible');
            // hides the text
            $(object).css('visibility', 'hidden');

            // define loclenght

            var locLenght = [];

            jQuery.ajax({
                url: url,
                success: function (response) {

                    var ret = "";
                    var retEcomm = "";

                    // Initializes the map
                    var marker;
                    var bounds = new google.maps.LatLngBounds();

                    setMapOnAll(null);

                    var i = 0;
                    // Processes the API Response
                    response.forEach(function (item) {
                        // Is it an eComm link?
                        var isLocation = (item._id != undefined) ? true : false;

                        if (isLocation) {

                            var phonenumberformatted = item.phone.substr(0, 3) + '-' + item.phone.substr(3, 3) + '-' + item.phone.substr(6, 4)
                            if (phonenumberformatted == '--') phonenumberformatted = ''
                            var locationTag = "";
                            var infoTag = "";

                            click = '$(this).openInfoWindow(); ' +
                                '$(".locationItem").removeClass("active"); ' +
                                '$(this).toggleClass("active")';
                            locationTag += "<dl class='locationItem' id='" + i + "' onclick='" + click + ";' style='cursor:pointer'>";

                            var numbers;
                            numbers = (settings.numbers) ? (i + 1) + ". " : "";

                            locationTag += "<span class='number-map'>" + numbers + "</span>";
                            locationTag += "<div class='locationInfo'>";
                            locationTag += "<h4 class='locationName'>"
                                + item.locationName + "</h4>";
                            locationTag += "<dd class='locationAddress'>" + item.address + "</dd>";
                            locationTag += "<dd class='locationCity'>" + item.city + ", " + item.state + " " + item.zip + "</dd><br>";
                            /*  locationTag += "<span class='locationZip'>" + item.zip + "</span>";*/
                            locationTag += "<dd class='locationPhone'><a href='tel:" + phonenumberformatted + "' target='_blank'>" + phonenumberformatted + "</a></dd>";
                            locationTag += "</dl>";
                            locationTag += "</div>";

                            infoTag += "<dl class='locationItem-info' id='" + i + "' onclick='" + click + ";' style='cursor:pointer'>";
                            infoTag += "<dd class=locationName'>" + numbers
                                + item.locationName + "</dd>";
                            infoTag += "<dd class='locationAddress'>" + item.address + "</dd>";
                            infoTag += "<dd class='locationCity'>" + item.city + ", " + item.state + " " + item.zip + "</dd>";
                            /*  infoTag += "<span class='locationZip'>" + item.zip + "</span>";*/
                            infoTag += "<dd class='locationPhone'><a href='tel:" + phonenumberformatted + "' target='_blank'>" + phonenumberformatted + "</a></dd>";
                            infoTag += "</dl>";

                            ret += locationTag;

                            // get locations count and push it to array

                            $(locationTag).each(function (index) {
                                locLenght.push(index);
                            });

                            // add markers
                            // create info window

                            infoWindows.push(new google.maps.InfoWindow({
                                content: infoTag,
                                maxWidth: 200
                            }));

                            window.setTimeout(function () {
                                var numbers;
                                numbers = (settings.numbers) ? (markers.length + 1) : "";

                                markers.push(new google.maps.Marker({
                                    map: map,
                                    animation: animation,
                                    position: {
                                        lat: item.location.coordinates[1],
                                        lng: item.location.coordinates[0]
                                    },
                                    icon: settings.markerURL,
                                    label: {
                                        text: '' + numbers + '',
                                        color: 'white',
                                        fontFamily: 'elephant'
                                    },
                                    title: item.locationName,
                                    id: markers.length
                                }));

                                var myLatLng = new google.maps.LatLng(item.location.coordinates[1], item.location.coordinates[0]);

                                bounds.extend(myLatLng);
                                map.setCenter(myLatLng);
                                map.fitBounds(bounds);


                                markers[markers.length - 1].addListener('click', function () {
                                    markers.forEach(function (item) {
                                        infoWindows[item.id].close();
                                        item.setIcon(settings.markerURL)
                                    });
                                    infoWindows[this.id].open(map, this);
                                    this.setIcon(settings.markerActiveURL)
                                });

                            }, i * timeOut);
                            i++;

                        } else {
                            // if it is not a Location, it is an eComm
                            // it can either be Drizzly or Reserve Bar
                            // Drizzly
                            var ecomtype = 'generic';
                            var url = '';
                            var cta = '';
                            var logo = '';
                            if (item.drizly) {
                                ecomtype = 'drizly';
                                url = item.drizly.drizly_url;
                                cta = item.drizly.cta_text;
                                logo = item.drizly.logo_url
                            }
                            if (item.reserveBar) {
                                ecomtype = 'reserveBar';
                                url = item.reserveBar.reserveBar_url;
                                cta = item.reserveBar.cta_text;
                                logo = item.reserveBar.logo_url
                            }

                            var ecomItem = '<div class="eCommArea">';
                            ecomItem += "<div class='eCommButton'><a href='" + url + "' target='_blank' rel='nofollow' id='" + ecomtype + "'>"
                            ecomItem += "<span>" + cta + "</span></div>";
                            ecomItem += "<div class='eCommLogo'><div class='eCommImage' style='background-image: url(" + logo + ")'></div></div>";
                            ecomItem += "</a>";
                            ecomItem += '</div>';
                            retEcomm += ecomItem
                        }

                    });

                    // Aplies the brands to the destination object

                    $('.ecomm-area').html(retEcomm);

                    $('.wtb-button').on('click', function () {

                        $('#locations').show();

                        $('.no-result-map').hide();

                        $(object).css('visibility', 'visible');

                        $('.map-wrapper').addClass('col-xs-12 col-sm-12 col-md-12 col-lg-9');
                        // Hides the loader gif
                        $(loader).css('visibility', 'hidden');

                        /*console.log(locLenght.length);*/

                        if ($(object).hasClass('slick-initialized')) {
                            $(object).empty();
                            $(object).slick('unslick');
                        }

                        if (locLenght.length > 4) {
                            /*console.log('armar slider');*/
                            $(object).html(ret);
                            $('#locationsList').slick({
                                rows: 4, dots: true, slidesPerRow: 1, dotsClass: 'dotcustom'
                            });
                        }

                        else {
                            $(object).html(ret);
                        }


                        if (locLenght.length == 0) {
                            $('.no-result-map').show();
                        } else {
                            $('.no-result-map').hide();
                        }

                    });

                    $('#locations').show();

                    if (locLenght.length == 0) {
                        $('.no-result-map').show();
                    } else {
                        $('.no-result-map').hide();
                    }

                    $(object).css('visibility', 'visible');

                    $('.map-wrapper').addClass('col-xs-12 col-sm-12 col-md-12 col-lg-9');
                    // Hides the loader gif
                    $(loader).css('visibility', 'hidden');

                    /*console.log(locLenght.length);*/

                    if ($(object).hasClass('slick-initialized')) {
                        $(object).empty();
                        $(object).slick('unslick');
                    }

                    if (locLenght.length > 4) {
                        /*console.log('armar slider');*/
                        $(object).html(ret);
                        $('#locationsList').slick({
                            rows: 4, dots: true, slidesPerRow: 1, dotsClass: 'dotcustom'
                        });
                    }

                    else {
                        $(object).html(ret);
                    }

                }
            });

        });
    };

    // CREATE URL

    function createCall(par) {
        var url = settings.diageo_api_url;
        // add slash?

        switch (par.method) {
            case "brands":
                url += par.method + "/" + settings.locale + "/?appid=" + settings.appid;
                break;
            case "variants":
                if (par.brand) {
                    url += par.method + "/" + settings.locale + "/" + par.brand + "/?appid=" + settings.appid;
                } else {
                    url += par.method + "/" + settings.locale + "/?appid=" + settings.appid;
                }
                break;
            case "locations":
                url += settings.locale + "/" + par.variant + "/" + par.lat + "/" + par.lon + "/" + par.distance + "/" + par.uom + "?appid=" + settings.appid + "&loctype=" + par.loctype;
                if (settings.eComm) {
                    url += '&showonline=true'
                }
                break;
        }
        //console.log(url);
        return url;
    }


}(jQuery));
