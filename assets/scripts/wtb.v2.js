(function ($) {

    // Initialization
    var settings = {
        appid: "thebar-us",
        diageo_api_url: "https://api.diageoapi.com/wtb/v2/",
        locale: "EN-US",
        defaultLat: 0,
        defaultLon: 0,
        defaultZoom: 8,
        refreshOnDrag: false,
        animation: false,
        updateOnAddressChange: true,
        locations: {},
        showAddress: true
    };

    var brands = [{
        brand: "",
        brandSlug: ""
    }];

    var variants = [{
        variant: "",
        variantSlug: ""
    }];

    var lastSearch;

    var map;
    var markers = [];
    var infoWindows = [];

    // Initialize
    $.fn.wtbInitialize = function (par) {

        return this.each(function () {
            //settings = par;
            settings.appid = (par.appid != undefined) ? par.appid : "";
            settings.locale = (par.locale != undefined) ? par.locale : "";
            settings.defaultLat = (par.defaultLat != undefined) ? par.defaultLat : 40.7554058;
            settings.defaultLon = (par.defaultLon != undefined) ? par.defaultLon : -73.9816544;
            settings.defaultZoom = (par.defaultZoom != undefined) ? par.defaultZoom : 15;
            settings.appid = (par.appid != undefined) ? par.appid : "";
            settings.eComm = (par.eComm != undefined && par.eComm.enabled != undefined) ? par.eComm.enabled : false;
            settings.eCommContainer = (par.eComm != undefined && par.eComm.idEComm != undefined) ? par.eComm.idEComm : undefined;
            settings.refreshOnDrag = (par.refreshOnDrag != undefined) ? par.refreshOnDrag : true;
            settings.animation = (par.animation != undefined) ? par.animation : false;
            settings.markerURL = (par.markerURL != undefined) ? par.markerURL : '';
            settings.updateOnAddressChange = (par.updateOnAddressChange != undefined) ? par.updateOnAddressChange : true;
            settings.numbers = (par.numbers != undefined) ? par.numbers : false;
            settings.phoneFormat = (par.phoneFormat != undefined) ? par.phoneFormat : '###-###-####';

            settings.locations = par.locations;
            settings.locations.notFoundMessage = (settings.locations.notFoundMessage != undefined) ? settings.locations.notFoundMessage : "No locations were found for your location and product, please try extending the radius or change the location";

            // Initializes Google Maps
            // Create map object

            //

            var styles = [{
                stylers: [{
                    hue: "#bcc5d8"
                }, {
                    saturation: -80
                }]
            }, {
                featureType: "road",
                elementType: "geometry",
                stylers: [{
                    lightness: 100
                }, {
                    visibility: "simplified"
                }]
            }, {
                featureType: "road",
                elementType: "labels",
                stylers: [{
                    visibility: "off"
                }]
            }];

            // Create a new StyledMapType object, passing it the array of styles,
            // as well as the name to be displayed on the map type control.
            var styledMap = new google.maps.StyledMapType(styles, {
                name: "Styled Map"
            });

            //

            map = new google.maps.Map(document.getElementById('map'), {
                center: {
                    lat: settings.defaultLat,
                    lng: settings.defaultLon
                },
                mapTypeControlOptions: {
                    mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
                },
                zoom: settings.defaultZoom,
                scrollwheel: false
            });

            //Associate the styled map with the MapTypeId and set it to display.
            map.mapTypes.set('map_style', styledMap);
            map.setMapTypeId('map_style');

            // Listens the map's drag function
            google.maps.event.addListener(map, 'dragend', function () {
                if ((settings.refreshOnDrag)) {
                    //console.log("Got here")
                    var center = map.getCenter();
                    $(settings.locations.locationsList).readFromAPI();
                    //lastSearch.lat = center.lat();
                    //lastSearch.lon = center.lng();

                    //$(lastSearch.object).getLocationsFromAPI(lastSearch);
                }
            });

            // Checks if geolocalization is available
            if ((settings.defaultLat != 0) && (settings.defaultLon != 0)) {
                handleNoGeolocation(true);

                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function (position) {
                        var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                        fromlat = position.coords.latitude;
                        fromlong = position.coords.longitude;

                        //alert('123456789');

                        if (settings.showAddress == true) {
                            if (jQuery('#searchTextField').length) {
                                jQuery.ajax({
                                    url: 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + fromlat + ',' + fromlong + '&key=AIzaSyDFRoLKEKfJ0OLco54mRNL6KGjCrf9n_e8',
                                    success: function (response) {
                                        if (response && response['results'] && response['results'].length > 0) {
                                            var formattedAddress = response['results'][0]['formatted_address'];
                                            jQuery('#searchTextField').val(formattedAddress);
                                            $(settings.locations.locationsList).readFromAPI();
                                        }
                                    }
                                });
                            }
                        }

                        map.setCenter(pos);
                    }, function () {
                        handleNoGeolocation(true);
                    });
                } else {
                    // Browser doesn't support Geolocation
                    handleNoGeolocation(false);
                }
            }

            // Initalizes Google Search
            var AutoComplete = {
                init: function () {
                    /*--
                     Autocomplete Functionality
                     --*/
                    var input = document.getElementById('searchTextField');
                    var autocomplete = new google.maps.places.Autocomplete(input);
                    autocomplete.bindTo('bounds', map);
                    //var infowindow = new google.maps.InfoWindow();
                    //Add listener on place_changed event
                    google.maps.event.addListener(autocomplete, 'place_changed', function () {
                        //Save the user selection
                        var placeSelected = $('#searchTextField').val();
                        // Initially the filter search boxes are disabled
                        var locationsField = $('#searchTextField'),
                            locationsFound = $('#locationsFound h3');
                        locationsFound.fadeOut();
                        $('#loading').fadeIn();
                        var geocoder = new google.maps.Geocoder();
                        geocoder.geocode({
                            'address': locationsField.val()
                        }, function (results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                var location = results[0].geometry.location;
                                var pos = new google.maps.LatLng(location.lat(), location.lng());
                                map.setCenter(pos);
                                fromlat = location.lat()
                                fromlong = location.lng()
                                $(settings.locations.locationsList).readFromAPI();
                            } else {
                                locationsFound.html(status).fadeIn();
                                $('#loading, #locationsResults, .scrollbar').fadeOut();
                                Map.resetMap();
                                $(settings.locations.locationsList).readFromAPI();
                            }
                        });
                        var place = autocomplete.getPlace();
                        if (settings.updateOnAddressChange) {
                            $(settings.locations.locationsList).readFromAPI();
                        }
                    });
                }
            }
            AutoComplete.init();

            function handleNoGeolocation(errorFlag) {
                if (errorFlag) {

                    jQuery.ajax({
                        url: 'https://freegeoip.net/json/',
                        success: function (response) {
                            // Tries to get a lat/lon for the IP address 
                            //console.log('Got default lat/lon')
                            //console.log(response)
                            settings.defaultLat = parseFloat(response.latitude)
                            settings.defaultLon = parseFloat(response.longitude)

                            // Create an array of styles.
                            var styles = [{
                                stylers: [{
                                    hue: "#bcc5d8"
                                }, {
                                    saturation: -80
                                }]
                            }, {
                                featureType: "road",
                                elementType: "geometry",
                                stylers: [{
                                    lightness: 100
                                }, {
                                    visibility: "simplified"
                                }]
                            }, {
                                featureType: "road",
                                elementType: "labels",
                                stylers: [{
                                    visibility: "off"
                                }]
                            }];

                            // Create a new StyledMapType object, passing it the array of styles,
                            // as well as the name to be displayed on the map type control.
                            var styledMap = new google.maps.StyledMapType(styles, {
                                name: "Styled Map"
                            });

                            map = new google.maps.Map(document.getElementById('map'), {
                                center: {
                                    lat: settings.defaultLat,
                                    lng: settings.defaultLon
                                },
                                mapTypeControlOptions: {
                                    mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
                                },
                                zoom: settings.defaultZoom,
                                scrollwheel: false
                            });

                            //Associate the styled map with the MapTypeId and set it to display.
                            map.mapTypes.set('map_style', styledMap);
                            map.setMapTypeId('map_style');

                            google.maps.event.addListener(map, 'dragend', function () {
                                if ((settings.refreshOnDrag)) {
                                    var center = map.getCenter();
                                    $(settings.locations.locationsList).readFromAPI();
                                }
                            });
                        }
                    });
                } else {
                    var content = 'Error: Your browser doesn\'t support geolocation.';
                }
                var options = {
                    map: map,
                    position: new google.maps.LatLng(60, 105),
                    content: content
                };
                var infowindow = new google.maps.InfoWindow(options);
                map.setCenter(options.position);
            }

        });

    }

    // BRANDS related methos

    $.fn.setBrands = function (par) {
        // sets up the brands JSON
        brands = par;
    }

    $.fn.getBrands = function () {
        // Creates the select options based on the brands JSON (local)
        return this.each(function () {
            var ret = "";
            brands.forEach(function (item) {
                ret += "<option value=" + item.brandSlug + ">" + item.brand + "";

            });
            $(this).html(ret);
        });

    }

    $.fn.getBrandsFromAPI = function (par) {
        // Calls the API and returns 
        // Parameters are:
        //      loader: the id of the DOM object that has the loader spin

        return this.each(function () {
            // Initializes the API call
            var callParameters = {
                method: 'brands',
                loader: par.loader
            }

            // Identifies the DOM objects
            var loader = par.loader; // Loader associated to the event
            var object = "#" + $(this).attr('id'); // Object to fill with the options

            // Composes the api call 
            var url = createCall(callParameters);

            // shows the loader
            $(loader).css('visibility', 'visible');
            jQuery.ajax({
                url: url,
                // xhrFields: {
                //    withCredentials: true
                // },
                success: function (response) {
                    var ret = ''
                    //var ret = "<option value=''>Please Choose A Brand</option>";
                    // Processes the API Response
                    response.forEach(function (item) {
                        ret += "<option value=" + item.brandSlug + ">" + item.brandName + "</option>";
                    });
                    // Aplies the brands to the destination object
                    $(object).html(ret);
                    $(object + ' option:eq(0)').prop('selected', true)
                    $('#selectBrands').trigger('change');
                    // Hides the loader gif
                    $(loader).css('visibility', 'hidden');
                }
            });

        });

    }

    // VARIANTS related methods

    $.fn.setVariants = function (par) {
        // sets up the brands JSON
        variants = par;
    }

    $.fn.getVariants = function () {
        // Creates the select options based on the brands JSON (local)
        return this.each(function () {
            var ret = "";
            var i = 0;
            var slug;
            variants.forEach(function (item) {
                ret += "<option value=" + item.variantSlug + ">" + item.variant + "";
                if (i == 0) {
                    slug = item.variantSlug;
                }
                i++;
            });
            $(settings.locations.locationsList).readFromAPI({
                variant: slug
            });
            $(this).html(ret);
        });

    }

    $.fn.getVariantsFromAPI = function (par) {
        // Calls the API and returns 
        // Parameters are:
        //      loader: the id of the DOM object that has the loader spin
        //      brand: sapL3 of the brand

        return this.each(function () {
            // Initializes the API call
            var callParameters = {
                method: 'variants',
                loader: par.loader,
                brand: par.brand
            }
            if (par.brand != null) {
                // Identifies the DOM objects
                var loader = par.loader; // Loader associated to the event
                var object = "#" + $(this).attr('id'); // Object to fill with the options

                // Composes the api call 
                var url = createCall(callParameters);
                // shows the loader
                $(loader).css('visibility', 'visible');
                jQuery.ajax({
                    url: url,
                    xhrFields: {
                        withCredentials: true
                    },
                    success: function (response) {
                        var ret = "<option value=''>Please Choose A Variant</option>";
                        // Processes the API Response
                        response.forEach(function (item) {
                            ret += "<option value=" + item.variantSlug + ">" + item.variantName + "</option>";
                        });
                        // Aplies the brands to the destination object
                        $(object).html(ret);
                        // Hides the loader gif
                        $(loader).css('visibility', 'hidden');
                    }
                });
            }

        });
    }

    $.fn.readFromAPI = function (par) {
        return this.each(function () {
            var query = settings.locations;
            if (par != undefined) {
                if (par.distance != undefined) query.distance = par.distance;
                if (par.loctype != undefined) query.loctype = par.loctype;
                if (par.variant != undefined) query.variant = par.variant;
            }
            $(this).getLocationsFromAPI(query);
        });
    }

    // LOCATIONS related methods

    // GOOGLE MAPS Functions

    //Sets the map on all markers in the array.
    function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
        markers = [];
        infoWindows = [];
    }

    $.fn.openInfoWindow = function () {
        return this.each(function () {

            $('.locationItem').removeClass('active');
            $(this).toggleClass('active');

            id = $(this).attr('id');
            markers.forEach(function (item) {
                infoWindows[item.id].close();
            });
            infoWindows[id].open(map, markers[id]);
        });
    }

    $.fn.getLocationsFromAPI = function (par) {
        // Calls the API and returns 
        // Parameters are:
        //      loader: the id of the DOM object that has the loader spin
        //      brand: sapL3 of the brand

        //console.log(par.variant);
        if ((par.variant == "") || (par.variant == undefined)) {
            par.variant = settings.variant;
            $('.btn.wtb-button').addClass('inactive');
            // console.log(par.variant+'aaaaaaaaaaaaaaaaaa');
        } else {
            $('.btn.wtb-button').removeClass('inactive');
        }

        return this.each(function () {
            // Initializes the API call

            if ((par.variant == "") || (par.variant == undefined)) {
                par.variant = settings.variant;
            }

            if ((par.variant == "") || (par.variant == undefined)) {
                return;
            }

            //console.log(par.variant);

            var callParameters = {
                method: 'locations',
                loader: par.loader,
                variant: par.variant,
                lat: map.getCenter().lat(),
                lon: map.getCenter().lng(),
                distance: par.distance,
                uom: par.uom,
                loctype: par.loctype,
                object: par.object
            }

            // update settings so system remembers the last query parameters

            settings.distance = par.distance;
            settings.variant = par.variant;
            settings.loctype = par.loctype;

            lastSearch = callParameters;

            // Identifies the DOM objects
            var loader = par.loader; // Loader associated to the event
            var object = '';
            var timeOut = 0;
            var animation;
            if (settings.animation) {
                timeOut = 10;
                animation = google.maps.Animation.DROP;
            }

            if (par.object == undefined) {
                object = "#" + $(this).attr('id'); // Object to fill with the options
                lastSearch.object = object;
            } else {
                object = par.object;
            }
            // Clean the map
            map.panTo({
                lat: map.getCenter().lat(),
                lng: map.getCenter().lng()
            });

            // Composes the api call 
            var url = createCall(callParameters);

            // shows the loader
            $(loader).css('visibility', 'visible');
            // hides the text
            $(object).css('visibility', 'hidden');

            // define loclenght

            var locLenght = [];

            jQuery.ajax({
                url: url,
                xhrFields: {
                    withCredentials: true
                },
                success: function (response) {
                    //alert(url);

                    var ret = "";
                    var retEcomm = "";

                    // Initializes the map
                    var marker;
                    setMapOnAll(null);

                    var i = 0;
                    // Processes the API Response
                    if (response.locations != undefined && response.locations.length > 0) {

                        response.locations.forEach(function (item) {
                            // Is it an eComm link?
                            var isLocation = (item._id != undefined);

                            if (isLocation) {
                                var phonenumberformatted = parsePhoneNumber(item.phone, settings.phoneFormat);
                                var locationTag = "";

                                click = '$(this).openInfoWindow(); ' +
                                    '$(".locationItem").removeClass("active"); ' +
                                    '$(this).toggleClass("active")';

                                locationTag += "<dl class='locationItem' id='" + i + "' onclick='" + click + ";' style='cursor:pointer'>";

                                var numbers;
                                numbers = (settings.numbers) ? (i + 1) + ". " : "";

                                var numero = i + 1;


                                locationTag += "<dd class='locationName'>" + numbers + getValueIfDefined(item, "locationName") + "</dd>";
                                locationTag += "<dd class='locationAddress'>" + getValueIfDefined(item, "address") + "</dd>";
                                locationTag += "<dd class='locationCity'>" + getValueIfDefined(item, "city") + ", " + getValueIfDefined(item, "state") + " " + getValueIfDefined(item, "zip") + "</dd>";
                                locationTag += "<dd class='locationPhone'><a href='tel:" + phonenumberformatted + "' target='_blank'>" + phonenumberformatted + "</a></dd>";
                                locationTag += "<dd class='locationPhone'><a class='directionsBtn' target='_blank' href='https://maps.google.com?saddr=Current+Location&daddr=" + getValueIfDefined(item, "address") + "'>Directions</a></dd>";

                                locationTag += "</dl>";

                                ret += locationTag;
                                // add markers
                                // create info window

                                // get locations count and push it to array

                                $(locationTag).each(function (index) {
                                    locLenght.push(index);
                                });


                                infoWindows.push(new google.maps.InfoWindow({
                                    content: locationTag,
                                    maxWidth: 200
                                }));

                                window.setTimeout(function () {
                                    var numbers;
                                    numbers = (settings.numbers) ? (markers.length + 1) : "";

                                    markers.push(new google.maps.Marker({
                                        map: map,
                                        animation: animation,
                                        position: {
                                            lat: item.location.coordinates[1],
                                            lng: item.location.coordinates[0]
                                        },
                                        icon: settings.markerURL,
                                        label: {
                                            text: '' + numbers + '',
                                            color: 'white',
                                            fontFamily: 'elephantregular'
                                        },
                                        title: item.locationName,
                                        id: markers.length
                                    }));

                                    markers[markers.length - 1].addListener('click', function () {
                                        markers.forEach(function (item) {
                                            if (infoWindows[item.id]) {
                                                infoWindows[item.id].close();
                                            }
                                        });
                                        if (infoWindows[this.id]) {
                                            infoWindows[this.id].open(map, this);
                                        }
                                    });

                                }, i * timeOut);
                                i++;
                            }
                            // else {
                            //     // if it is not a Location, it is an eComm
                            //     // it can either be Drizzly or Reserve Bar
                            //     // Drizzly
                            //     var ecomtype = 'generic';
                            //     var url = '';
                            //     var cta = '';
                            //     var logo = '';
                            //     if (item.drizly) {
                            //         ecomtype = 'drizly';
                            //         url = item.drizly.drizly_url;
                            //         cta = item.drizly.cta_text;
                            //         logo = item.drizly.logo_url
                            //     }
                            //     if (item.reserveBar) {
                            //         ecomtype = 'reserveBar';
                            //         url = item.reserveBar.reserveBar_url;
                            //         cta = item.reserveBar.cta_text;
                            //         logo = item.reserveBar.logo_url
                            //     }
                            //
                            //     var ecomItem = '<div class="eCommArea">';
                            //     ecomItem += "<div class='eCommButton'><a href='" + url + "' target='_blank' rel='nofollow' id='" + ecomtype + "'>"
                            //     ecomItem += "<span>" + cta + "</span></div>";
                            //     ecomItem += "<div class='eCommLogo'><div class='eCommImage' style='background-image: url(" + logo + ")'></div></div>";
                            //     ecomItem += "</a>";
                            //     ecomItem += '</div>';
                            //     retEcomm += ecomItem;
                            // }
                        });
                    }
                    if (response.ecommerce != undefined && response.ecommerce.length > 0) {
                        setTimeout(function () {
                            $('#' + settings.eCommContainer).empty();
                            response.ecommerce.forEach(function (item) {

                                if (settings.eComm == true && settings.eCommContainer != undefined) {
                                    var nodeString = '<a href="' + item.link_url + '" target="_blank" rel="nofollow"><span>' + item.cta_text + '</span> <img src="' + item.logo_url + '" alt="' + item.service + '" /> </a>';
                                    console.log(nodeString);
                                    console.log(settings.eCommContainer);
                                    $('#' + settings.eCommContainer).append(nodeString);
                                }

                            });
                        }, 100);
                    }

                    if (i == 0 && settings.locations.notFoundMessage != undefined) {
                        var notFoundLocation = "<dl class='locationItem'>";
                        notFoundLocation += "<dd class='locationName'>" + settings.locations.notFoundMessage + "</dd>";
                        notFoundLocation += "</dl>";

                        ret += notFoundLocation;
                    }
                    // Aplies the brands to the destination object

                    $('.ecomm-area').html(retEcomm);

                    $('.wtb-button').on('click', function () {

                        //alert('click en wtb button');

                        $('#locations').show();

                        $('.no-result-map').hide();

                        $(object).css('visibility', 'visible');

                        $('.map-wrapper').addClass('col-xs-12 col-sm-12 col-md-12 col-lg-9');
                        // Hides the loader gif
                        $(loader).css('visibility', 'hidden');

                        /*console.log(locLenght.length);*/

                        if ($(object).hasClass('slick-initialized')) {
                            $(object).empty();
                            $(object).slick('unslick');
                        }

                        if (locLenght.length > 4) {
                            /*console.log('armar slider');*/
                            $(object).html(ret);
                            $('#locationsList').slick({
                                rows: 4, dots: true, slidesPerRow: 1, dotsClass: 'dotcustom'
                            });
                        }

                        else {
                            $(object).html(ret);
                        }


                        if (locLenght.length == 0) {
                            $('.no-result-map').show();
                        } else {
                            $('.no-result-map').hide();
                        }

                    });

                    $('#locations').show();

                    if (locLenght.length == 0) {
                        $('.no-result-map').show();
                    } else {
                        $('.no-result-map').hide();
                    }

                    $(object).css('visibility', 'visible');

                    $('.map-wrapper').addClass('col-xs-12 col-sm-12 col-md-12 col-lg-9');
                    // Hides the loader gif
                    $(loader).css('visibility', 'hidden');

                    /*console.log(locLenght.length);*/

                    if ($(object).hasClass('slick-initialized')) {
                        $(object).empty();
                        $(object).slick('unslick');
                    }

                    if (locLenght.length > 4) {
                        /*console.log('armar slider');*/
                        $(object).html(ret);
                        $('#locationsList').slick({
                            rows: 4, dots: true, slidesPerRow: 1, dotsClass: 'dotcustom'
                        });
                    }

                    else {
                        $(object).html(ret);
                        $(object).css('visibility', 'visible');
                        // Hides the loader gif
                        $(loader).css('visibility', 'hidden');
                    }


                }
            });

        });
    }

    function parsePhoneNumber(phone, phoneFormat) {
        var rawPhone = (phone != undefined) ? phone : "";
        var parsedPhone = "";

        if (rawPhone == '--' || rawPhone.length == 0) {
            return parsedPhone;
        }

        for (var i = (phoneFormat.length - 1); i >= 0; i--) {
            if (phoneFormat[i] == '#') {
                if (rawPhone.length > 0) {
                    parsedPhone = rawPhone[rawPhone.length - 1] + parsedPhone;
                    rawPhone = rawPhone.substring(0, rawPhone.length - 1);
                } else {
                    continue;
                }
            } else {
                parsedPhone = phoneFormat[i] + parsedPhone;
            }
        }
        if (rawPhone.length > 0) {
            parsedPhone = rawPhone + parsedPhone;
        }

        // console.log('originalPhone: ' + ((phone != undefined) ? phone : "") + ', phoneFormat: ' + phoneFormat + ', parsedPhone: ' + parsedPhone);
        return parsedPhone;
    }

    function getValueIfDefined(obj, propName) {
        if (obj != undefined && obj[propName] != undefined)
            return obj[propName];
        return "";
    }


    // CREATE URL

    function createCall(par) {
        var url = settings.diageo_api_url;

        switch (par.method) {
            case "brands":
                url += par.method + "/" + settings.locale + "/?appid=" + settings.appid + "&r=" + new Date().getTime();
                break;
            case "variants":
                url += par.method + "/" + settings.locale + "/" + par.brand + "/?appid=" + settings.appid + "&r=" + new Date().getTime();
                break;
            case "locations":
                url += settings.locale + "/" + par.variant + "/" + par.lat + "/" + par.lon + "/" + par.distance + "/" + par.uom + "?appid=" + settings.appid + "&loctype=" + par.loctype + "&r=" + new Date().getTime();
                if (settings.eComm) {
                    url += '&showonline=true'
                }
                break;
        }
        //console.log(url);
        return url;
    }

}(jQuery));



