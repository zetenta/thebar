<?php
include 'classSEO.php';
include 'classCacheSEO.php';

$cacheSEO = new cacheSEO();

$cacheSEO->generateHome();
$cacheSEO->generateRecipesDetail('recipe', 'bourbon-hot-toddy');
$cacheSEO->generateRecipesDetail('recipe', 'captain-morgan-mojito');
$cacheSEO->generateRecipesDetail('recipe', 'ciroc-peach-bellini');
$cacheSEO->generateRecipesDetail('recipe', 'dickel-rye-manhattan');
$cacheSEO->generateRecipesDetail('recipe', 'heavens-to-betsy');

$cacheSEO->generateRecipesDetail('recipe', 'baileys-hot-co-co-bay');

$cacheSEO->generateRecipesDetail('recipe', 'smirnoff-french-collins');
//$cacheSEO->generateRecipesDetail('recipe', 'dickel-rye-manhattan');
//$cacheSEO->generateRecipesDetail('recipe', 'dickel-rye-manhattan');
//$cacheSEO->generateRecipesDetail('recipe', 'dickel-rye-manhattan');

