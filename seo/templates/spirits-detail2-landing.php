<div class="col-md-12 page-header">
    <h1>Whisk(e)y Step 2</h1>
</div>

<div class="col-md-3">
    <ul class="list-group">
        <li class="list-group-item">
            <a href="/spirits/whisky/what-is-whisky">
                What is Whisky
            </a>
        </li>
        <li class="list-group-item">
            <a href="/spirits/whisky/types-of-whisky">
                Types of Whisky
            </a>
        </li>
        <li class="list-group-item">
            <a href="/spirits/whisky/our-brands">
                Our Brands
            </a>
        </li>
        <li class="list-group-item">
            <a href="/spirits/whisky/your-whisky">
                Your Whisky
            </a>
        </li>
    </ul>
</div>

<div class="col-md-9">

    <div class="col-md-12 page-header">
        <?php
        foreach ($this->detailSpirits->spirit_detail as $repeater) { ?>

            <?php if ($current_spirit == $repeater->slug) { ?>
                <h1>
                    <?php echo sprintf($repeater->spirits_type_of_title); ?>
                </h1>
                <h2><?php echo sprintf($repeater->spirits_type_of_subtitle); ?></h2>
            <?php }
            ?>

        <?php }
        ?>
    </div>


    <div class="col-md-12">

        <?php
        foreach ($this->detailSpirits->spirit_detail as $repeater) { ?>

            <?php if ($current_spirit == $repeater->slug) { ?>

                <?php
                foreach ($repeater->spirits_types_of_whisky as $repeater2) { ?>
                    <div class="col-md-4">
                        <div class="well well-lg" style="min-height: 600px;">
                            <h1>
                                <?php echo sprintf($repeater2->spirits_types_of_whisky_title); ?>
                            </h1>
                            <h2>
                                <?php echo $repeater2->spirits_types_of_whisky_text; ?>
                            </h2>
                            <h5>
                                <?php echo sprintf($repeater2->spirits_types_of_whisky_title_flavor); ?>
                            </h5>
                            <h3>
                                <?php echo sprintf($repeater2->spirits_types_of_whisky_flavor_notes); ?>
                            </h3>
                        </div>
                    </div>
                <?php }
                ?>


            <?php }
            ?>

        <?php }
        ?>

    </div>

    <div class="col-md-12">
        <div class="well well-lg">
            <?php
            foreach ($this->detailSpirits->spirit_detail as $repeater) { ?>

                <?php if ($current_spirit == $repeater->slug) { ?>

                    <?php
                    foreach ($repeater->spirits_flavor_content as $repeater2) { ?>
                        <h1>
                            <?php echo sprintf($repeater2->spirits_flavor_content_title); ?>
                        </h1>
                        <p>
                            <?php echo sprintf($repeater2->spirits_flavor_content_description); ?>
                        </p>
                    <?php }
                    ?>


                <?php }
                ?>

            <?php }
            ?>

            <div class="col-md-12 page-header">
                <h1>Flavor List</h1>
            </div>

            <?php
            foreach ($this->productsListFlavor->list_flavor as $repeater) { ?>
                <h1>
                    <?php echo sprintf($repeater->name); ?>
                </h1>
                <p>
                    <?php echo $repeater->description; ?>
                </p>

            <?php }
            ?>

        </div>
    </div>


    <div class="col-md-12 page-header">
        <?php
        foreach ($this->detailSpirits->spirit_detail as $repeater) { ?>

            <?php if ($current_spirit == $repeater->slug) { ?>
                <h1>
                    <?php echo sprintf($repeater->single_malt_vs_blended_title); ?>
                </h1>

                <p>
                    <img src="<?php echo sprintf($repeater->single_malt_vs_blended_image); ?>"
                         class="img-thumbnail"
                         alt="<?php echo sprintf($repeater->spirits_type_of_title); ?>"/>
                </p>

                <p><?php echo $repeater->single_malt_vs_blended_description; ?></p>
            <?php }
            ?>

        <?php }
        ?>
    </div>


    <div class="col-md-12">

        <?php
        foreach ($this->detailSpirits->spirit_detail as $repeater) { ?>

            <?php if ($current_spirit == $repeater->slug) { ?>

                <?php
                foreach ($repeater->single_malt_vs_blended_content as $repeater2) { ?>
                    <div class="col-md-12">
                        <div class="well well-lg">
                            <h1>
                                <?php echo sprintf($repeater2->single_malt_vs_blended_content_title); ?>
                            </h1>
                            <p>
                                <?php echo $repeater2->single_malt_vs_blended_content_description; ?>
                            </p>
                            <h2>
                                <?php echo sprintf($repeater2->single_malt_vs_blended_content_distillation_title); ?>
                            </h2>
                            <p>
                                <?php echo sprintf($repeater2->single_malt_vs_blended_content_distillation_description); ?>
                            </p>
                            <h2>
                                <?php echo sprintf($repeater2->single_malt_vs_blended_content_flavor_title); ?>
                            </h2>
                            <p>
                                <?php echo sprintf($repeater2->single_malt_vs_blended_content_flavor_description); ?>
                            </p>
                        </div>
                    </div>

                <?php }
                ?>


            <?php }
            ?>

        <?php }
        ?>

    </div>

    <div class="col-md-12">

        <?php
        foreach ($this->detailSpirits->spirit_detail as $repeater) { ?>

            <?php if ($current_spirit == $repeater->slug) { ?>

                <?php
                foreach ($repeater->single_malt_vs_blended_single_grain as $repeater2) { ?>
                    <div class="col-md-12">
                        <div class="well well-lg">
                            <h1>
                                <?php echo sprintf($repeater2->single_malt_vs_blended_single_grain_title); ?>
                            </h1>
                            <p>
                                <?php echo $repeater2->single_malt_vs_blended_single_grain_description; ?>
                            </p>

                            <p>
                                <img src="<?php echo sprintf($repeater2->single_malt_vs_blended_single_grain_image); ?>"
                                     class="img-thumbnail"
                                     alt="<?php echo sprintf($repeater2->single_malt_vs_blended_single_grain_tooltip); ?>"/>
                            </p>

                            <p>
                                <?php echo sprintf($repeater2->single_malt_vs_blended_single_grain_tooltip); ?>
                            </p>
                        </div>
                    </div>

                <?php }
                ?>


            <?php }
            ?>

        <?php }
        ?>

    </div>


</div>

<?php
//echo 'template 2<br/>';
//echo $page;