<div class="col-md-12 page-header">
    <h1><?php echo sprintf($theSpirit->name); ?></h1>
    <h2>All you need to know</h2>
</div>

<div class="col-md-6">
    <div class="well well-lg" style="min-height: 500px;">
        <h1>
            <?php echo sprintf($theSpirit->name); ?>
        </h1>
        <p>
            <img src="<?php echo sprintf($theSpirit->image); ?>"
                 class="img-thumbnail"
                 alt="<?php echo sprintf($theSpirit->name); ?>"/>
        </p>
        <p>
            <?php echo sprintf($theSpirit->description); ?>
        </p>
    </div>
</div>

<div class="col-md-6">
    <div class="well well-lg" style="min-height: 500px;">
        <?php
        foreach ($this->detailSpirits->spirit_detail as $repeater) { ?>

            <?php if ($current_spirit == $repeater->slug) { ?>

                <?php
                foreach ($repeater->spirits_origin as $repeater2) { ?>
                    <h1>
                        <?php echo sprintf($repeater2->spirits_origin_title); ?>
                    </h1>
                    <h2>
                        <?php echo sprintf($repeater2->spirits_origin_subtitle); ?>
                    </h2>
                    <p>
                        <?php echo sprintf($repeater2->spirits_origin_text); ?>
                    </p>
                    <p>
                        <img src="<?php echo sprintf($repeater2->spirits_origin_image); ?>"
                             class="img-thumbnail"
                             alt="<?php echo sprintf($repeater2->spirits_origin_title); ?>"/>
                    </p>
                <?php }
                ?>


            <?php }
            ?>

        <?php }
        ?>

    </div>
</div>


<div class="col-md-12 page-header">
    <h1>Our Brands </h1>
</div>

<?php
foreach ($this->brandsList->list_brands as $repeater) { ?>
    <?php if ($repeater->spirit_slug == $theSpirit->slug) { ?>
        <div class="col-md-3">
            <div class="well well-lg" style="min-height: 800px;">
                <h1>
                    <?php echo sprintf($repeater->name); ?>
                </h1>
                <p>
                    <img src="<?php echo sprintf($repeater->image); ?>"
                         class="img-thumbnail"
                         alt="<?php echo sprintf($repeater->name); ?>"/>
                </p>
                <p>
                    <?php echo sprintf($repeater->description); ?>
                </p>
                <a title="GET BRAND"
                   href="/brands/<?php echo sprintf($repeater->slug); ?>">
                    GET BRAND
                </a>
            </div>
        </div>
    <?php }
    ?>
<?php }
?>

<div class="col-md-12 page-header">
    <h1>Recipes</h1>
</div>
<?php
foreach ($this->templateRecipesAllRecipes3->docs as $repeater) { ?>
    <div class="col-md-3">
        <div class="well well-lg" style="min-height: 500px;">
            <h1>
                <?php echo sprintf($repeater->recipeTitle); ?>
            </h1>

            <?php
            $arrayTemp = (array)$repeater->SearchImage;
            //var_dump($arrayTemp);
            ?>

            <p>
                <img src="<?php echo $arrayTemp['original']; ?>"
                     class="img-thumbnail"
                     alt="<?php echo sprintf($repeater->recipeTitle); ?>"/>
            </p>

            <a title="GET RECIPE"
               href="/recipes/<?php echo sprintf($repeater->recipeSlug); ?>">
                GET RECIPE
            </a>
        </div>
    </div>
<?php }
?>

<!---->
<?php
//echo '<pre>';
//var_dump($theSpirit); ?>
