<div class="col-md-12 page-header">
    <h1>Search Page</h1>
</div>

<div class="col-md-12 page-header">
    <h1>All Recipes</h1>
</div>


<?php
foreach ($this->templateRecipesAllRecipes->docs as $repeater) { ?>
    <div class="col-md-3">
        <div class="well well-lg" style="min-height: 500px;">
            <h1>
                <?php echo sprintf($repeater->recipeTitle); ?>
            </h1>

            <?php
            $arrayTemp = (array)$repeater->SearchImage;
            //var_dump($arrayTemp);
            ?>

            <p>
                <img src="<?php echo $arrayTemp['original']; ?>"
                     class="img-thumbnail"
                     alt="<?php echo sprintf($repeater->recipeTitle); ?>"/>
            </p>

            <a title="GET RECIPE"
               href="/recipes/<?php echo sprintf($repeater->recipeSlug); ?>">
                GET RECIPE
            </a>
        </div>
    </div>
<?php }
?>


<div class="col-md-12 page-header">
    <h1>All Brands</h1>
</div>

<?php
foreach ($this->brandsList->list_brands as $repeater) { ?>
    <div class="col-md-3">
        <div class="well well-lg" style="min-height: 800px;">
            <h1>
                <?php echo sprintf($repeater->name); ?>
            </h1>
            <p>
                <img src="<?php echo sprintf($repeater->image); ?>"
                     class="img-thumbnail"
                     alt="<?php echo sprintf($repeater->name); ?>"/>
            </p>
            <p>
                <?php echo sprintf($repeater->description); ?>
            </p>
            <a title="LEARN MORE"
               href="/brands/<?php echo sprintf($repeater->slug); ?>">
                LEARN MORE
            </a>
        </div>
    </div>
<?php }
?>

<div class="col-md-12 page-header">
    <h1>All Spirits</h1>
</div>

<?php
foreach ($this->spiritsList->spirit_list as $repeater) { ?>
    <div class="col-md-3">
        <div class="well well-lg" style="min-height: 800px;">
            <h1>
                <?php echo sprintf($repeater->name); ?>
            </h1>
            <p>
                <img src="<?php echo sprintf($repeater->image); ?>"
                     class="img-thumbnail"
                     alt="<?php echo sprintf($repeater->name); ?>"/>
            </p>
            <p>
                <?php echo sprintf($repeater->description); ?>
            </p>

            <?php if ($repeater->slug == 'whisky') { ?>
                <a title="EXPLORE"
                   href="/spirits/<?php echo sprintf($repeater->slug); ?>/what-is-whisky">
                    EXPLORE
                </a>
            <?php } else { ?>
                <a title="EXPLORE"
                   href="/spirits/<?php echo sprintf($repeater->slug); ?>">
                    EXPLORE
                </a>
            <?php } ?>

        </div>
    </div>
<?php }
?>

<div class="col-md-12 page-header">
    <h1>All Occasions</h1>
</div>

<div class="col-md-12">

    <?php
    foreach ($this->occasionsList->list_occasions as $repeater) { ?>
        <div class="col-md-4">
            <div class="well well-lg" style="min-height: 500px;">
                <h1>
                    <?php echo sprintf($repeater->name); ?>
                </h1>
                <p>
                    <img src="<?php echo sprintf($repeater->image); ?>"
                         class="img-thumbnail"
                         alt="<?php echo sprintf($repeater->name); ?>"/>
                </p>
                <p>
                    <?php echo sprintf($repeater->description); ?>
                </p>
                <a title="EXPLORE OCCASION"
                   href="/occasions/<?php echo sprintf($repeater->slug); ?>">
                    EXPLORE OCCASION
                </a>
            </div>
        </div>
    <?php }
    ?>

</div>

<div class="col-md-12 page-header">
    <h1>All Articles</h1>
</div>

<div class="col-md-12">
    <?php
    foreach ($this->articlesList->list_articles as $repeater) { ?>
        <div class="col-md-3">
            <div class="well well-lg" style="min-height: 700px;">
                <h1>
                    <?php echo sprintf($repeater->name); ?>
                </h1>
                <p>
                    <img src="<?php echo sprintf($repeater->image); ?>"
                         class="img-thumbnail"
                         alt="<?php echo sprintf($repeater->name); ?>"/>
                </p>
                <p>
                    <?php echo sprintf($repeater->description); ?>
                </p>
                <a target="_blank" title="GET ARTICLE"
                   href="<?php echo sprintf($repeater->slug); ?>">
                    GET ARTICLE
                </a>
            </div>
        </div>
    <?php }
    ?>
</div>
