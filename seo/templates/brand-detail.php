<div class="col-md-12 page-header">
    <h1>BRAND DETAIL</h1>
</div>

<?php
$arrayTemp = (array)$this->brandRecipeIn;
//var_dump($arrayTemp);
?>

<div class="col-md-12">

    <?php
    foreach ($this->brandDetail->list_brands as $repeater) { ?>

        <?php if ($current_brand == $repeater->slug) { ?>

            <div class="col-md-6">
                <div class="well well-lg">

                    <h5><?php echo sprintf($repeater->spirit_name); ?></h5>
                    <h1><?php echo sprintf($repeater->collection_name); ?></h1>

                    <p>
                        <img src="<?php echo sprintf($repeater->image); ?>"
                             class="img-thumbnail"
                             alt="<?php echo sprintf($repeater->name); ?>"/>
                    </p>

                    <h4> <?php echo $arrayTemp['totalcount']; ?> recipes</h4>
                    <?php
                    $counter = 0;
                    foreach ($this->productsList->list_our_products as $repeater1) { ?>
                        <?php if ($current_brand == $repeater1->brand_slug) {
                            $counter++; ?>
                        <?php }
                        ?>
                    <?php }
                    ?>
                    <h4> <?php echo $counter; ?> products</h4>

                </div>

            </div>

            <div class="col-md-6">

                <div class="col-md-12">
                    <div class="well well-lg">
                        <h1><?php echo sprintf($repeater->about_title); ?></h1>
                        <p><?php echo sprintf($repeater->about_content); ?></p>
                    </div>
                </div>

                <?php if ($repeater->distillery_title) { ?>
                    <div class="col-md-12">
                        <div class="well well-lg">
                            <h1><?php echo sprintf($repeater->distillery_title); ?></h1>
                            <p>
                                <img src="<?php echo $repeater->distillery_map_image; ?>"
                                     class="img-thumbnail"
                                     alt="<?php echo $repeater->distillery_title; ?>"/>
                            </p>
                            <?php
                            foreach ($repeater->distillery_marker as $repeater2) { ?>
                                <p>
                                    <?php echo sprintf($repeater2->brands_distillery_map_marker_name); ?>
                                </p>
                            <?php }
                            ?>

                        </div>
                    </div>
                <?php }
                ?>

                <div class="col-md-12">
                    <div class="well well-lg">
                        <h1><?php echo sprintf($repeater->brands_where_to_buy); ?>
                            - <?php echo sprintf($repeater->name); ?></h1>
                    </div>
                </div>

            </div>

        <?php }
        ?>

    <?php }
    ?>
</div>


<div class="col-md-12 page-header">
    <h1>Products</h1>
</div>

<div class="col-md-12">

    <ul class="list-group col-md-3">
        <?php
        foreach ($this->productsList->list_our_products as $repeater) { ?>
            <?php if ($current_brand == $repeater->brand_slug) { ?>
                <li class="list-group-item">
                    <a title=" <?php echo sprintf($repeater->name); ?>"
                       href="/brands/<?php echo $current_brand; ?>/<?php echo $repeater->slug; ?>">
                        <?php echo sprintf($repeater->name); ?>
                    </a>
                </li>

            <?php }
            ?>
        <?php }
        ?>
    </ul>

    <div class="col-md-9">
        <?php
        $counter = 0;
        foreach ($this->productsList->list_our_products as $repeater) { ?>
            <?php if ($current_brand == $repeater->brand_slug) {
                $counter++; ?>
                <div class="col-md-4">
                    <div class="well well-lg" style="min-height: 700px;">
                        <p>
                            <img src="<?php echo sprintf($repeater->image); ?>"
                                 class="img-thumbnail"
                                 alt="<?php echo sprintf($repeater->name); ?>"/>
                        </p>

                        <h1>
                            <?php echo sprintf($repeater->name); ?>
                        </h1>

                        <p>
                            <?php echo $repeater->description; ?>
                        </p>

                    </div>
                </div>
            <?php }
            ?>

        <?php }
        ?>
    </div>
</div>

<div class="col-md-12 page-header">
    <h1>Explore recipes </h1>
</div>

<?php
foreach ($this->brandRecipeIn->docs as $repeater) { ?>
    <div class="col-md-3">
        <div class="well well-lg" style="min-height: 500px;">
            <h1>
                <?php echo sprintf($repeater->recipeTitle); ?>
            </h1>

            <?php
            $arrayTemp = (array)$repeater->SearchImage;
            //var_dump($arrayTemp);
            ?>

            <p>
                <img src="<?php echo $arrayTemp['original']; ?>"
                     class="img-thumbnail"
                     alt="<?php echo sprintf($repeater->recipeTitle); ?>"/>
            </p>

            <a title="GET RECIPE"
               href="/recipes/<?php echo sprintf($repeater->recipeSlug); ?>">
                GET RECIPE
            </a>
        </div>
    </div>
<?php }
?>

