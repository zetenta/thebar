<div class="col-md-12 page-header">
    <h1>Frequently Asked Questions</h1>
</div>


<div class="col-md-12">
    <div class="well well-lg">

        <div class="container">

            <div class="row">

                <div id="article-content" class="white_cnt whiteBoxBackground">
                    <div class="container">
                        <div id="pageHeader">
                            <h1 id="articleNameHeader">Frequently Asked Questions<span></span></h1>
                        </div>

                        <ol class="questions">
                            <li><a href="#whoIs">Who is Diageo?</a></li>

                            <li><a href="#affiliated">Is thebar.com affiliated with an actual bar?</a></li>

                            <li><a href="#createProfile">I'm accessing thebar.com from outside the U.S. Can I still
                                    create a mybar profile?</a></li>

                            <li><a href="#personalInfo">Why do you need my personal information? </a></li>

                            <li><a href="#drinkingPreferences">Why do you ask about my drinking preferences? </a></li>

                            <li><a href="#register">Do I have to get your email(s) if I register at thebar.com? </a>
                            </li>

                            <li><a href="#mailingList">If I enter a sweepstakes at thebar.com, am I automatically signed
                                    up on the brand's mailing list? </a></li>

                            <li><a href="#changeEmail">How do I change my email (login ID) and/or password?</a></li>

                            <li><a href="#getCoupons">How can I get coupons or discounts on drinks from thebar.com?</a>
                            </li>

                            <li><a href="#seeVideos">Why can't I see the videos?</a></li>

                            <li><a href="#flashPlugin">How do I get the Flash plugin? </a></li>

                            <li><a href="#problem">What can I do if I'm having a technical problem with the site? </a>
                            </li>

                            <li><a href="#requirements">What are the system requirements in order to have the optimal
                                    website experience? </a></li>

                            <li><a href="#password">What if I forget my login ID and/or password? </a></li>

                            <li><a href="#sharepersonalInfo">Will thebar.com share my personal information? </a></li>

                            <li><a href="#merchandise">Do you sell merchandise? </a></li>

                            <li><a href="#ownRecipes">How can I submit my own recipes to thebar.com? </a></li>

                            <li><a href="#jobOpportunities">Are there job opportunities at thebar.com? </a></li>

                            <li><a href="#usernamePasword">How do I get a username and password so I can log in? </a>
                            </li>

                            <li><a href="#spirit">What is a spirit?</a></li>

                            <li><a href="#saveRecipe">How do I save a recipe, article or video?</a></li>

                            <li><a href="#removeRecipe">How do I remove a recipe, shopping list, article or video from
                                    my mybar profile? </a></li>

                            <li><a href="#shoppingList">What is "Shopping List" and how does it work? </a></li>

                            <li><a href="#quantityAdjustor">What is the "Quantity Adjustor" and how does it work?</a>
                            </li>

                            <li><a href="#storeLocator">How does the "Store Locator" work?</a></li>
                        </ol>

                        <h3 id="whoIs">Who is Diageo?</h3>

                        <p>Diageo is the world's leading premium drinks business with an outstanding collection of
                            beverage alcohol brands across spirits, wines and beer categories. Diageo is a global
                            company, trading in more than 200 markets around the world. The company is listed on both
                            the New York Stock Exchange (DEO) and the London Stock Exchange (DGE). For more information
                            about Diageo, it's people, brands and performance, please visit <a
                                    href="http://www.diageo.com/en-row/Pages/default.aspx">http://www.diageo.com</a>.
                        </p>

                        <h3 id="affiliated">Is thebar.com affiliated with an actual bar?</h3>

                        <p>No.</p>

                        <h3 id="createProfile">I'm accessing thebar.com from outside the U.S. Can I still create a mybar
                            profile?</h3>

                        <p>You can create a mybar profile, but you won't be able to personalize your membership
                            (sorry).</p>

                        <h3 id="personalInfo">Why do you need my personal information?</h3>

                        <p>We ask questions pertaining to your personal information in order to verify that you are of
                            legal drinking age. We cannot allow consumers to become members unless they are of legal
                            drinking age.</p>

                        <h3 id="drinkingPreferences">Why do you ask about my drinking preferences?</h3>

                        <p>We ask questions about your drinking preferences to get to know you better. Your information
                            helps us personalize your experience at thebar.com ? and bring you recipes and types of
                            drinks that most interest you.</p>

                        <h3 id="register">Do I have to get your email(s) if I register at thebar.com?</h3>

                        <p>No. You can change your email preferences anytime by editing your profile.</p>

                        <h3 id="mailingList">If I enter a sweepstakes at thebar.com, am I automatically signed up on the
                            brand's mailing list?</h3>

                        <p>No. For more information, please see our Privacy Policy. If you enter a sweepstakes on an
                            individual brand site, please be sure to read the privacy policy there too.</p>

                        <h3 id="changeEmail">How do I change my email (login ID) and/or password?</h3>

                        <p>You can change your personal information anytime by editing your <a class="myBarCTA overlay"
                                                                                               href="/contact">mybar
                                profile</a>.</p>

                        <h3 id="getCoupons">How can I get coupons or discounts on drinks from thebar.com?</h3>

                        <p>Sorry, but legal regulations in some states prohibit us from directly offering coupons or
                            discounts on spirits, wine or beer.</p>

                        <h3 id="seeVideos">Why can't I see the videos?</h3>

                        <p>All videos are hosted on YouTube.com and leverage the YouTube video player. If you are unable
                            to view videos please visit the following URL for support <a
                                    href="                          https://support.google.com/youtube/bin/answer.py?hl=en&amp;answer=56115&amp;ctx=cb&amp;src=cb&amp;cbid=14mml65sj769m&amp;cbrank=3                          ">
                                https://support.google.com/youtube/bin/answer.py?hl=en&amp;answer=56115&amp;ctx=cb&amp;src=cb&amp;cbid=14mml65sj769m&amp;cbrank=3</a>
                        </p>

                        <h3 id="flashPlugin">How do I get the Flash plugin?</h3>

                        <p>You can download Flash free from <a href="http://www.adobe.com">www.adobe.com</a></p>

                        <h3 id="problem">What can I do if I'm having a technical problem with the site?</h3>

                        <p class="noMargin">Contact us at <a href="/contact">ConsumerCare@thebar.com</a>. Be sure to
                            include the following information so we can look into the problem:</p>

                        <ul>
                            <li>Browser-Internet Explorer, Mozilla Firefox, etc.</li>

                            <li>Operating system (Mac, PC, etc.)</li>

                            <li>Connection type-dial-up, cable, DSL, T1, etc.</li>

                            <li>ISP/Internet provider</li>

                            <li>Problem details-error message, etc.</li>
                        </ul>

                        <h3 id="requirements">What are the system requirements in order to have the optimal website
                            experience?</h3>

                        <ul>
                            <li>1024 x 768 resolution preferred</li>

                            <li>PC with broadband Internet connection</li>

                            <li>1 Ghz or faster processor</li>

                            <li>512 MB or greater RAM</li>

                            <li>This site has been optimized to be viewed in the following browsers: Internet Explorer
                                version 7,8 or 9, Firefox version 12 or higher, Safari version 5 or Chrome version 18.
                            </li>
                        </ul>

                        <h3 id="password">What if I forget my login ID and/or password?</h3>

                        <p>Your login ID is the email address you used to register. If you forget your password, click
                            the "Forgot Password" link on any login screen.</p>

                        <h3 id="sharepersonalInfo">Will thebar.com share my personal information?</h3>

                        <p>Thebar.com is committed to protecting your privacy and will only use your data in accordance
                            with our Privacy Policy.</p>

                        <h3 id="merchandise">Do you sell merchandise?</h3>

                        <p>We're working on it. As soon as it's available, we'll announce it on the site. Any particular
                            merchandise you'd like to see? Go to Contact Us, choose "Other" from the drop-down list and
                            send us an email.</p>

                        <h3 id="ownRecipes">How can I submit my own recipes to thebar.com?</h3>

                        <p>You can suggest a recipe by <a href="/contact">contacting us</a>.</p>

                        <h3 id="jobOpportunities">Are there job opportunities at thebar.com?</h3>

                        <p>For more information about a Diageo career, visit <a
                                    href="http://www.diageo-careers.com/en-row/Pages/default.aspx">Diageo-careers.com</a>
                        </p>

                        <h3 id="usernamePasword">How do I get a username and password so I can log in?</h3>

                        <p>It's simple, all you need to do is create a <a class="myBarCTA overlay"
                                                                          href="/account/login">mybar profile</a>. Your
                            username will be your own email address, and you can choose your own password. If you've
                            forgotten your password, click the "Forgot Password" link on any mybar <a class="overlay"
                                                                                                      href="/account/login">login</a>
                            screen.</p>

                        <h3 id="spirit">What is a spirit?</h3>

                        <p>Spirits are distilled liquors, such as rum, gin, vodka or whisky. At thebar.com, we also
                            occasionally use "spirit" to refer to all alcoholic beverages or alcoholic beverages in
                            general.</p>

                        <h3 id="saveRecipe">How do I save a recipe, article or video?</h3>

                        <p>To save any of these items, you must first have a <a class="myBarCTA overlay"
                                                                                href="/account/login">mybar profile</a>.
                            If you already have a mybar profile, click the "Save To mybar" button on the recipe page.
                        </p>

                        <h3 id="removeRecipe">How do I remove a recipe, shopping list, article or video from my mybar
                            profile?</h3>

                        <p>Visit "mybar" at the top of the page to remove any saved items.</p>

                        <h3 id="shoppingList">What is "Shopping List" and how does it work?</h3>

                        <p>Planning a party? Add drink recipes to your shopping list for a complete list of ingredients
                            you'll need at the store.</p>

                        <h3 id="quantityAdjustor">What is the "Quantity Adjustor" and how does it work?</h3>

                        <p>You'll find a quantity adjustor wherever you find a recipe. Select the number of servings you
                            want to make, and the recipe will tell you how much of each ingredient you'll need.</p>

                        <h3 id="storeLocator">How does the "Store Locator" work?</h3>

                        <p>Trying to track down your favorite spirits? Enter your address, city, state or ZIP code and
                            the brand you're looking for into the <a href="/where-to-buy">Store Locator</a>. It will
                            locate the retailers near you that carry your favorite brands. (Please note, the store
                            locator is only available in the following states: CA, DE, FL, HI, IA, IL, KS, LA, MA, MN,
                            MO, MT, NC, NE, NH, NV, NM, NJ, NY, SD, TX, VT, WI.)</p>
                    </div>
                </div>

            </div>

        </div>

    </div>

</div>
