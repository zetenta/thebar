<meta name="site" content="<?php echo $seoData['lang']; ?>">
<link rel="canonical" href="<?php echo $seoData['canonicalUrl'] ?>">
<title><?php echo $seoData['title']; ?></title>
<meta name="description" content="<?php echo $seoData['description']; ?>">
<meta name="keywords" content="<?php echo $seoData['keywords']; ?>">
<meta property="google-site-verification" content="<?php echo $seoData['googleVerif']; ?>">
<?php foreach ($seoData['alternate'] as $alter) { ?>
    <link rel="alternate" hreflang="<?php echo $alter['lang']; ?>" href="<?php echo $alter['href']; ?>">
<?php } ?>
<!--facebook metatags-->
<meta property="og:title" content="<?php echo $seoData['title']; ?>">
<meta property="og:url" content="<?php echo $seoData['currentURL']; ?>">
<meta property="og:image" content="<?php echo $seoData['imgURL']; ?>">

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">

<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">

<style>
    body {
        font-family: 'Roboto Condensed', sans-serif;
    }
</style>
