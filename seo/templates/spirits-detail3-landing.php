<div class="col-md-12 page-header">
    <h1>Whisk(e)y Step 3</h1>
</div>

<div class="col-md-3">
    <ul class="list-group">
        <li class="list-group-item">
            <a href="/spirits/whisky/what-is-whisky">
                What is Whisky
            </a>
        </li>
        <li class="list-group-item">
            <a href="/spirits/whisky/types-of-whisky">
                Types of Whisky
            </a>
        </li>
        <li class="list-group-item">
            <a href="/spirits/whisky/our-brands">
                Our Brands
            </a>
        </li>
        <li class="list-group-item">
            <a href="/spirits/whisky/your-whisky">
                Your Whisky
            </a>
        </li>
    </ul>
</div>

<div class="col-md-9">

    <div class="col-md-12">
        <div class="col-md-12 page-header">
            <h1>Our Brands </h1>
        </div>

        <?php
        foreach ($this->brandsList->list_brands as $repeater) { ?>
            <?php if ($repeater->spirit_slug == $theSpirit->slug) { ?>
                <div class="col-md-3">
                    <div class="well well-lg" style="min-height: 800px;">
                        <h1>
                            <?php echo sprintf($repeater->name); ?>
                        </h1>
                        <p>
                            <img src="<?php echo sprintf($repeater->image); ?>"
                                 class="img-thumbnail"
                                 alt="<?php echo sprintf($repeater->name); ?>"/>
                        </p>
                        <p>
                            <?php echo sprintf($repeater->description); ?>
                        </p>
                        <a title="GET BRAND"
                           href="/brands/<?php echo sprintf($repeater->slug); ?>">
                            GET BRAND
                        </a>
                    </div>
                </div>
            <?php }
            ?>
        <?php }
        ?>
    </div>

    <div class="col-md-12">
        <div class="col-md-12 page-header">
            <h1>Our single malts </h1>
        </div>

        <?php
        foreach ($this->detailSpirits->spirit_products_malts_list as $repeater) { ?>

            <div class="col-md-3">
                <div class="well well-lg" style="min-height: 800px;">
                    <h1>
                        <?php echo sprintf($repeater->name); ?>
                    </h1>
                    <p>
                        <img src="<?php echo sprintf($repeater->image); ?>"
                             class="img-thumbnail"
                             alt="<?php echo sprintf($repeater->name); ?>"/>
                    </p>
                    <p>
                        <?php echo sprintf($repeater->description); ?>
                    </p>
                    <a title="GET BRAND"
                       href="/brands/<?php echo sprintf($repeater->slug); ?>">
                        GET BRAND
                    </a>
                </div>
            </div>

        <?php }
        ?>
    </div>

    <div class="col-md-12">
        <div class="col-md-12 page-header">
            <h1>Our blends </h1>
        </div>

        <?php
        foreach ($this->detailSpirits->spirit_brands_blends_list as $repeater) { ?>

            <div class="col-md-3">
                <div class="well well-lg" style="min-height: 800px;">
                    <h1>
                        <?php echo sprintf($repeater->name); ?>
                    </h1>
                    <p>
                        <img src="<?php echo sprintf($repeater->image); ?>"
                             class="img-thumbnail"
                             alt="<?php echo sprintf($repeater->name); ?>"/>
                    </p>
                    <p>
                        <?php echo sprintf($repeater->description); ?>
                    </p>
                    <a title="GET BRAND"
                       href="/brands/<?php echo sprintf($repeater->slug); ?>">
                        GET BRAND
                    </a>
                </div>
            </div>

        <?php }
        ?>
    </div>

    <div class="col-md-12">
        <div class="col-md-12 page-header">
            <h1>Our single grains</h1>
        </div>

        <?php
        foreach ($this->detailSpirits->spirit_brands_grains_list as $repeater) { ?>

            <div class="col-md-3">
                <div class="well well-lg" style="min-height: 800px;">
                    <h1>
                        <?php echo sprintf($repeater->name); ?>
                    </h1>
                    <p>
                        <img src="<?php echo sprintf($repeater->image); ?>"
                             class="img-thumbnail"
                             alt="<?php echo sprintf($repeater->name); ?>"/>
                    </p>
                    <p>
                        <?php echo sprintf($repeater->description); ?>
                    </p>
                    <a title="GET BRAND"
                       href="/brands/<?php echo sprintf($repeater->slug); ?>">
                        GET BRAND
                    </a>
                </div>
            </div>

        <?php }
        ?>
    </div>


</div>


<?php
//echo 'template 3<br/>';
//echo $page;