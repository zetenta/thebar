<div class="col-md-12 page-header">
    <h1>Whisk(e)y Step 4</h1>
</div>

<div class="col-md-3">
    <ul class="list-group">
        <li class="list-group-item">
            <a href="/spirits/whisky/what-is-whisky">
                What is Whisky
            </a>
        </li>
        <li class="list-group-item">
            <a href="/spirits/whisky/types-of-whisky">
                Types of Whisky
            </a>
        </li>
        <li class="list-group-item">
            <a href="/spirits/whisky/our-brands">
                Our Brands
            </a>
        </li>
        <li class="list-group-item">
            <a href="/spirits/whisky/your-whisky">
                Your Whisky
            </a>
        </li>
    </ul>
</div>

<div class="col-md-9">

    <div class="col-md-12">

        <?php
        foreach ($this->detailSpirits->spirit_detail as $repeater) { ?>

            <?php if ($current_spirit == $repeater->slug) { ?>

                <?php
                foreach ($repeater->spirits_caloric_content as $repeater2) { ?>
                    <div class="col-md-6">
                        <div class="well well-lg" style="min-height: 600px;">
                            <h1>
                                <?php echo sprintf($repeater2->spirits_caloric_title); ?>
                            </h1>
                            <p>
                                <img src="<?php echo sprintf($repeater2->spirits_caloric_icon); ?>"
                                     class="img-thumbnail"
                                     alt="<?php echo sprintf($repeater2->spirits_caloric_title); ?>"/>
                            </p>
                            <p>
                                <?php echo $repeater2->spirits_caloric_description; ?>
                            </p>
                            <h1>
                                <?php echo sprintf($repeater2->spirits_caloric_did_you_know_title); ?>
                            </h1>
                            <h3>
                                <?php echo $repeater2->spirits_caloric_did_you_know_data; ?>
                            </h3>
                            <p>
                                <?php echo $repeater2->spirits_caloric_did_you_know_description; ?>
                            </p>
                        </div>
                    </div>
                <?php }
                ?>

                <?php
                foreach ($repeater->spirits_expiration_date_content as $repeater2) { ?>
                    <div class="col-md-6">
                        <div class="well well-lg" style="min-height: 600px;">
                            <h1>
                                <?php echo sprintf($repeater2->spirits_expiration_date_title); ?>
                            </h1>
                            <p>
                                <img src="<?php echo sprintf($repeater2->spirits_expiration_date_icon); ?>"
                                     class="img-thumbnail"
                                     alt="<?php echo sprintf($repeater2->spirits_expiration_date_title); ?>"/>
                            </p>
                            <p>
                                <?php echo $repeater2->spirits_expiration_date_description; ?>
                            </p>
                            <h1>
                                <?php echo sprintf($repeater2->spirits_expiration_date_did_you_know_title); ?>
                            </h1>
                            <h3>
                                <?php echo $repeater2->spirits_expiration_date_did_you_know_data; ?>
                            </h3>
                            <p>
                                <?php echo $repeater2->spirits_expiration_date_did_you_know_description; ?>
                            </p>
                        </div>
                    </div>
                <?php }
                ?>

            <?php }
            ?>

        <?php }
        ?>

    </div>

    <div class="col-md-12">

        <?php
        foreach ($this->detailSpirits->spirit_detail as $repeater) { ?>

            <?php if ($current_spirit == $repeater->slug) { ?>

                <div class="col-md-12 page-header">
                    <h1><?php echo sprintf($repeater->spirits_timeline_title); ?></h1>
                </div>

                <?php
                foreach ($repeater->spirits_how_do_you_take_your_whiskey as $repeater2) { ?>
                    <div class="col-md-3">
                        <div class="well well-lg" style="min-height: 600px;">
                            <h1>
                                <?php echo sprintf($repeater2->spirits_how_title); ?>
                            </h1>
                            <p>
                                <img src="<?php echo sprintf($repeater2->spirits_how_image); ?>"
                                     class="img-thumbnail"
                                     alt="<?php echo sprintf($repeater2->spirits_how_title); ?>"/>
                            </p>
                            <p>
                                <?php echo $repeater2->spirits_how_description; ?>
                            </p>
                        </div>
                    </div>
                <?php }
                ?>

            <?php }
            ?>

        <?php }
        ?>

    </div>

    <div class="col-md-12 page-header">
        <h1>Whisky Recipes</h1>
    </div>


    <?php
    foreach ($this->templateRecipesAllRecipes2->docs as $repeater) { ?>
        <div class="col-md-3">
            <div class="well well-lg" style="min-height: 500px;">
                <h1>
                    <?php echo sprintf($repeater->recipeTitle); ?>
                </h1>

                <?php
                $arrayTemp = (array)$repeater->SearchImage;
                //var_dump($arrayTemp);
                ?>

                <p>
                    <img src="<?php echo $arrayTemp['original']; ?>"
                         class="img-thumbnail"
                         alt="<?php echo sprintf($repeater->recipeTitle); ?>"/>
                </p>

                <a title="GET RECIPE"
                   href="/recipes/<?php echo sprintf($repeater->recipeSlug); ?>">
                    GET RECIPE
                </a>
            </div>
        </div>
    <?php }
    ?>


</div>

