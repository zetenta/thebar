<?php
//var_dump($this->templateHappyhour); ?>

    <div class="col-md-12 page-header">
        <h1>Happy Hour</h1>
    </div>

    <div class="col-md-4">
        <div class="well well-lg">
            <h2><?php echo sprintf($this->templateHappyhour->hh_title); ?></h2>

            <br/><br/>

            <h3><?php echo sprintf($this->templateHappyhour->hh_subtitle); ?></h3>

            <p>
                <img src="<?php echo sprintf($this->templateHappyhour->hh_imagen_hero); ?>"
                     class="img-thumbnail"
                     alt=""/>
            </p>
        </div>
    </div>

    <div class="col-md-4">
        <div class="well well-lg">
            <h4>
                <?php echo sprintf($this->templateHappyhour->hh_htu_title); ?>
            </h4>
            <p>
                <?php echo sprintf($this->templateHappyhour->hh_htu_instructions); ?>
                <?php echo sprintf($this->templateHappyhour->hh_htu_skills); ?>
            </p>
            <p>
                <img src="<?php echo sprintf($this->templateHappyhour->hh_htu_imagen); ?>"
                     class="img-thumbnail"
                     alt="<?php echo sprintf($this->templateHappyhour->hh_htu_imagen_alt_tags); ?>"/>
            </p>
        </div>
    </div>

    <div class="col-md-4">
        <div class="well well-lg">
            <iframe width="100%" height="320"
                    src="<?php echo sprintf($this->templateHappyhour->hh_video_url); ?>"
                    frameborder="0" allowfullscreen>
            </iframe>
        </div>
    </div>

<?php
//echo 'template 2<br/>';
//echo $page;