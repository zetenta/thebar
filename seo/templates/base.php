<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php include 'meta.php' ?>
</head>
<body>
<div class="container-fluid">
    <div class="row">

        <div class="col-md-3">

            <div id="menu">
                <ul class="list-group">
                    <?php include 'header.php' ?>
                </ul>
            </div>

        </div>

        <div class="col-md-9">

            <?php echo $view ?>

        </div>


    </div>
</div>
</body>
</html>