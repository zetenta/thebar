<div class="col-md-12 page-header">
    <h1>HOME PAGE</h1>
</div>

<br>
<?php
foreach ($this->templateHome->occasions_home_list as $repeater) { ?>
    <div class="col-md-12">
        <div class="well well-lg">
            <h3>OCCASION</h3>
            <h1>
                <?php echo sprintf($repeater->name); ?>
            </h1>
            <p>
                <img src="<?php echo sprintf($repeater->occasion_hero_image); ?>"
                     class="img-thumbnail"
                     alt="<?php echo sprintf($repeater->name); ?>"/>
            </p>
            <p>
                <?php echo sprintf($repeater->occasion_description); ?>
            </p>
            <a title="Explore Occasion"
               href="/occasions/<?php echo sprintf($repeater->slug); ?>">
                Explore Occasion
            </a>
        </div>
    </div>
<?php }
?>

<div class="col-md-12 page-header">
    <h1>HOME LIST</h1>
</div>

<br>
<?php
foreach ($this->recipesInHome->docs as $repeater) { ?>
    <div class="col-md-2">
        <div class="well well-lg" style="min-height: 500px;">
            <h1>
                <?php echo sprintf($repeater->recipeTitle); ?>
            </h1>

            <?php
            $arrayTemp = (array)$repeater->BackgroundImage;
            //var_dump($arrayTemp);
            ?>

            <p>
                <img src="<?php echo $arrayTemp['original']; ?>"
                     class="img-thumbnail"
                     alt="<?php echo sprintf($repeater->recipeTitle); ?>"/>
            </p>
            <p>
                <?php echo sprintf($repeater->variant); ?>
            </p>
            <a title="GET RECIPE"
               href="/recipes/<?php echo sprintf($repeater->recipeSlug); ?>">
                GET RECIPE
            </a>
        </div>
    </div>
<?php }
?>
