<div class="col-md-12 page-header">
    <h1>BRANDS</h1>
</div>

<?php
foreach ($this->templateBrands->slidesBrands as $repeater) { ?>
    <div class="col-md-6">
        <div class="well well-lg" style="min-height: 600px;">
            <h3><?php echo sprintf($repeater->spirit_type); ?></h3>
            <h1>
                <?php echo sprintf($repeater->name); ?>
            </h1>
            <p>
                <img src="<?php echo sprintf($repeater->hero_image); ?>"
                     class="img-thumbnail"
                     alt="<?php echo sprintf($repeater->name); ?>"/>
            </p>
            <p>
                <?php echo sprintf($repeater->description); ?>
            </p>
            <a title="<?php echo sprintf($this->templateBrands->brands_button_name); ?>"
               href="/brands/<?php echo sprintf($repeater->slug); ?>">
                <?php echo sprintf($this->templateBrands->brands_button_name); ?>
            </a>
        </div>
    </div>
<?php }
?>

<div class="col-md-12 page-header">
    <h1><?php echo sprintf($this->templateBrands->brands_section_title); ?></h1>
</div>

<?php
foreach ($this->templateBrands->brandsList as $repeater) { ?>
    <div class="col-md-3">
        <div class="well well-lg" style="min-height: 800px;">
            <h1>
                <?php echo sprintf($repeater->name); ?>
            </h1>
            <p>
                <img src="<?php echo sprintf($repeater->image); ?>"
                     class="img-thumbnail"
                     alt="<?php echo sprintf($repeater->name); ?>"/>
            </p>
            <p>
                <?php echo sprintf($repeater->description); ?>
            </p>
            <a title="<?php echo sprintf($this->templateBrands->brands_button_name); ?>"
               href="/brands/<?php echo sprintf($repeater->slug); ?>">
                <?php echo sprintf($this->templateBrands->brands_button_name); ?>
            </a>
        </div>
    </div>
<?php }
?>

<!--<pre>-->
<?php
//var_dump($this->templateBrands); ?>
<!--</pre>-->