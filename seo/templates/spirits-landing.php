<div class="col-md-12 page-header">
    <h1><?php echo sprintf($this->template->title); ?></h1>
    <h2><?php echo sprintf($this->template->subtitle); ?></h2>
</div>

<?php
foreach ($this->template->spirits as $repeater) { ?>
    <div class="col-md-3">
        <div class="well well-lg" style="min-height: 650px;">
            <h1>
                <?php echo sprintf($repeater->name); ?>
            </h1>
            <p>
                <img src="<?php echo sprintf($repeater->image); ?>"
                     class="img-thumbnail"
                     alt="<?php echo sprintf($repeater->name); ?>"/>
            </p>
            <p>
                <?php echo sprintf($repeater->description); ?>
            </p>

            <?php if ($repeater->slug == 'whisky') { ?>
                <a title="EXPLORE"
                   href="/spirits/<?php echo sprintf($repeater->slug); ?>/what-is-whisky">
                    EXPLORE
                </a>
            <?php } else { ?>
                <a title="EXPLORE"
                   href="/spirits/<?php echo sprintf($repeater->slug); ?>">
                    EXPLORE
                </a>
            <?php } ?>

        </div>
    </div>
<?php }
?>