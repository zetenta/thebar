<?php
//var_dump($this->templateOccasions); ?>

<div class="col-md-12 page-header">
    <h1>Explore Occasions</h1>
    <h2>Find the right drink for any moment</h2>
</div>

<?php
foreach ($this->templateOccasions->hero_occasions as $repeater) { ?>
    <div class="col-md-3">
        <div class="well well-lg" style="min-height: 500px;">
            <h1>
                <?php echo sprintf($repeater->name); ?>
            </h1>
            <p>
                <img src="<?php echo sprintf($repeater->occasion_image); ?>"
                     class="img-thumbnail"
                     alt="<?php echo sprintf($repeater->name); ?>"/>
            </p>
            <p>
                <?php echo sprintf($repeater->occasion_description); ?>
            </p>
            <a title="<?php echo sprintf($this->templateOccasions->occasions_button_get); ?>"
               href="/occasions/<?php echo sprintf($repeater->slug); ?>">
                <?php echo sprintf($this->templateOccasions->occasions_button_get); ?>
            </a>
        </div>
    </div>
<?php }
?>

<div class="col-md-12 page-header">
    <h1>Occasions List</h1>
</div>


<div class="col-md-12">

    <div class="col-md-3">
        <ul class="list-group">
            <?php
            foreach ($this->occasionsListCat as $repeater) { ?>
                <li class="list-group-item">
                    <?php echo sprintf($repeater->name); ?>
                </li>
            <?php }
            ?>
        </ul>
    </div>

    <div class="col-md-9">

        <?php
        foreach ($this->occasionsList->list_occasions as $repeater) { ?>
            <div class="col-md-4">
                <div class="well well-lg" style="min-height: 500px;">
                    <h1>
                        <?php echo sprintf($repeater->name); ?>
                    </h1>
                    <p>
                        <img src="<?php echo sprintf($repeater->image); ?>"
                             class="img-thumbnail"
                             alt="<?php echo sprintf($repeater->name); ?>"/>
                    </p>
                    <p>
                        <?php echo sprintf($repeater->description); ?>
                    </p>
                    <a title="<?php echo sprintf($this->templateOccasions->occasions_button_get); ?>"
                       href="/occasions/<?php echo sprintf($repeater->slug); ?>">
                        <?php echo sprintf($this->templateOccasions->occasions_button_get); ?>
                    </a>
                </div>
            </div>
        <?php }
        ?>

    </div>
</div>


<div class="col-md-12 page-header">
    <h1>Occasions Video List</h1>
</div>

<div class="col-md-12">
    <?php
    foreach ($this->templateOccasions->list_videos as $repeater) { ?>
        <div class="col-md-3">
            <div class="well well-lg" style="min-height: 500px;">
                <h1>
                    <?php echo sprintf($repeater->name); ?>
                </h1>
                <p>
                    <img src="<?php echo sprintf($repeater->video_image); ?>"
                         class="img-thumbnail"
                         alt="<?php echo sprintf($repeater->name); ?>"/>
                </p>

                <a target="_blank" title="VER VIDEO"
                   href="<?php echo sprintf($repeater->video_url); ?>">VER VIDEO</a>

            </div>
        </div>
    <?php }
    ?>
</div>


