<div class="col-md-12 page-header">
    <h1>Occasion Detail</h1>
</div>

<?php
foreach ($this->occasionsList->list_occasions as $repeater) {

    if ($occasion == $repeater->slug) { ?>
        <div class="col-md-12">
            <div class="well well-lg">
                <h1>
                    <?php echo sprintf($repeater->name); ?>
                </h1>
                <p>
                    <img src="<?php echo sprintf($repeater->image); ?>"
                         class="img-thumbnail"
                         alt="<?php echo sprintf($repeater->name); ?>"/>
                </p>
                <p>
                    <?php echo sprintf($repeater->description); ?>
                </p>

            </div>
        </div>
        <?php break;
    }
}
?>

<div class="col-md-12 page-header">
    <h1>BRANDS</h1>
</div>

<?php
foreach ($this->brandDetail->list_brands as $repeater) { ?>
    <div class="col-md-3">
        <div class="well well-lg" style="min-height: 800px;">
            <h1>
                <?php echo sprintf($repeater->name); ?>
            </h1>
            <p>
                <img src="<?php echo sprintf($repeater->image); ?>"
                     class="img-thumbnail"
                     alt="<?php echo sprintf($repeater->name); ?>"/>
            </p>
            <p>
                <?php echo sprintf($repeater->description); ?>
            </p>
            <a title="Learn More"
               href="/brands/<?php echo sprintf($repeater->slug); ?>">
                Learn More
            </a>
        </div>
    </div>
<?php }
?>

<div class="col-md-12 page-header">
    <h1>OUR RECIPES </h1>
</div>

<?php
foreach ($this->OccasionsRecipeIn->docs as $repeater) { ?>
    <div class="col-md-3">
        <div class="well well-lg" style="min-height: 500px;">
            <h1>
                <?php echo sprintf($repeater->recipeTitle); ?>
            </h1>

            <?php
            $arrayTemp = (array)$repeater->SearchImage;
            //var_dump($arrayTemp);
            ?>

            <p>
                <img src="<?php echo $arrayTemp['original']; ?>"
                     class="img-thumbnail"
                     alt="<?php echo sprintf($repeater->recipeTitle); ?>"/>
            </p>

            <a title="GET RECIPE"
               href="/recipes/<?php echo sprintf($repeater->recipeSlug); ?>">
                GET RECIPE
            </a>
        </div>
    </div>
<?php }
?>