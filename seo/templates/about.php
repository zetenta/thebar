<div class="col-md-12 page-header">
    <h1>About</h1>
</div>


<div class="col-md-12">
    <div class="well well-lg">

        <div class="container">

            <div class="row">

                <div id="article-content" class="white_cnt whiteBoxBackground">
                    <div class="container">

                        <p>We don't mean to brag, but thebar.com is simply the best place to be if you're bartending at
                            home. It's where you'll find top recipes for all your favourite drinks, and where you'll
                            discover new drinks you just can't wait to try.</p>

                        <p>thebar.com was created for you, so we've 'gone beyond the glass' to offer you advice on all
                            things cocktail. Whether you're looking for tips on home bar essentials or suggestions for a
                            cozy get-together, this is the place to be if you're serving cocktails at home.</p>

                        <p>We're also the showcase destination for special offers and info from some of the world's
                            favourite spirits, beer and wine brands, including Crown Royal, Johnnie Walker, Captain
                            Morgan, Smirnoff and Guinness.</p>

                        <p>So who created thebar.com? Diageo, the world's leading premium drinks business with an
                            outstanding collection of beverage alcohol brands across spirits, wines and beer categories.
                            These brands include Johnnie Walker, Guinness, Smirnoff, J&amp;B, Baileys, Tanqueray,
                            Captain Morgan, and Crown Royal, as well as Beaulieu Vineyard and Sterling Vineyards
                            wine.</p>

                        <h3>Looking for some more legal text? Have a go at this one:</h3>

                        <p>Diageo is a global company, trading in more than 200 markets around the world. The company is
                            listed on both the New York Stock Exchange (DEO) and the London Stock Exchange (DGE). For
                            more information about Diageo, its people, brands, and performance, visit us at <a
                                    href="http://www.diageo.com/en-row/Pages/default.aspx">http://www.diageo.com</a>.
                            Celebrating life, every day everywhere, responsibly.</p>
                    </div>
                </div>

            </div>

        </div>

    </div>

</div>
