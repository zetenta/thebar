<div class="col-md-12 page-header">
    <h1>RECIPES DRINK TYPE</h1>
</div>

<?php
$drinkTypes[] = '';
foreach ($this->templateRecipesTaxDrinkType->taxonomy_drink_type_list as $taxonomy_drink_type_list) { ?>
    <?php if ($currentdrinktype == $taxonomy_drink_type_list->drink_slug) { ?>
        <div class="col-md-12">
            <div class="well well-lg">
                <h1>
                    <?php echo sprintf($taxonomy_drink_type_list->drink_type); ?>
                </h1>
                <p>
                    <?php echo sprintf($taxonomy_drink_type_list->drink_type_desc); ?>
                </p>

            </div>

        </div>

        <br/><br/>

        <div class="col-md-12">

            <?php
            $tempSlug = $taxonomy_drink_type_list->drink_slug;

            $data = $drinkTypes[$tempSlug]->docs;

            foreach ($data as $recipe) {

                $arrayTemp = (array)$recipe->SearchImage;
                ?>

                <div class="col-md-3">
                    <div class="well well-lg" style="min-height: 500px;">

                        <h1><?php echo $recipe->recipeTitle; ?></h1>

                        <p>
                            <img src="<?php echo $arrayTemp['original']; ?>"
                                 class="img-thumbnail"
                                 alt="<?php echo sprintf($recipe->recipeTitle); ?>"/>
                        </p>

                        <a title="<?php echo sprintf($recipe->recipeSlug); ?>"
                           href="/recipes/<?php echo sprintf($recipe->recipeSlug); ?>">
                            GET RECIPE
                        </a>
                    </div>
                </div>

            <?php }
            ?>
        </div>
    <?php }
    ?>
<?php }
?>


<?php
//echo '<pre>';
//var_dump($this->template);