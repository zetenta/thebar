<?php
//var_dump($this->recipe); ?>

<div class="col-md-12 page-header">
    <h1>RECIPE DETAIL</h1>
</div>

<div class="col-md-12 page-header">
    <h2>RECIPE NAME: <?php echo $this->recipe->recipeTitle; ?></h2>
</div>

<div class="col-md-6">
    <div class="well well-lg">

        <?php
        $arrayTemp = (array)$this->recipe->attributes;
        foreach ($arrayTemp as $repeater) {
            $data = (array)$repeater;
            if (isset($data['DrinkType'])) {
                ?><h2><?php echo $data['DrinkType']; ?></h2><?php
            }
            ?>
        <?php }
        ?>

        <?php $arrayTempImg = (array)$this->recipe->SearchImage; ?>
        <p>
            <img src="<?php echo $arrayTempImg['original']; ?>"
                 class="img-thumbnail"
                 alt="<?php echo $this->recipe->recipeTitle; ?>"/>
        </p>

        <h2><?php echo $this->recipe->brand; ?></h2>

        <?php
        $arrayTempR = (array)$this->recipe->rating;
        ?>
        <h3>Rating: <?php echo $arrayTempR['avgRating']; ?></h3>


        <?php
        $counterDiageoIngredients = 0;
        foreach ($this->recipe->diageoIngredients as $repeater) {
            $counterDiageoIngredients++;
        } ?>
        <?php
        $counterIngredients = 0;
        foreach ($this->recipe->ingredients as $repeater) {
            $counterIngredients++;
        } ?>
        <h4>INGREDIENTS: <?php echo $counterDiageoIngredients + $counterIngredients; ?></h4>

        <?php
        $TempEffor = (array)$this->recipe->attributes;
        foreach ($TempEffor as $repeater) {
            $data = (array)$repeater;
            if (isset($data['EffortLevel'])) {
                ?><h4>DIFFICULTY: <?php echo $data['EffortLevel']; ?></h4><?php
            }
            ?>
        <?php }
        ?>

    </div>
</div>

<div class="col-md-6">
    <div class="well well-lg" style="">
        <h1>Ingredients:</h1>

        <?php
        foreach ($this->recipe->diageoIngredients as $repeater) { ?>
            <p>
                <?php echo sprintf($repeater->qty); ?> <?php if (isset($repeater->uom)) {
                    echo sprintf($repeater->uom);
                } ?>
                --- <?php echo sprintf($repeater->variantName); ?>
            </p>
        <?php }
        ?>
        <?php
        foreach ($this->recipe->ingredients as $repeater) { ?>
            <p>
                <?php echo sprintf($repeater->qty); ?> <?php if (isset($repeater->uom)) {
                    echo sprintf($repeater->uom);
                } ?>
                --- <?php echo sprintf($repeater->name); ?>
            </p>
        <?php }
        ?>
    </div>
</div>

<div class="col-md-12 page-header">
    <br/>
</div>

<div class="col-md-6">
    <div class="well well-lg" style="">

        <div class="col-md-12 page-header">
            <h1>How-To-Mix</h1>
        </div>
        <ul>
            <?php
            foreach ($this->recipe->prepSteps as $repeater) { ?>
                <li>
                    <?php echo sprintf($repeater->stepNo); ?> - <?php echo sprintf($repeater->instruction); ?>
                </li>
            <?php }
            ?>
        </ul>

        <?php
        $arrayTempSlider = (array)$this->recipe->attributes;
        foreach ($arrayTempSlider as $repeater) {
            $data = (array)$repeater;
            if (isset($data['WTBURL'])) {
                ?>
                <br/>
                <br/>
                <div class="col-md-12 page-header">
                    <h1>Video:</h1>
                </div>

                <iframe class="img-thumbnail" src="<?php echo $data['WTBURL']; ?>"></iframe>

                <br/>
                <br/>
                <div class="col-md-12 page-header">
                    <h1>Image Recipe:</h1>
                </div>

                <?php $arrayTempImg2 = (array)$this->recipe->recipeImage; ?>
                <?php if (isset($arrayTempImg2['original'])) {
                    ?>
                    <p>
                        <img src="<?php echo $arrayTempImg2['original']; ?>"
                             class="img-thumbnail"
                             alt="<?php echo $this->recipe->recipeTitle; ?>"/>
                    </p>
                    <?php
                }
                ?>

                <?php
            }
            ?>
        <?php }
        ?>


    </div>
</div>

<div class="col-md-6">
    <div class="well well-lg" style="">
        <div class="col-md-12 page-header">
            <h1>Base Spirit</h1>
        </div>
        <p>
            <img src="<?php echo $this->brandSlugWTB->brandImg; ?>"
                 class="img-thumbnail"
                 alt="<?php echo $this->brandSlugWTB->wtbSlug; ?>"/>
        </p>
        <h5><?php echo $this->recipe->variant; ?></h5>
    </div>
</div>

<?php
//echo '<pre>';
//var_dump($this->recipe); ?>


<div class="col-md-12 page-header">
    <h1>VARIATIONS ON <?php
        $arrayTemp = (array)$this->recipe->attributes;
        foreach ($arrayTemp as $repeater) {
            $data = (array)$repeater;
            if (isset($data['DrinkType'])) {
                ?><?php echo $data['DrinkType']; ?><?php
            }
            ?>
        <?php }
        ?></h1>
</div>

<?php
$currentVariation = '';
$arrayTemp = (array)$this->recipe->attributes;
foreach ($arrayTemp as $repeater) {
    $data = (array)$repeater;
    if (isset($data['DrinkType'])) {
        $currentVariation = $data['DrinkType'];
    }
    ?>
<?php }
?>

<div class="col-md-12">
    <?php
    foreach ($this->tempDrinkType->docs as $repeater) { ?>
        <div class="col-md-3">
            <div class="well well-lg" style="min-height: 500px;">
                <h1>
                    <?php echo sprintf($repeater->recipeTitle); ?>
                </h1>

                <?php
                $arrayTemp = (array)$repeater->SearchImage;
                //var_dump($arrayTemp);
                ?>

                <p>
                    <img src="<?php echo $arrayTemp['original']; ?>"
                         class="img-thumbnail"
                         alt="<?php echo sprintf($repeater->recipeTitle); ?>"/>
                </p>

                <a title="GET RECIPE"
                   href="/recipes/<?php echo sprintf($repeater->recipeSlug); ?>">
                    GET RECIPE
                </a>
            </div>
        </div>
    <?php }
    ?>
</div>
