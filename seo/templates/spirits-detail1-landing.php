<div class="col-md-12 page-header">
    <h1><?php echo sprintf($theSpirit->name); ?></h1>
    <h2>All you need to know</h2>
</div>

<div class="col-md-6">
    <div class="well well-lg" style="min-height: 900px;">
        <h1>
            <?php echo sprintf($theSpirit->name); ?>
        </h1>
        <p>
            <img src="<?php echo sprintf($theSpirit->image); ?>"
                 class="img-thumbnail"
                 alt="<?php echo sprintf($theSpirit->name); ?>"/>
        </p>
        <p>
            <?php echo sprintf($theSpirit->description); ?>
        </p>
    </div>
</div>

<div class="col-md-6">
    <div class="well well-lg" style="min-height: 900px;">
        <?php
        foreach ($this->detailSpirits->spirit_detail as $repeater) { ?>

            <?php if ($current_spirit == $repeater->slug) { ?>

                <?php
                foreach ($repeater->spirits_origin as $repeater2) { ?>
                    <h1>
                        <?php echo sprintf($repeater2->spirits_origin_title); ?>
                    </h1>
                    <h2>
                        <?php echo sprintf($repeater2->spirits_origin_subtitle); ?>
                    </h2>
                    <p>
                        <?php echo sprintf($repeater2->spirits_origin_text); ?>
                    </p>
                    <p>
                        <img src="<?php echo sprintf($repeater2->spirits_origin_image); ?>"
                             class="img-thumbnail"
                             alt="<?php echo sprintf($repeater2->spirits_origin_title); ?>"/>
                    </p>
                <?php }
                ?>


            <?php }
            ?>

        <?php }
        ?>

    </div>
</div>

<div class="col-md-12 page-header">
    <h1>Whisk(e)y Step 1</h1>
</div>

<div class="col-md-3">
    <ul class="list-group">
        <li class="list-group-item">
            <a href="/spirits/whisky/what-is-whisky">
                What is Whisky
            </a>
        </li>
        <li class="list-group-item">
            <a href="/spirits/whisky/types-of-whisky">
                Types of Whisky
            </a>
        </li>
        <li class="list-group-item">
            <a href="/spirits/whisky/our-brands">
                Our Brands
            </a>
        </li>
        <li class="list-group-item">
            <a href="/spirits/whisky/your-whisky">
                Your Whisky
            </a>
        </li>
    </ul>
</div>

<div class="col-md-9">
    <div class="col-md-12">
        <div class="well well-lg">
            <?php
            foreach ($this->detailSpirits->spirit_detail as $repeater) { ?>

                <?php if ($current_spirit == $repeater->slug) { ?>

                    <?php
                    foreach ($repeater->spirits_versus as $repeater2) { ?>
                        <h1>
                            <?php echo sprintf($repeater2->spirits_versus_title); ?>
                        </h1>
                        <p>
                            <?php echo sprintf($repeater2->spirits_versus_text); ?>
                        </p>
                    <?php }
                    ?>


                <?php }
                ?>

            <?php }
            ?>
        </div>
    </div>

    <div class="col-md-6">
        <div class="well well-lg">
            <?php
            foreach ($this->detailSpirits->spirit_detail as $repeater) { ?>

                <?php if ($current_spirit == $repeater->slug) { ?>

                    <?php
                    foreach ($repeater->spirits_ingredients as $repeater2) { ?>
                        <h1>
                            <?php echo sprintf($repeater2->spirits_ingredients_title); ?>
                        </h1>
                        <p>
                            <img src="<?php echo sprintf($repeater2->spirits_ingredients_icon); ?>"
                                 class="img-thumbnail"
                                 alt="<?php echo sprintf($repeater2->spirits_ingredients_title); ?>"/>
                        </p>
                        <p>
                            <?php echo sprintf($repeater2->spirits_ingredients_text); ?>
                        </p>
                    <?php }
                    ?>


                <?php }
                ?>

            <?php }
            ?>

        </div>
    </div>

    <div class="col-md-6">
        <div class="well well-lg">
            <?php
            foreach ($this->detailSpirits->spirit_detail as $repeater) { ?>

                <?php if ($current_spirit == $repeater->slug) { ?>

                    <?php
                    foreach ($repeater->spirits_aging as $repeater2) { ?>
                        <h1>
                            <?php echo sprintf($repeater2->spirits_aging_title); ?>
                        </h1>
                        <p>
                            <img src="<?php echo sprintf($repeater2->spirits_aging_icon); ?>"
                                 class="img-thumbnail"
                                 alt="<?php echo sprintf($repeater2->spirits_aging_title); ?>"/>
                        </p>
                        <p>
                            <?php echo sprintf($repeater2->spirits_aging_text); ?>
                        </p>
                    <?php }
                    ?>


                <?php }
                ?>

            <?php }
            ?>

        </div>
    </div>

    <div class="col-md-12">
        <div class="well well-lg">
            <?php
            foreach ($this->detailSpirits->spirit_detail as $repeater) { ?>

                <?php if ($current_spirit == $repeater->slug) { ?>

                    <?php
                    foreach ($repeater->spirits_where_comes_from as $repeater2) { ?>
                        <h1>
                            <?php echo sprintf($repeater2->spirits_where_comes_from_title); ?>
                        </h1>
                        <p>
                            <?php echo sprintf($repeater2->spirits_where_comes_from_text); ?>
                        </p>
                    <?php }
                    ?>


                <?php }
                ?>

            <?php }
            ?>
        </div>
    </div>

    <div class="col-md-12">
        <div class="well well-lg">
            <?php
            foreach ($this->detailSpirits->spirit_detail as $repeater) { ?>

                <?php if ($current_spirit == $repeater->slug) { ?>

                    <?php
                    foreach ($repeater->spirits_distillaries as $repeater2) { ?>
                        <p>
                            <img src="<?php echo sprintf($repeater2->spirits_distillaries_image); ?>"
                                 class="img-thumbnail"
                                 alt="<?php echo sprintf($repeater2->spirits_distillaries_title); ?>"/>
                        </p>
                        <p>
                            <?php echo sprintf($repeater2->spirits_distillaries_title); ?>
                        </p>
                    <?php }
                    ?>


                <?php }
                ?>

            <?php }
            ?>
        </div>
    </div>
</div>

<?php
//echo 'template 1<br/>';
//echo $page;