<div class="col-md-12 page-header">
    <h1>RECIPES</h1>
</div>

<?php
foreach ($this->templateRecipes->recipes_featured_list as $recipes_featured_list) { ?>
    <div class="col-md-3">
        <div class="well well-lg" style="min-height: 300px;">
            <h3><?php echo sprintf($recipes_featured_list->recipes_drink_type); ?></h3>
            <h1>
                <?php echo sprintf($recipes_featured_list->name); ?>
            </h1>

            <p>
                <?php echo sprintf($recipes_featured_list->recipe_data); ?>
            </p>
            <a title="<?php echo sprintf($recipes_featured_list->name); ?>"
               href="/recipes/<?php echo sprintf($recipes_featured_list->slug); ?>">
                <?php echo sprintf($this->templateRecipes->recipes_button_get_recipe); ?>
            </a>
        </div>
    </div>
<?php }
?>


<div class="col-md-12 page-header">
    <h1><?php echo sprintf($this->templateRecipes->recipes_title_explore_recipes); ?></h1>
</div>

<?php
$drinkTypes[] = '';
foreach ($this->templateRecipesTaxDrinkType->taxonomy_drink_type_list as $taxonomy_drink_type_list) { ?>
    <div class="col-md-12">
        <div class="well well-lg">
            <h1>
                <?php echo sprintf($taxonomy_drink_type_list->drink_type); ?>
            </h1>
            <p>
                <?php echo sprintf($taxonomy_drink_type_list->drink_type_desc); ?>
            </p>
            <a title="<?php echo sprintf($taxonomy_drink_type_list->drink_type); ?>"
               href="/recipes/drink-type/<?php echo sprintf($taxonomy_drink_type_list->drink_slug); ?>">
                SEE ALL
            </a>

        </div>

    </div>

    <br/><br/>

    <div class="col-md-12">

        <?php
        $tempSlug = $taxonomy_drink_type_list->drink_slug;

        $data = $drinkTypes[$tempSlug]->docs;

        foreach ($data as $recipe) {

            $arrayTemp = (array)$recipe->SearchImage;
            ?>

            <div class="col-md-3">
                <div class="well well-lg" style="min-height: 500px;">

                    <h1><?php echo $recipe->recipeTitle; ?></h1>

                    <p>
                        <img src="<?php echo $arrayTemp['original']; ?>"
                             class="img-thumbnail"
                             alt="<?php echo sprintf($recipe->recipeTitle); ?>"/>
                    </p>

                    <a title="<?php echo sprintf($recipe->recipeSlug); ?>"
                       href="/recipes/<?php echo sprintf($recipe->recipeSlug); ?>">
                        GET RECIPE
                    </a>
                </div>
            </div>

        <?php }
        ?>
    </div>
<?php }
?>

<div class="col-md-12 page-header">
    <h1><?php echo sprintf($this->templateRecipes->recipes_title_all_recipes); ?></h1>
</div>


<?php
foreach ($this->templateRecipesAllRecipes->docs as $repeater) { ?>
    <div class="col-md-3">
        <div class="well well-lg" style="min-height: 500px;">
            <h1>
                <?php echo sprintf($repeater->recipeTitle); ?>
            </h1>

            <?php
            $arrayTemp = (array)$repeater->SearchImage;
            //var_dump($arrayTemp);
            ?>

            <p>
                <img src="<?php echo $arrayTemp['original']; ?>"
                     class="img-thumbnail"
                     alt="<?php echo sprintf($repeater->recipeTitle); ?>"/>
            </p>

            <a title="GET RECIPE"
               href="/recipes/<?php echo sprintf($repeater->recipeSlug); ?>">
                GET RECIPE
            </a>
        </div>
    </div>
<?php }
?>

<!--<pre>-->
<!---->
<!--</pre>-->
<!---->
<?php //var_dump($this->templateRecipesAllRecipes); ?>

