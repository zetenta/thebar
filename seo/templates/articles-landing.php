<?php
//var_dump($this->templateArticles); ?>

<div class="col-md-12 page-header">
    <h1><?php echo sprintf($this->templateArticles->articles_article_title); ?></h1>
</div>

<?php
foreach ($this->templateArticles->hero_articles as $repeater) { ?>
    <div class="col-md-3">
        <div class="well well-lg" style="min-height: 700px;">
            <h1>
                <?php echo sprintf($repeater->name); ?>
            </h1>
            <p>
                <img src="<?php echo sprintf($repeater->hero_image); ?>"
                     class="img-thumbnail"
                     alt="<?php echo sprintf($repeater->name); ?>"/>
            </p>
            <p>
                <?php echo sprintf($repeater->description); ?>
            </p>
            <a target="_blank" title="<?php echo sprintf($this->templateArticles->articles_button_get); ?>"
               href="<?php echo sprintf($repeater->article_url); ?>">
                <?php echo sprintf($this->templateArticles->articles_button_get); ?>
            </a>
        </div>
    </div>
<?php }
?>

<div class="col-md-12 page-header">
    <h1><?php echo sprintf($this->templateArticles->articles_list_message); ?></h1>
</div>

<div class="col-md-12">
    <?php
    foreach ($this->templateArticles->list_articles as $repeater) { ?>
        <div class="col-md-3">
            <div class="well well-lg" style="min-height: 700px;">
                <h1>
                    <?php echo sprintf($repeater->name); ?>
                </h1>
                <p>
                    <img src="<?php echo sprintf($repeater->article_image); ?>"
                         class="img-thumbnail"
                         alt="<?php echo sprintf($repeater->name); ?>"/>
                </p>
                <p>
                    <?php echo sprintf($repeater->description); ?>
                </p>
                <a target="_blank" title="<?php echo sprintf($this->templateArticles->articles_button_get); ?>"
                   href="<?php echo sprintf($repeater->article_url); ?>">
                    <?php echo sprintf($this->templateArticles->articles_button_get); ?>
                </a>
            </div>
        </div>
    <?php }
    ?>
</div>

<div class="col-md-12 page-header">
    <h1>Articles Video List</h1>
</div>

<div class="col-md-12">
    <?php
    foreach ($this->templateArticles->list_videos as $repeater) { ?>
        <div class="col-md-3">
            <div class="well well-lg" style="min-height: 500px;">
                <h1>
                    <?php echo sprintf($repeater->name); ?>
                </h1>
                <p>
                    <img src="<?php echo sprintf($repeater->video_image); ?>"
                         class="img-thumbnail"
                         alt="<?php echo sprintf($repeater->name); ?>"/>
                </p>

                <a target="_blank" title="VER VIDEO"
                   href="<?php echo sprintf($repeater->video_url); ?>">VER VIDEO</a>

            </div>
        </div>
    <?php }
    ?>
</div>


