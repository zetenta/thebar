<?php
error_reporting(0);
include 'classSEO.php';
include 'classCacheSEO.php';

$SEO = new seo();
//$_SERVER['QUERY_STRING']= str_replace('recipes','recipe',$_SERVER['QUERY_STRING']);
$file =  $SEO->slugify($_SERVER['QUERY_STRING']).'.html';
$folder = 'cache/';
// UN Día
//SS MM HH DD
    $cacheTTL = 60 * 60* 24 * 2;
//$cacheTTL = 86400;

//$fileName = $folder.$file;
$fileName = dirname(__FILE__).'/'.$folder.$file;
$fileName = str_replace('/','\\', $fileName);

if (file_exists($fileName)) {
    $fileDate = filemtime($fileName);

    date_default_timezone_set('Europe/Berlin');
    $now = strtotime("now");

    $lived = $now - $fileDate;
}
//var_dump(($lived < $cacheTTL));
//echo $folder.$file;
if (file_exists($fileName) && ($lived < $cacheTTL)) {
//    NOS FIJAMOS SI HAY QUE REFRESCARLO
    file_put_contents('create.log','from cache: '. $folder.$file.PHP_EOL, FILE_APPEND);
    echo file_get_contents($fileName);
} else {
    file_put_contents('create.log','to cache: '.$folder.$file.PHP_EOL, FILE_APPEND);
    $cacheSEO = new cacheSEO();
    $urlData = explode('/', $_SERVER['QUERY_STRING']);

    if (!isset($urlData[1]) || $urlData[1] == '' ) {
        //ESTAMOS EN UNA DE LAS URLS PRINCIPALES
        switch ($urlData[0]) {
            case "index.php":
                $result = $cacheSEO->generateHome($urlData[0]);
                break;
            case "recipes" :
                $result = $cacheSEO->generateRecipes($urlData[0]);
                break;
            case "brands" :
                $result = $cacheSEO->generateBrands($urlData[0]);
                break;
            case "occasions" :
                $result = $cacheSEO->generateOccasions($urlData[0]);
                break;
            case "articles" :
                $result = $cacheSEO->generateArticles($urlData[0]);
                break;
            case "search" :
                $result = $cacheSEO->generateSearch($urlData[0]);
                break;
            case "where-to-buy" :
                $result = $cacheSEO->generateWTB($urlData[0]);
                break;
            case "about" :
                $result = $cacheSEO->generateAbout($urlData[0]);
                break;
            case "faq" :
                $result = $cacheSEO->generateFaq($urlData[0]);
                break;
            case "amazon-alexa-happy-hour" :
                $result = $cacheSEO->generateHappyHour($urlData[0]);
                break;
            case "spirits" :
                $result = $cacheSEO->generateSpirits($urlData[0]);
                break;
            default:
                $result = $cacheSEO->generateDefault();
                break;
        }
    } else {
        switch ($urlData[0]) {
            case "recipes" :
                if ($urlData[1] != 'drink-type') {
//                    estamos en el deatlle de un receta
                    $result = $cacheSEO->generateRecipesDetail($urlData[0], $urlData[1] );
                } else {
//                    estamos en el detalle de una categoría de recetas
                    $result = $cacheSEO->generateDrinktype($urlData[0].'/'.$urlData[1], $urlData[2] );
                }
                break;
            case "brands" :
                $result = $cacheSEO->generateBrandDetail($urlData[0], $urlData[1], @$urlData[2] );
                break;
            case "occasions" :
                $result = $cacheSEO->generateOccasionsDetail($urlData[0], $urlData[1] );
                break;
            case "spirits" :
                if ($urlData[1] != 'whisky') {
//                    estamos en un detalla de spirit que no es whisky
                    $result = $cacheSEO->generateSpiritsDetail($urlData[0], $urlData[1] );
                } else {
                    $result = $cacheSEO->generateSpiritsWhiskyDetail($urlData[0], $urlData[1], $urlData[2] );
                }
                break;
            default:
                $result = $cacheSEO->generateDefault();
                break;
        }
//        ESTAMOS EN UN DETALLE O LANDING
//        var_dump($urlData);
    }
    file_put_contents($fileName, $result);
    echo $result;
}

?>
