<?php

/**
 * Created by PhpStorm.
 * User: esteban
 * Date: 6/4/2017
 * Time: 15:49
 */
class cacheSEO
{
//    const apiURL = '/api';
//    const apiURL = 'https://php55.projectsunderdev.com/api-thebar/en-us/wp-json';
    const apiURL = 'https://api.thebar.com/en-us/wp-json';
    const templateFolder = 'templates/';
    const cacheFolder = 'cache/';
//    const siteURL = 'http://thebar-ci.tangomodem.com';
    const siteURL = 'https://us.thebar.com';
    const appID = 'thebar-us';
    const janusUrl = 'https://recipe.diageoapi.com/v1';
    const proxyJanusUrl = 'https://us.thebar.com/api-janus';

    function __construct()
    {

        $this->menu = json_decode(file_get_contents(self::apiURL . '/options/list'));
        $this->templateRecipesAllRecipes = json_decode(file_get_contents(self::proxyJanusUrl . '/recipe/list?appid=' . self::appID . '&limit=99&page=1'));

    }

    function generateDefault()
    {
        $seoData = $this->BuildSeo();
        //$this->templateHome = json_decode(file_get_contents(self::apiURL . '/templates/home-recipes'));

        ob_start();
        require self::templateFolder . 'default.php';
        //armamos la vista
        $view = ob_get_contents();
        ob_end_clean();

        ob_start();
//        armamos la página
        require self::templateFolder . 'base.php';
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
//        file_put_contents(self::cacheFolder . 'index-php.html', $content);
    }

    function generateHome($base)
    {
        $seoData = $this->BuildSeo();
//        echo self::apiURL . '/templates/home-recipes';
        $this->templateHome = json_decode(file_get_contents(self::apiURL . '/templates/home-recipes'));
        $this->recipesInHome = json_decode(file_get_contents(self::janusUrl . '/recipe/list?attributes=Color:homepage&appid=' . self::appID . '&limit=6&page=1'));

        ob_start();
        require self::templateFolder . 'home.php';
//        armamos la vista
        $view = ob_get_contents();
        ob_end_clean();

        ob_start();
//        armamos la página
        require self::templateFolder . 'base.php';
        $content = ob_get_contents();
        ob_end_clean();

        return $content;
//        file_put_contents(self::cacheFolder . $base.'.html', $content);
    }

    function generateRecipes($base)
    {
        $this->templateRecipes = json_decode(file_get_contents(self::apiURL . '/templates/recipes-landing'));
        $this->templateRecipesTaxDrinkType = json_decode(file_get_contents(self::apiURL . '/taxonomy/drink-type-recipes'));

        $temp = $this->templateRecipesTaxDrinkType->taxonomy_drink_type_list;

        $drinkTypes = array();

        foreach ($temp as $docs) {
            $url = self::proxyJanusUrl . '/recipe/list?attributes=DrinkType:' . urlencode($docs->drink_type) . '&limit=9&page=1&appid=' . self::appID;
            $drinkTypes[$docs->drink_slug] = json_decode(file_get_contents($url));
        }

        $seoData = $this->BuildSeo($this->templateRecipes->seoTitle, $this->templateRecipes->seoDescription, $this->templateRecipes->seoKeywords, $base);

        ob_start();
        require self::templateFolder . 'recipes-landing.php';
//        armamos la vista
        $view = ob_get_contents();
        ob_end_clean();

        ob_start();
//        armamos la página
        require self::templateFolder . 'base.php';
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
//        file_put_contents(self::cacheFolder . 'recipes.html', $content);

    }

    function generateBrands($base)
    {
        $this->templateBrands = json_decode(file_get_contents(self::apiURL . '/templates/brands'));
        $seoData = $this->BuildSeo($this->templateBrands->seoTitle, $this->templateBrands->seoDescription, $this->templateBrands->seoKeywords, $base);

        ob_start();
        require self::templateFolder . 'brands.php';
//        armamos la vista
        $view = ob_get_contents();
        ob_end_clean();

        ob_start();
//        armamos la página
        require self::templateFolder . 'base.php';
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
//        file_put_contents(self::cacheFolder . 'recipes.html', $content);

    }

    function generateBrandDetail($base, $brand, $variant = null)
    {


        $this->brandDetail = json_decode(file_get_contents(self::apiURL . '/brands/list'));

        $this->productsList = json_decode(file_get_contents(self::apiURL . '/products/list'));

        $this->brandCurrentDetail = json_decode(file_get_contents(self::apiURL . '/brands/detail?slug=' . $brand));

        $tempBrand = $this->brandCurrentDetail->wtbSlug;

        //var_dump($tempBrand);

        $this->brandRecipeIn = json_decode(file_get_contents(self::proxyJanusUrl . '/recipe/list?brand=' . $tempBrand . '&appid=' . self::appID . '&limit=99&page=1'));

//        var_dump($this->brandRecipeIn);

        file_put_contents('detail.log', 'loading: ' . $brand . PHP_EOL, FILE_APPEND);

        $url = self::janusUrl . '/recipe/list?brand=' . $brand . '&limit=1&appid=' . self::appID;

        //echo $url;
        $temp = file_get_contents($url);

        $current_brand = $brand;
        $current_variant = $variant;

        if ($temp === false) {
            file_put_contents('detail.log', 'failed: ' . $brand . PHP_EOL, FILE_APPEND);
        }
        $this->brand = json_decode($temp);


        $brandSeo = json_decode(file_get_contents(self::apiURL . '/brands/detail?slug=' . $brand));

        $seoData = $this->BuildSeo($brandSeo->seoTitle, $brandSeo->seoDescription, $brandSeo->seoKeywords, $base . '/' . $brand);

        ob_start();
        require self::templateFolder . 'brand-detail.php';

        // armamos la vista
        $view = ob_get_contents();
        ob_end_clean();

        ob_start();

        // armamos la página
        require self::templateFolder . 'base.php';
        $content = ob_get_contents();
        ob_end_clean();

        return $content;

        //  if (file_put_contents(self::cacheFolder . $base. '-'.$recipe.'.html', $content));

    }

    function generateRecipesDetail($base, $recipe)
    {
        file_put_contents('detail.log', 'loading: ' . $recipe . PHP_EOL, FILE_APPEND);

        $temp = file_get_contents(self::janusUrl . '/recipe/' . $recipe . '?limit=1page=1&appid=' . self::appID);

        if ($temp === false) {
            file_put_contents('detail.log', 'failed: ' . $recipe . PHP_EOL, FILE_APPEND);
        }
        $this->recipe = json_decode($temp);

        $currentBrandSlug = $this->recipe->brandSlug[0];
        $this->brandSlugWTB = json_decode(file_get_contents(self::apiURL . '/brands/fromWTB?slug=' . $currentBrandSlug));

        $DrinkTypeTitle = '';
        foreach ($this->recipe->attributes as $attr) {
            $data = array($attr);
            if (isset($data[0]->DrinkType)) {
                $DrinkTypeTitle = $data[0]->DrinkType;
            }
            if (isset($data[0]->SEOPageTitle)) {
                $seoTitle = $data[0]->SEOPageTitle;
            }
            if (isset($data[0]->SEOHints)) {
                $seoKeys = $data[0]->SEOHints;
            }
            if (isset($data[0]->SEOMetaDescription)) {
                $seoDesc = $data[0]->SEOMetaDescription;
            }
        }
        $seoData = $this->BuildSeo($seoTitle, $seoDesc, $seoKeys, $base . '/' . $recipe);

        $temp = file_get_contents(self::janusUrl . '/recipe/list?&appid=' . self::appID . '&attributes=DrinkType:' . urlencode($DrinkTypeTitle));
        $this->tempDrinkType = json_decode($temp);
        $seoData['imgURL'] = $this->recipe->SearchImage->original;
        //echo $seoData['imgURL'];

        ob_start();
        require self::templateFolder . 'recipe-detail.php';
//        armamos la vista
        $view = ob_get_contents();
        ob_end_clean();

        ob_start();
//        armamos la página
        require self::templateFolder . 'base.php';
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
//        if (file_put_contents(self::cacheFolder . $base. '-'.$recipe.'.html', $content));

    }

    function generateArticles($url)
    {
        $this->templateArticles = json_decode(file_get_contents(self::apiURL . '/templates/articles'));

        $seoData = $this->BuildSeo($this->templateArticles->seoTitle, $this->templateArticles->seoDescription, $this->templateArticles->seoKeywords, $url);
        ob_start();
        require self::templateFolder . 'articles-landing.php';
//        armamos la vista
        $view = ob_get_contents();
        ob_end_clean();

        ob_start();
//        armamos la página
        require self::templateFolder . 'base.php';
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
//        file_put_contents(self::cacheFolder . 'recipes.html', $content);

    }

    function generateSearch($url)
    {

        $this->spiritsList = json_decode(file_get_contents(self::apiURL . '/list/spirits'));
        $this->brandsList = json_decode(file_get_contents(self::apiURL . '/brands/list'));
        $this->articlesList = json_decode(file_get_contents(self::apiURL . '/articles/list'));
        $this->occasionsList = json_decode(file_get_contents(self::apiURL . '/occasions/list'));

//        $this->templateArticles = json_decode(file_get_contents(self::apiURL . '/templates/articles'));
        $seoData = $this->BuildSeo(null, null, null, $url);
        ob_start();
        require self::templateFolder . 'search-landing.php';
//        armamos la vista
        $view = ob_get_contents();
        ob_end_clean();

        ob_start();
//        armamos la página
        require self::templateFolder . 'base.php';
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
//        file_put_contents(self::cacheFolder . 'recipes.html', $content);

    }

    function generateWTB($url)
    {
//        $this->templateArticles = json_decode(file_get_contents(self::apiURL . '/templates/articles'));
        $seoData = $this->BuildSeo(null, null, null, $url);
        ob_start();
        require self::templateFolder . 'wtb-landing.php';
//        armamos la vista
        $view = ob_get_contents();
        ob_end_clean();

        ob_start();
//        armamos la página
        require self::templateFolder . 'base.php';
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
//        file_put_contents(self::cacheFolder . 'recipes.html', $content);

    }

    function generateAbout($url)
    {
        $seoData = $this->BuildSeo(null, null, null, $url);
        ob_start();
        require self::templateFolder . 'about.php';

        $view = ob_get_contents();
        ob_end_clean();

        ob_start();

        require self::templateFolder . 'base.php';
        $content = ob_get_contents();
        ob_end_clean();
        return $content;

    }

    function generateFaq($url)
    {
        $seoData = $this->BuildSeo(null, null, null, $url);
        ob_start();
        require self::templateFolder . 'faq.php';

        $view = ob_get_contents();
        ob_end_clean();

        ob_start();

        require self::templateFolder . 'base.php';
        $content = ob_get_contents();
        ob_end_clean();
        return $content;

    }

    function generateHappyHour($base)
    {
        $this->templateHappyhour = json_decode(file_get_contents(self::apiURL . '/templates/happyhour'));

        $seoData = $this->BuildSeo($this->templateHappyhour->seoTitle, $this->templateHappyhour->seoDescription, $this->templateHappyhour->seoKeywords, $base);
        ob_start();
        require self::templateFolder . 'amazon-alexa-happy-hour.php';
//        armamos la vista
        $view = ob_get_contents();
        ob_end_clean();

        ob_start();
//        armamos la página
        require self::templateFolder . 'base.php';
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
//        file_put_contents(self::cacheFolder . 'recipes.html', $content);

    }

    function generateOccasions($base)
    {
        $this->templateOccasions = json_decode(file_get_contents(self::apiURL . '/templates/occasions'));
        $this->occasionsList = json_decode(file_get_contents(self::apiURL . '/occasions/list'));
        $this->occasionsListCat = json_decode(file_get_contents(self::apiURL . '/wp/v2/categories'));

        $seoData = $this->BuildSeo($this->templateOccasions->seoTitle, $this->templateOccasions->seoDescription, $this->templateOccasions->seoKeywords, $base);
        ob_start();
        require self::templateFolder . 'occasions-landing.php';
//        armamos la vista
        $view = ob_get_contents();
        ob_end_clean();

        ob_start();
//        armamos la página
        require self::templateFolder . 'base.php';
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
//        file_put_contents(self::cacheFolder . 'recipes.html', $content);

    }

    function generateOccasionsDetail($base, $occasion)
    {

        $this->brandDetail = json_decode(file_get_contents(self::apiURL . '/brands/list'));
        $this->occasionsList = json_decode(file_get_contents(self::apiURL . '/occasions/list'));

        file_put_contents('detail.log', 'loading: ' . $occasion . PHP_EOL, FILE_APPEND);
        $temp = file_get_contents(self::apiURL . '/occasions/detail?slug=' . $occasion);
        if ($temp === false) {
            file_put_contents('detail.log', 'failed: ' . $occasion . PHP_EOL, FILE_APPEND);
        }
        $this->occasion = json_decode($temp);

        $seoData = $this->BuildSeo($this->occasion->seoTitle, $this->occasion->seoDescription, $this->occasion->seoKeywords, $base . '/' . $occasion);

        $this->OccasionsRecipeIn = json_decode(file_get_contents(self::janusUrl . '/recipe/list?&occasion=' . urlencode($this->occasion->name) . '&appid=' . self::appID . '&limit=40&page=1'));

        //echo self::janusUrl . '/recipe/list?&occasion=' . urlencode($this->occasion->name) . '&appid=' . self::appID . '&limit=40&page=1';

        ob_start();
        require self::templateFolder . 'occasions-detail.php';
//        armamos la vista
        $view = ob_get_contents();
        ob_end_clean();

        ob_start();
//        armamos la página
        require self::templateFolder . 'base.php';
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
//        if (file_put_contents(self::cacheFolder . $base. '-'.$recipe.'.html', $content));

    }

    function BuildSeo($seoTitle = null, $seoDesc = null, $seoKeys = null, $seoUrl = null)
    {
//        DEFAULT SEO
        $siteUrl = self::siteURL . '/' . $seoUrl;
        $title = 'Mixed Drinks and Cocktail Recipes | thebar.com';
        $keywords = 'thebar.com | Mixed Drinks and Cocktail Recipes';
        $description = 'Thebar.com is the ultimate resource for mixed drinks and cocktail recipes. Find drink recipes, tutorials and more for any occasion.';
        $imgUrl = self::siteURL . '/dist/images/logo.png';

        if ($seoTitle) $title = $seoTitle;
        if ($seoDesc) $description = $seoDesc;
        if ($seoKeys) $keywords = $seoKeys;

        $result = array(
            'lang' => 'EN-US',
            'canonicalUrl' => $siteUrl,
            'currentURL' => $siteUrl,
            'title' => $title,
            'keywords' => $keywords,
            'description' => $description,
            'imgURL' => $imgUrl,
            'googleVerif' => 'i3B-wdzbp43lUCfaODCbhoewxA4Y2bKzXO72AEYe5sA',
            'alternate' => array(
                array(
                    'lang' => 'es-ES',
                    'href' => 'https://rowes.thebar.com/' . $seoUrl,
                ),
                array(
                    'lang' => 'en-GB',
                    'href' => 'https://uk.thebar.com/' . $seoUrl,
                ),
                array(
                    'lang' => 'pt-BR',
                    'href' => 'https://rowpt.thebar.com/' . $seoUrl,
                ),
            ),
        );

        return $result;
    }

    function generateDrinktype($base, $drinktype)
    {
        $this->template = json_decode(file_get_contents(self::apiURL . '/templates/recipes-landing'));

        $this->templateRecipesTaxDrinkType = json_decode(file_get_contents(self::apiURL . '/taxonomy/drink-type-recipes'));

        $temp = $this->templateRecipesTaxDrinkType->taxonomy_drink_type_list;

        $drinkTypes = array();

        foreach ($temp as $docs) {
            $url = self::proxyJanusUrl . '/recipe/list?attributes=DrinkType:' . urlencode($docs->drink_type) . '&limit=9&page=1&appid=' . self::appID;
            $drinkTypes[$docs->drink_slug] = json_decode(file_get_contents($url));
        }

        $seoData = $this->BuildSeo($this->template->seoTitle, $this->template->seoDescription, $this->template->seoKeywords, $base . '/' . $drinktype);

        $currentdrinktype = $drinktype;

        ob_start();
        require self::templateFolder . 'drinktype-landing.php';
//        armamos la vista
        $view = ob_get_contents();
        ob_end_clean();

        ob_start();
//        armamos la página
        require self::templateFolder . 'base.php';
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
//        file_put_contents(self::cacheFolder . 'recipes.html', $content);

    }

    function generateSpirits($base)
    {
        $seoData = $this->BuildSeo(null, null, null, $base);

        $this->template = json_decode(file_get_contents(self::apiURL . '/templates/spirits'));

        ob_start();
        require self::templateFolder . 'spirits-landing.php';
//        armamos la vista
        $view = ob_get_contents();
        ob_end_clean();

        ob_start();
//        armamos la página
        require self::templateFolder . 'base.php';
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
//        file_put_contents(self::cacheFolder . 'recipes.html', $content);

    }

    function generateSpiritsDetail($base, $spirit)
    {

        $this->spiritsList = json_decode(file_get_contents(self::apiURL . '/list/spirits'));

        $this->detailSpirits = json_decode(file_get_contents(self::apiURL . '/detail/spirits'));

        $this->brandsList = json_decode(file_get_contents(self::apiURL . '/brands/list'));

        $current_spirit = $spirit;

        $arrSpirits = (array)$this->spiritsList->spirit_list;

        // buscamos el spirit
        $theSpirit = '';
        foreach ($arrSpirits as $row) {
            if ($row->slug == $spirit) {
                $theSpirit = $row;
            }
        }

        //var_dump($theSpirit->name);

        $seoData = $this->BuildSeo($theSpirit->seoTitle, $theSpirit->seoDescription, $theSpirit->seoKeywords, $base . '/' . $spirit);

        $this->templateRecipesAllRecipes3 = json_decode(file_get_contents(self::janusUrl . '/recipe/list?appid=' . self::appID . '&category=' . urlencode($theSpirit->name) . '&limit=16&page=1'));

        ob_start();
        require self::templateFolder . 'spirits-detail-landing.php';
        // armamos la vista
        $view = ob_get_contents();
        ob_end_clean();

        ob_start();
        //        armamos la página
        require self::templateFolder . 'base.php';
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
        //        file_put_contents(self::cacheFolder . 'recipes.html', $content);

    }

    function generateSpiritsWhiskyDetail($base, $spirit, $page)
    {
        $this->spiritsList = json_decode(file_get_contents(self::apiURL . '/list/spirits'));

        $this->detailSpirits = json_decode(file_get_contents(self::apiURL . '/detail/spirits'));

        $this->brandsList = json_decode(file_get_contents(self::apiURL . '/brands/list'));

        $this->productsListFlavor = json_decode(file_get_contents(self::apiURL . '/products/list-flavor'));

        $current_spirit = $spirit;

        $arrSpirits = (array)$this->spiritsList->spirit_list;
//        buscamos el spirit
        $theSpirit = '';
        foreach ($arrSpirits as $row) {
            if ($row->slug == $spirit) {
                $theSpirit = $row;
            }
        }

        $this->templateRecipesAllRecipes2 = json_decode(file_get_contents(self::janusUrl . '/recipe/list?appid=' . self::appID . '&category=' . $theSpirit->name . '&limit=4&page=1'));

        $seoData = $this->BuildSeo($theSpirit->seoTitle, $theSpirit->seoDescription, $theSpirit->seoKeywords, $base . '/' . $spirit . '/' . $page);
        ob_start();
        switch ($page) {
            case 'types-of-whisky' :
                require self::templateFolder . 'spirits-detail2-landing.php';
                break;
            case 'our-brands' :
                require self::templateFolder . 'spirits-detail3-landing.php';
                break;
            case 'your-whisky' :
                require self::templateFolder . 'spirits-detail4-landing.php';
                break;
            default:
                require self::templateFolder . 'spirits-detail1-landing.php';
                break;
        }
//        armamos la vista
        $view = ob_get_contents();
        ob_end_clean();

        ob_start();
//        armamos la página
        require self::templateFolder . 'base.php';
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
//        file_put_contents(self::cacheFolder . 'recipes.html', $content);

    }


}