function runBlock($rootScope, MetaTags) {
    $rootScope.MetaTags = MetaTags;

    //console.log($rootScope.MetaTags);
}

angular
    .module('thebarApp')
    .run(['$rootScope', 'MetaTags', runBlock]);

function configure(UIRouterMetatagsProvider) {
    UIRouterMetatagsProvider
        .setTitlePrefix('')
        .setTitleSuffix('')
        .setDefaultTitle('thebar.com | Mixed Drinks and Cocktail Recipes')
        .setDefaultDescription('Thebar.com is the ultimate resource for mixed drinks and cocktail recipes. Find drink recipes, tutorials and more for any occasion.')
        .setDefaultKeywords('thebar.com, bar.com, bar,  bartender guide, cocktail recipes, drink recipes')
        .setStaticProperties({
            'google-site-verification': config.googleVerif,
        })
        .setOGURL(true);
}

angular
    .module('thebarApp')
    .config(['UIRouterMetatagsProvider', configure]);


app.config(function ($stateProvider, $locationProvider, $urlRouterProvider) {

    function onEnter() {
        //equalHeights
        setTimeout(function () {
            $.fn.equalHeights = function () {
                var max_height = 0;
                $(this).each(function () {
                    max_height = Math.max($(this).height(), max_height);
                });
                $(this).each(function () {
                    $(this).height(max_height);
                });
                $('.spirits-boxes .box-white .block').height(max_height);
            };
            $('.spirits-boxes').equalHeights();

            //ul
            $(".oSelect").on("click", ".init", function () {
                $(this).closest(".oSelect").children('li.noinit').slideDown();
            });

            var allOptions = $(".oSelect").children('li.noinit');
            $(".oSelect").on("click", "li.noinit", function () {
                allOptions.removeClass('selected');
                $(this).addClass('selected');
                $(".oSelect").children('.init').html($(this).html());
                allOptions.slideUp();
            });
        }, 1000);

        setTimeout(function () {
            var scrollTop = 0;
            $('html, body').animate({scrollTop: scrollTop});
            //alert($("div").scrollTop() + " px");
            $('.primaryWrapper #page-content-wrapper').css("visibility", "visible");
        }, 500);


        // ajustar dots de recetas relacionadas (las que ocupan 1/3 de slider)
        $('.recipeslider .slick-dots').addClass('col-md-4');

        // ocultar menú responsive
        $('#wrapper').removeClass('toggled')
    }

    function onExit() {
        //console.log('Entering onExit', this.name);
        document.getElementById("anchorlogo").focus();
    }

    $stateProvider.state('home', {
        name: 'home',
        url: '/',
        templateUrl: 'home.html',
        controller: 'HomeCtrl',
        metaTags: {
            title: 'Home'
        },
        onEnter: onEnter, onExit: onExit
    });

    $stateProvider.state('about', {
        name: 'about',
        url: '/about',
        templateUrl: 'about.html',
        controller: 'AboutCtrl',
        metaTags: {
            title: 'About'
        },
        onEnter: onEnter, onExit: onExit
    });

    $stateProvider.state('sitemap', {
        name: 'sitemap',
        url: '/sitemap',
        templateUrl: 'sitemap.html',
        controller: 'SiteMapCtrl',
        metaTags: {
            title: 'SiteMap'
        },
        onEnter: onEnter, onExit: onExit
    });

    $stateProvider.state('faq', {
        name: 'faq',
        url: '/faq',
        templateUrl: 'faq.html',
        controller: 'FaqCtrl',
        metaTags: {
            title: 'Faq'
        },
        onEnter: onEnter, onExit: onExit
    });

    $stateProvider.state('drink-responsibly', {
        name: 'drink-responsibly',
        url: '/drink-responsibly',
        templateUrl: 'drink-responsibly.html',
        controller: 'DrinkResponsiblyCtrl',
        metaTags: {
            title: 'Drink Responsibly'
        },
        onEnter: onEnter, onExit: onExit
    });

    $stateProvider.state('404', {
        name: '404',
        url: '/404',
        templateUrl: '404.html',
        controller: 'GlobalHeaderSearchFooterCrtl',
        metaTags: {
            title: '404 not Found'
        },
        onEnter: onEnter, onExit: onExit
    });

    $stateProvider.state('articles', {
        name: 'articles',
        url: '/articles',
        templateUrl: 'articles-landing.html',
        controller: 'ArticlesCrtl',
        metaTags: {
            title: 'Articles'
        },
        onEnter: onEnter, onExit: onExit
    });


    $stateProvider.state('happyhour', {
        name: 'happyhour',
        url: '/amazon-alexa-happy-hour',
        templateUrl: 'happy-hour.html',
        controller: 'HappyHourCrtl',
        metaTags: {
            title: 'Happy Hour'
        },
        onEnter: onEnter, onExit: onExit
    });

    $stateProvider.state('occasions', {
        name: 'occasions',
        url: '/occasions',
        templateUrl: 'occasions-landing.html',
        controller: 'OccasionsCrtl',
        metaTags: {
            title: 'Occasions'
        },
        onEnter: onEnter, onExit: onExit
    });

    $stateProvider.state('search', {
        name: 'search',
        url: '/search',
        templateUrl: 'search-landing.html',
        controller: 'GlobalHeaderSearchFooterCrtl',
        metaTags: {
            title: 'Search results'
        },
        onEnter: onEnter, onExit: onExit
    });

    var searchResultState = {
        name: 'searchresult',
        url: '/search-result/{searchText}',
        templateUrl: 'search-landing.html',
        resolve: {
            searchText: ['$stateParams', function ($stateParams) {
                return $stateParams.searchText;
            }]
        },
        metaTags: {
            title: 'Search results'
        },
        controller: 'GlobalHeaderSearchFooterCrtl',
        onEnter: ['searchText', function (searchText) {
            setTimeout(function () {
                $('#searchBox').val(searchText).trigger('change');
                $('.search-navbar .list-inline li:first-child').addClass('activate');

                $('.icon-search-stamina.search a.tisi').addClass('activate');
            }, 1000);
            onEnter();
        }], onExit: onExit
    };

    $stateProvider.state('wtb', {
        name: 'wtb',
        url: '/where-to-buy',
        templateUrl: 'where-to-buy.html',
        controller: 'WTBPageCtrl',
        metaTags: {
            title: 'Where to buy'
        },
        onEnter: onEnter, onExit: onExit
    });

    var recipesState = {
        name: 'recipes',
        url: '/recipes',
        templateUrl: 'recipes-landing.html',
        controller: 'RecipesCtrl',
        metaTags: {
            title: 'Recipes'
        },
        onEnter: onEnter, onExit: onExit
    };

    var recipesLandingState = {
        name: 'recipeslanding',
        url: '/recipes/drink-type/{postRecipesLandingId}',
        templateUrl: 'recipes-landing-detail.html',
        controller: 'RecipesDrinkTypeCtrl',
        resolve: {
            postRecipesLandingId: ['$stateParams', function ($stateParams) {
                return $stateParams.postRecipesLandingId;
            }]
        },
        metaTags: {
            title: 'Recipes'
        },
        onEnter: onEnter, onExit: onExit
    };
    $stateProvider.state(recipesLandingState);

    var recipesDetailState = {
        name: 'recipedetail',
        url: '/recipes/{postRecipesId}',
        templateUrl: 'recipes-detail.html',
        resolve: {
            postRecipesId: ['$location', '$stateParams', function ($location, $stateParams) {
                $('link[rel="canonical"]').attr('href', $location.absUrl());
                return $stateParams.postRecipesId;
            }]
        },
        controller: 'RecipesDetailCtrl',
        onEnter: onEnter,
        onExit: onExit
    };

    var spiritsState = {
        name: 'spirits',
        url: '/spirits',
        templateUrl: 'spirits-landing.html',
        controller: 'SpiritsCtrl',
        onEnter: onEnter, onExit: onExit
    };

    var spiritsDetailState = {
        name: 'spiritdetail',
        url: '/spirits/{postSpiritsId}',
        templateUrl: 'spirits-detail.html',
        resolve: {
            postSpiritsId: ['$stateParams', function ($stateParams) {
                return $stateParams.postSpiritsId;
            }]
        },
        controller: 'SpiritsDetailCtrl',
        onEnter: ["postSpiritsId", "$location", function (postSpiritsId, $location) {
            if (postSpiritsId == 'whisky') {
                $location.path('/spirits/whisky/what-is-whisky');
            }
            onEnter();
        }],
        onExit: onExit

    };

    var spiritsDetailState1 = {
        name: 'spiritdetail1',
        url: '/spirits/whisky/what-is-whisky',
        templateUrl: 'spirits-detail-1.html',
        resolve: {
            postSpiritsId: ['$stateParams', function ($stateParams) {
                return $stateParams.postSpiritsId;
            }]
        },
        data: {
            pageTitle: 'Spirit Detail'
        },
        controller: 'SpiritsDetailCtrl',
        onEnter: onEnter, onExit: onExit
    };

    var spiritsDetailState2 = {
        name: 'spiritdetail2',
        url: '/spirits/whisky/types-of-whisky',
        templateUrl: 'spirits-detail-2.html',
        resolve: {
            postSpiritsId: ['$stateParams', function ($stateParams) {
                return $stateParams.postSpiritsId;
            }]
        },
        data: {
            pageTitle: 'Spirit Detail'
        },
        controller: 'SpiritsDetailCtrl',
        onEnter: onEnter, onExit: onExit
    };

    var spiritsDetailState3 = {
        name: 'spiritdetail3',
        url: '/spirits/whisky/our-brands',
        templateUrl: 'spirits-detail-3.html',
        resolve: {
            postSpiritsId: ['$stateParams', function ($stateParams) {
                return $stateParams.postSpiritsId;
            }]
        },
        data: {
            pageTitle: 'Spirit Detail'
        },
        controller: 'SpiritsDetailCtrl',
        onEnter: onEnter, onExit: onExit
    };

    var spiritsDetailState4 = {
        name: 'spiritdetail4',
        url: '/spirits/whisky/your-whisky',
        templateUrl: 'spirits-detail-4.html',
        resolve: {
            postSpiritsId: ['$stateParams', function ($stateParams) {
                return $stateParams.postSpiritsId;
            }]
        },
        data: {
            pageTitle: 'Spirit Detail'
        },
        controller: 'SpiritsDetailCtrl',
        onEnter: onEnter, onExit: onExit
    };

    var brandsState = {
        name: 'brands',
        url: '/brands',
        controller: 'BrandsCtrl',
        templateUrl: 'brands-landing.html',
        data: {
            pageTitle: 'Brands'
        },
        onEnter: onEnter, onExit: onExit
    };

    var brandsRedirectState = {
        name: 'brandsdetail',
        url: '/brands/{postBrandsId}',
        controller: 'BrandsDetailCtrl',
        templateUrl: 'brands-detail.html',
        resolve: {
            postBrandsId: ['$stateParams', function ($stateParams) {
                return $stateParams.postBrandsId;
            }]
        },
        onEnter: ['postBrandsId', '$http', "$location", function (postBrandsId, $http, $location) {
            $http.get('../detail/brand/' + postBrandsId + '.json').then(function (res) {
                if (res.data.firstProduct != '') {
                    $location.path('/brands/' + postBrandsId + '/' + res.data.firstProduct);
                }
                setTimeout(function () {
                    $('#selectBrands').val(res.data.wtbSlug).trigger('change')
                }, 2000);
            });
        }]
    };

    var brandsDetailState = {
        name: 'brandsRedirect',
        url: '/brands/{postBrandsId}/{postVariantId}',
        controller: 'BrandsDetailCtrl',
        templateUrl: 'brands-detail.html',
        resolve: {
            postBrandsId: ['$stateParams', function ($stateParams) {
                return $stateParams.postBrandsId;
            }],
            postVariantId: ['$stateParams', function ($stateParams) {
                return $stateParams.postVariantId;
            }]
        },
        data: {
            pageTitle: 'Brands Detail'
        },
        onEnter: ['postBrandsId', 'postVariantId', '$http', '$state', "$location", function (postBrandsId, postVariantId, $http, $state, $location) {

            $http.get('../detail/brand/' + postBrandsId + '.json').then(function (res) {
                setTimeout(function () {
                    $('#selectBrands').val(res.data.wtbSlug).trigger('change')
                }, 2000);
            });


        }]
    };

    var occasionsDetail = {
        name: 'occasiondetail',
        url: '/occasions/{postOccasionId}',
        templateUrl: 'ocassion-detail.html',
        resolve: {
            postOccasionId: ['$stateParams', function ($stateParams) {
                return $stateParams.postOccasionId;
            }],
        },
        data: {
            pageTitle: 'Occasion Detail'
        },
        controller: 'OccasionsDetailCrtl',
        onEnter: onEnter,
        onExit: onExit
    };

    var occasionsCategory = {
        name: 'occasionscat',
        url: '/occasions/filter-by/{postOccasionCat}',
        templateUrl: 'occasions-category.html',
        resolve: {
            postOccasionCat: ['$stateParams', function ($stateParams) {
                return $stateParams.postOccasionCat;
            }],
        },
        data: {
            pageTitle: 'Occasion Category'
        },
        controller: 'OccasionsCategoryCrtl',
        onEnter: onEnter,
        onExit: onExit
    };

    var recipesPOCState = {
        name: 'recipesPOC',
        url: '/recipesPOC',
        templateUrl: 'recipesPOC-landing.html',
        controller: 'RecipesPOCCtrl',
        metaTags: {
            title: 'RecipesPOC'
        },
        onEnter: onEnter, onExit: onExit
    };

    var recipesPOCLandingState = {
        name: 'recipesPOClanding',
        url: '/recipesPOC/drink-type/{postRecipesPOCLandingId}',
        templateUrl: 'recipesPOC-landing-detail.html',
        controller: 'RecipesPOCDrinkTypeCtrl',
        resolve: {
            postRecipesPOCLandingId: ['$stateParams', function ($stateParams) {
                return $stateParams.postRecipesPOCLandingId;
            }]
        },
        metaTags: {
            title: 'RecipesPOC'
        },
        onEnter: onEnter, onExit: onExit
    };
    $stateProvider.state(recipesPOCLandingState);

    var recipesPOCDetailState = {
        name: 'recipePOCdetail',
        url: '/recipesPOC/{postRecipesPOCId}',
        templateUrl: 'recipesPOC-detail.html',
        resolve: {
            postRecipesPOCId: ['$location', '$stateParams', function ($location, $stateParams) {
                $('link[rel="canonical"]').attr('href', $location.absUrl());
                return $stateParams.postRecipesPOCId;
            }]
        },
        controller: 'RecipesPOCDetailCtrl',
        onEnter: onEnter,
        onExit: onExit
    };

    $stateProvider.state(spiritsState);

    $stateProvider.state(spiritsDetailState);

    $stateProvider.state(spiritsDetailState1);

    $stateProvider.state(spiritsDetailState2);

    $stateProvider.state(spiritsDetailState3);

    $stateProvider.state(spiritsDetailState4);

    $stateProvider.state(brandsState);

    $stateProvider.state(brandsDetailState);

    $stateProvider.state(brandsRedirectState);

    $stateProvider.state(recipesState);

    $stateProvider.state(recipesDetailState);

    $stateProvider.state(occasionsDetail);

    $stateProvider.state(occasionsCategory);

    $stateProvider.state(searchResultState);

    $stateProvider.state(recipesPOCState);

    $stateProvider.state(recipesPOCDetailState);

    // use the HTML5 History API
    $locationProvider.html5Mode(true);
    // the known route, with missing '/' - let's create alias
    $urlRouterProvider.when('', '/');
    // redirect any unmatched url to 404 view (without change location.hash)
    $urlRouterProvider.otherwise(function ($injector) {
        var $state = $injector.get('$state');
        $state.go('404', null, {
            location: false
        });
    });

});

app.run(function ($rootScope, $location, $state) {
    $rootScope.$state = $state;

    $rootScope.$watch(function () {

            //console.log(ipCookie('pass_agp'));
            // bind the listener
            // listenCookieChange('pass_agp', function () {
            //
            //     console.log('cookie pass_agp has changed!');
            //
            //     var cookieAge = getCookie("pass_agp");
            //     if (cookieAge == "pass_agp_true") {
            //         console.log('Push pageView');
            //         window.dataLayer = window.dataLayer || [];
            //         window.dataLayer.push({
            //             'event': 'pageView',
            //             'pagePath': window.location.href
            //         });
            //     } else {
            //         console.log('Push Diageo-gateway');
            //     }
            //
            // });

            //return $location.absUrl();
        },
        function (a) {

            updateTwitterAndLDJSON();

            //console.log('url has changed: ' + a);
            console.log('url has changed: ' + a);

            var cookieAge = getCookie("diageo-gateway");
            if (cookieAge != "") {
                console.log('Push pageView');
                window.dataLayer = window.dataLayer || [];
                window.dataLayer.push({
                    'event': 'pageView',
                    'pagePath': a
                });
            } else {
                console.log('Push Diageo-gateway');
            }

            // var cookieAge = getCookie("pass_agp");
            // if (cookieAge == "pass_agp_true") {
            //     console.log('Push pageView');
            //     window.dataLayer = window.dataLayer || [];
            //     window.dataLayer.push({
            //         'event': 'pageView',
            //         'pagePath': a
            //     });
            // }

            setTimeout(function () {
                var scrollTop = 0;
                $('html, body').animate({scrollTop: scrollTop});
                //alert($("div").scrollTop() + " px");

                $('.primaryWrapper #page-content-wrapper').css("visibility", "visible");

                var summonerheight = ($(window).height() - 250);
                $('.search-page-scroll .results').css("height", summonerheight + "px");

                var keydowncode = 0;
                $("body").on('keydown', function (e) {
                    var keyCode = e.keyCode || e.which;
                    if (keyCode === 9 && keydowncode === 0) {
                        e.preventDefault();
                        //console.log('Tab Pressed');
                        $("a.wb-sl").focus();
                        keydowncode = 1;
                        //console.log(keydowncode);
                    }
                });

                $("a.wb-sl").click(function (event) {
                    event.preventDefault();
                    //alert($("div").scrollTop() + " px");
                    // Make sure this.hash has a value before overriding default behavior
                    if (this.hash !== "") {
                        // Prevent default anchor click behavior
                        event.preventDefault();

                        // Store hash
                        var hash = this.hash;

                        // Using jQuery's animate() method to add smooth page scroll
                        // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                        $('html, body').animate({
                            scrollTop: $(hash).offset().top
                        }, 800, function () {

                            // Add hash (#) to URL when done scrolling (default click behavior)
                            window.location.hash = hash;
                        });
                    } // End if
                });

            }, 800);
            // show loading div, etc...
        });
});
/**
 * Created by PC on 23/03/2017.
 */
