app.filter('startFrom', function () {
    return function (input, start) {
        if (input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
});

app.filter('countIf', function () {
    return function (array, search) {

        var count = 0;
        for (var i in array) {
            if (array[i].name.indexOf(search) != -1)
                count++
        }

        return count;
    }
});

// LIGHTBOX HOW TO

app.filter('to_trusted', ['$sce', function ($sce) {
    return function (text) {
        return $sce.trustAsHtml(text);
    };
}]);

app.filter("GetYouTubeID", function ($sce) {
    return function (text) {
        var video_id = text.split('embed/')[1].split('&')[0];
        return video_id;
    }
});

app.filter('customISOChangeDurationYoutubeDuration', function () {
    return function (input) {
          if (!(input)) {
            return input;
        } else {
            var duration = input;
            var a = duration.match(/\d+/g);
            if (duration.indexOf('M') >= 0 && duration.indexOf('H') == -1 && duration.indexOf('S') == -1) {
                a = [0, a[0], 0];
            }
            if (duration.indexOf('H') >= 0 && duration.indexOf('M') == -1) {
                a = [a[0], 0, a[1]];
            }
            if (duration.indexOf('H') >= 0 && duration.indexOf('M') == -1 && duration.indexOf('S') == -1) {
                a = [a[0], 0, 0];
            }
            duration = 0;
            if (a.length == 3) {
                duration = duration + parseInt(a[0]) * 3600;
                duration = duration + parseInt(a[1]) * 60;
                duration = duration + parseInt(a[2]);
            }
            if (a.length == 2) {
                duration = duration + parseInt(a[0]) * 60;
                duration = duration + parseInt(a[1]);
            }
            if (a.length == 1) {
                duration = duration + parseInt(a[0]);
            }
            var h = Math.floor(duration / 3600);
            var m = Math.floor(duration % 3600 / 60);
            var s = Math.floor(duration % 3600 % 60);
            var r = (((h > 0 ? h + ":" + (m < 10 ? "0" : "") : "") + m + ":" + (s < 10 ? "0" : "") + s));

            return r;
        }
    }
});