var app = angular.module("thebarApp", ['slick', 'angular.filter', 'ui.router', 'ui.bootstrap', 'angular-loading-bar', 'ngAnimate', 'matchMedia', 'ui.router.metatags', 'updateMeta'])
    .config(['cfpLoadingBarProvider', function (cfpLoadingBarProvider) {
        cfpLoadingBarProvider.parentSelector = '#loading-bar-container';
        cfpLoadingBarProvider.spinnerTemplate = '<div id="loader"><div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div></div>';
    }]);

// var app = angular.module("thebarApp", ['slick', 'angular.filter', 'ui.router', 'ui.bootstrap', 'angular-loading-bar', 'ngAnimate', 'matchMedia', 'ui.router.metatags']);

