var distance = 5;
var variant = 'baileys-original';

$(document).wtbInitialize({
    appid: "tango-dev",
    diageo_api_url: "https://api.diageoapi.com/wtb/",
    locale: "EN-US",
    defaultLat: 0,
    defaultLon: 0,
    defaultZoom: 12,
    refreshOnDrag: false,
    animation: true,
    eComm: true,
    markerURL: 'dist/images/marker.png',
    markerActiveURL: 'dist/images/marker-active.png',
    numbers: true,
    locations: {
        locationsList: '#locationsList',
        loader: '#loaderLocations',
        distance: distance,
        variant: variant,
        uom: 'mi',
        loctype: $('#selectLoctype').val()
    }
});

$("input[type=checkbox]").on("click", function () {

    var arr = [];

    $("input[type=checkbox]:checked").each(function () {
        arr.push($(this).val());
    });

    /*       console.log(arr);*/

    $('#selectLoctype').val(arr);
    $('#selectVariants').trigger('change');


});


$('#selectBrands').getBrandsFromAPI({
    loader: '#loaderBrands'
});

$('#selectVariants').getVariantsFromAPI({
    loader: '#loaderVariants',
    brand: $("#selectBrands").val()
});


$("#btnLoadBrands").on('click', function () {
    $('#selectBrands').getBrandsFromAPI({
        loader: '#loaderBrands'
    });
});

$('#selectBrands').change(function () {
    $('#selectVariants').getVariantsFromAPI({
        loader: '#loaderVariants',
        brand: $(this).val()
    });
});

$('#selectVariants').change(function () {

    temp = $('#selectLoctype').val();
    if (temp.length > 1) temp = '';

    $('#locations').hide();
    $('.map-wrapper').removeClass('col-xs-12 col-sm-8 col-md-8 col-lg-8');
    $('#locationsList').getLocationsFromAPI({
        loader: '#loaderLocations',
        variant: $(this).val(),
        distance: distance,
        uom: 'mi',
        loctype: temp
    });
});

$('.distance').on('click', function () {
    $(".distance").css("color", "#fff");
    $(this).css("color", "#0cf");
    var d = $(this).attr("id");
    distance = d.replace("dist-", "");
    $('#selectVariants').trigger('change');
});
