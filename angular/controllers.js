app.controller("GlobalHeaderSearchFooterCrtl", ["$scope", "$http", "screenSize", "$stateParams", "$location", "$rootScope", "$window", "anchorSmoothScroll", function ($scope, $http, screenSize, $stateParams, $location, $rootScope, $window, anchorSmoothScroll) {

    // console.log(':::::::::::::::::::::::::::::::::::::::::::::::: Entro a GlobalHeaderSearchFooterCrtl');

    // given URL http://example.com/#/some/path?foo=bar&baz=xoxo
    $scope.absUrl = $location.absUrl();
    $scope.aWebUrl = window.location.href;
    //alert($scope.absUrl);

    $scope.gotoElement = function (eID) {
        // set the location.hash to the id of
        // the element you wish to scroll to.
        $location.hash('bottom');

        // call $anchorScroll()
        anchorSmoothScroll.scrollTo(eID);

    };

    // definir responsive if
    $scope.desktop = screenSize.is('lg');
    $scope.mobile = screenSize.is('xs, sm, md');
    $scope.desktop = screenSize.on('lg', function (match) {
        $scope.desktop = match;
    });
    $scope.mobile = screenSize.on('xs, sm, md', function (match) {
        $scope.mobile = match;
    });

    // slick settings
    $scope.miniGalleryResponsive = [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 980,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 800,
            settings: {
                dots: false,
                arrows: true,
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 540,
            settings: {
                dots: false,
                arrows: true,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ];
    $scope.miniGalleryResponsiveDots = [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 980,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 800,
            settings: {
                dots: true,
                arrows: false,
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 540,
            settings: {
                dots: true,
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ];
    $scope.bigSlider = [
        {
            breakpoint: 1200,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 800,
            settings: {
                dots: true,
                arrows: false,
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 540,
            settings: {
                dots: true,
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ];
    $scope.customDots = '<span class="dot"></span>';
    $scope.nextArrow = '<i class="fa fa-arrow-right"></i>';
    $scope.prevArrow = '<i class="fa fa-arrow-left"></i>';

    // GLOBAL SEO CONFIG
    angular.extend($scope, buildScopeSEO(config.SEOglobal, $location));

    // overlay
    $scope.shows = false;
    $scope.isActive = false;
    $scope.searchActive = false;

    var overlay = angular.element(document.querySelector('#overlay'));

    $scope.hideOverlay = function () {
        $scope.isActive = false;
        $scope.searchActive = false;
        $scope.shows = false;
    };

    $scope.status = function () {
        $scope.searchActive = false;
        $scope.shows = false;
        $scope.isActive = !$scope.isActive;
    };


    // search
    $scope.myVar = false;
    $scope.toggleTeamField = function () {
        $scope.myVar = !$scope.myVar;
        $scope.newteamfocus = false;
        $scope.searchKeyword = "";
        $scope.focus = !$scope.focus;
    };

    $scope.showSearch = function () {
        //alert('11111111111');
        $('.search-page').toggleClass("active");
        $('.icon-search-stamina.search').toggleClass("open");
        $('.icon-search-stamina.search a.tisi').toggleClass("active");
        $('.search-page .close-search').toggleClass("active");
        $scope.isActive = false;
        setTimeout(function () {
            $("#searchInput2").focus();
        }, 1000);
    };

    //showHideSearch
    $scope.showHideSearch = function () {
        //alert('22222222222222222');
        $('.search-page').removeClass("active");
        $('.icon-search-stamina.search').removeClass("open");
        $('.icon-search-stamina.search a.tisi').removeClass("active");
        $('.search-page .close-search').removeClass("active");
        $scope.isActive = false;
    };

    //SEARCH CALLS
    $scope.postSearchId = '';
    $scope.postSearchId = $stateParams.postSearchId;

    $scope.searchscroll = function () {
        var scrollTop = 0;
        $('html, body').animate({scrollTop: scrollTop}, 'slow');
    };

    // //anchorSmoothScroll
    // $scope.gotoElement = function (eID) {
    //     $location.hash('bottom');
    //     anchorSmoothScroll.scrollTo(eID);
    // };

    $http({
        method: 'GET',
        url: config.apiJanusUrl + '/recipe/list?appid=thebar-us&limit=380&short=true'
        //01

    }).success(function (data) {
        $scope.allrecipesjson = data;
    }).error(function () {
        $('#myModalAlert').modal('show');

        $http({
            method: 'GET',
            url: config.apiJanusUrl + '/recipe/list?appid=thebar-us&limit=99'
            //01

        }).success(function (data) {
            $scope.allrecipesjson = data;
        }).error(function () {
            $('#myModalAlert').modal('show');
        });

    });

    $http({
        method: 'GET',
        url: './list/spirits.json'
        //02

    }).success(function (data) {
        $scope.searchresultspirits = data;

        // console.log(':::::::: Get HTTP en GlobalHeaderSearchFooterCrtl de: list/spirits');
        // console.log(':::: Resultado de = $scope.searchresultspirits: ');
        // console.log($scope.searchresultspirits);
    });

    $http({
        url: './list/brands.json'
        //03

    }).then(function (res) {
        $scope.searchresultbrands = res.data;

        // console.log(':::::::: Get HTTP en GlobalHeaderSearchFooterCrtl de: brands/list');
        // console.log(':::: Resultado de = $scope.searchresultbrands: ');
        // console.log($scope.searchresultbrands);
    });

    $http({
        url: './articles/list.json'
        //04

    }).then(function (res) {
        $scope.searchresultarticles = res.data;

        // console.log(':::::::: Get HTTP en GlobalHeaderSearchFooterCrtl de: articles/list');
        // console.log(':::: Resultado de = $scope.searchresultarticles: ');
        // console.log($scope.searchresultarticles);
    });

    $http({
        url: './occasions/list/json'
        //05

    }).then(function (res) {
        $scope.searchresultoccasions = res.data;

        // console.log(':::::::: Get HTTP en GlobalHeaderSearchFooterCrtl de: occasions/list');
        // console.log(':::: Resultado de = $scope.searchresultoccasions: ');
        // console.log($scope.searchresultoccasions);
    });

    $http({
        method: 'GET',
        url: './list/options-list.json'
        //06

    }).success(function (data) {

        $scope.options_list = data;

        // console.log(':::::::: Get HTTP en GlobalHeaderSearchFooterCrtl de: options/list');
        // console.log(':::: Resultado de = $scope.options_list: ');
        // console.log($scope.options_list);

    }).error(function () {
        $('#myModalAlert').modal('show');
    });


    // menu toogle
    $scope.menuToggle = function () {
        $("#wrapper").toggleClass("toggled");
    };


    // scroll to top
    $scope.clickactive = function (idterm) {
        //alert(idterm);
        $('.search-navbar .list-inline li').removeClass('activate');
        if (idterm == 6) {
            $('.search-navbar .list-inline li#iad6').addClass('activate');
        }
        if (idterm == 7) {
            $('.search-navbar .list-inline li#iad7').addClass('activate');
        }
        if (idterm == 8) {
            $('.search-navbar .list-inline li#iad8').addClass('activate');
        }
        if (idterm == 9) {
            $('.search-navbar .list-inline li#iad9').addClass('activate');
        }
        if (idterm == 10) {
            $('.search-navbar .list-inline li#iad10').addClass('activate');
        }
    };

}]);

app.controller("HomeCtrl", ["$scope", "$http", "$stateParams", "screenSize", "$modal", "$log", "$location", function ($scope, $http, $stateParams, screenSize, $modal, $log, $location) {

    // console.log('::::::::::::::::::::::::::::::::::::::::::::::::  Entro a HomeCtrl');

    // PETICION 01
    $http({
        method: 'GET',
        url: './templates/home-recipes.json'
        //07

    }).success(function (data) {
        $scope.homerecipes = data;
        angular.extend($scope, buildScopeSEO($scope.homerecipes.home_recipes_template[0], $location));

        // console.log(':::::::: Get HTTP en HomeCtrl de: template/home-recipes');
        // console.log(':::: Resultado de = $scope.homerecipes: ');
        // console.log($scope.homerecipes);
        // console.log(':::: Resultado de = $scope.homerecipes.fburl: ');
        // console.log($scope.homerecipes.fburl);

    }).error(function () {
        $('#myModalAlert').modal('show');
    });

    // PETICION 02
    $http({
        method: 'GET',
        url: config.janusUrl + '/recipe/list?attributes=Color:homepage&appid=thebar-us&limit=6&page=1'
        //08

    }).success(function (data) {
        $scope.recipesjsoninhome = data;

        // console.log(':::::::: Get HTTP en HomeCtrl de: list?attributes=Color:homepage&appid=thebar-us&limit=6&page=1');
        // console.log(':::: Resultado de Las 6 Recetas de Home = $scope.recipesjsoninhome:');
        // console.log($scope.recipesjsoninhome);

    }).error(function () {
        $('#myModalAlert').modal('show');
    });


}]);

app.controller("AboutCtrl", ["$scope", "$http", "$stateParams", "screenSize", "$modal", "$log", "$location", function ($scope, $http, $stateParams, screenSize, $modal, $log, $location) {

    // console.log('::::::::::::::::::::::::::::::::::::::::::::::::  Entro a AboutCtrl');

}]);

app.controller("HappyHourCrtl", ["$scope", "$http", "$stateParams", "screenSize", "$modal", "$log", "$location", function ($scope, $http, $stateParams, screenSize, $modal, $log, $location) {

    // console.log('::::::::::::::::::::::::::::::::::::::::::::::::  Entro a HappyHourCrtl');

    // peticiones
    $http({
        method: 'GET',
        url: './templates/happyhour.json'
        //09

    }).success(function (data) {

        $scope.happyhour = data;

        //console.log($scope.happyhour);

        angular.extend($scope, buildScopeSEO($scope.happyhour, $location));

    }).error(function () {
        $('#myModalAlert').modal('show');
    });

}]);

app.controller("FaqCtrl", ["$scope", "$http", "$stateParams", "screenSize", "$modal", "$log", "$location", function ($scope, $http, $stateParams, screenSize, $modal, $log, $location) {

    // console.log('::::::::::::::::::::::::::::::::::::::::::::::::  Entro a FaqCtrl');

}]);

app.controller("DrinkResponsiblyCtrl", ["$scope", "$http", "$stateParams", "screenSize", "$modal", "$log", "$location", function ($scope, $http, $stateParams, screenSize, $modal, $log, $location) {

    // console.log('::::::::::::::::::::::::::::::::::::::::::::::::  Entro a DrinkResponsiblyCtrl');

}]);

app.controller("SiteMapCtrl", ["$scope", "$http", "$stateParams", "screenSize", "$modal", "$log", "$location", function ($scope, $http, $stateParams, screenSize, $modal, $log, $location) {

    // console.log('::::::::::::::::::::::::::::::::::::::::::::::::  Entro a SiteMapCtrl');

    $http.get("sitemap.xml",
        {
            transformResponse: function (cnv) {
                var x2js = new X2JS();
                var aftCnv = x2js.xml_str2json(cnv);
                return aftCnv;
            }
        })
        .success(function (response) {
            $scope.sitemap = response.urlset;
            console.log(response);
        });


}]);

app.controller("RecipesCtrl", ["$scope", "$http", "$stateParams", "$location", function ($scope, $http, $stateParams, $location) {

    //console.log(':::::::::::: Entro a RecipesCtrl');

    // PETICIONES

    //myDropDownSelect
    $scope.myDropDownSelect = 'one';

    // armar url con parametros
    $scope.postRecipesLandingId = '';
    $scope.postRecipesLandingId = $stateParams.postRecipesLandingId;


    // PETICION 03
    $http({
        method: 'GET',
        url: config.apiJanusUrl + '/recipe/list?appid=thebar-us&limit=99&page=1'
        //10

    }).success(function (data) {
        $scope.recipesjson = data;

        // console.log(':::::::: Get HTTP en RecipesCtrl de: recipe/list?appid=thebar-us&limit=99page=1');
        // console.log(':::: Resultado de = $scope.recipesjson: ');
        // console.log($scope.recipesjson);

        //PETICION EXACTA SLUG DE UNA RECETA
        arrayFeaturedRecipes = [];
        slugFR = '';
        $http({
            url: './templates/recipes-landing.json'
            //11

        }).then(function (res) {
			$scope.recipeslandingfeaturedrecipes = res.data;

            if ($scope.recipeslandingfeaturedrecipes.recipes_featured_list.length) {
                for (var i = 0; i < $scope.recipeslandingfeaturedrecipes.recipes_featured_list.length; i++) {
                    arrayFeaturedRecipes.push({
                        slugfr: $scope.recipeslandingfeaturedrecipes.recipes_featured_list[i].slug
                    });
                    filterFR(arrayFeaturedRecipes[i].slugfr);
                    //alert('slugfr: ' + slugfr);
                }
            }

            function filterFR(slugFR) {
                $http({
                    url: config.janusUrl + '/recipe/' + slugFR + '?appid=thebar-us&limit=1page=1',
                    //12
                    cache: false
                }).then(function (res) {
                    $scope.recipesjson[slugFR] = res.data;

                    // console.log(':::::::: Get HTTP en RecipesCtrl de: recipe/slugFR?appid=thebar-us&limit=1page=1');
                    // console.log(':::: Resultado de = $scope.recipesjson[slugFR]: ');
                    // console.log($scope.recipesjson[slugFR]);
                });
            }
        });


        //PETICION DINAMICA DRINK TYPE
        arrayDrinkType = [];
        nameFilterDrinkType = '';
        slugFilterDrinkType = '';
        $http({
            url: './taxonomy/drink-type-recipes.json'
            //12

        }).then(function (res) {

            $scope.taxonomydrinktyperecipes = res.data;

            // console.log(':::::::: Get HTTP en RecipesCtrl de: taxonomy/drink-type-recipes');
            // console.log(':::: Resultado de = $scope.taxonomydrinktyperecipes: ');
            // console.log($scope.taxonomydrinktyperecipes);

            for (var i = 0; i < $scope.taxonomydrinktyperecipes.taxonomy_drink_type_list.length; i++) {
                arrayDrinkType.push({
                    drink: $scope.taxonomydrinktyperecipes.taxonomy_drink_type_list[i].drink_type,
                    drinkslug: $scope.taxonomydrinktyperecipes.taxonomy_drink_type_list[i].drink_slug
                });
                if ($scope.taxonomydrinktyperecipes.taxonomy_drink_type_list[i].drink_slug == $stateParams.postRecipesLandingId) {
                    angular.extend($scope, buildScopeSEO($scope.taxonomydrinktyperecipes.taxonomy_drink_type_list[i], $location));
                }
                filterDrinkType(arrayDrinkType[i].drink, arrayDrinkType[i].drinkslug);

            }

            function filterDrinkType(nameFilterDrinkType, slugFilterDrinkType) {
                $http({
                    url: config.apiJanusUrl + '/recipe/list?appid=thebar-us&attributes=DrinkType:' + nameFilterDrinkType + '&limit=9&page=1'
                    //13

                }).then(function (res) {

                    $scope.recipesjson[slugFilterDrinkType] = res.data;

                    // console.log(':::::::: Get HTTP en RecipesCtrl de: recipe/list?appid=thebar-us&attributes=DrinkType:nameFilterDrinkType&limit=9&page=1');
                    // console.log(':::: Resultado de = $scope.recipesjson[slugFilterDrinkType]: ');
                    // console.log($scope.recipesjson[slugFilterDrinkType]);
                });
            }
        });

    }).error(function () {
        $('#myModalAlert').modal('show');
    });

    // PETICION 04
    $http({
        method: 'GET',
        url:  './templates/recipes-landing.json'
        //14

    }).success(function (data) {
        $scope.recipeslanding = data;

        // angular.extend($scope, buildScopeSEO($scope, $location));
        // console.log('asdfasdf');
        // $scope.recipeslanding.fburl = $location.absUrl();
        // document.querySelector("link[rel='canonical']").setAttribute("href", $location.absUrl());
        //console.log($scope.recipeslanding);
    }).error(function () {
        $('#myModalAlert').modal('show');
    });


    //LOAD MORE
    $scope.limitloadmorebrandsrec2 = 8;
    $scope.loadMore12 = function () {
        $scope.limitloadmorebrandsrec2 = 9999;
    };
    $scope.isDisabled2 = false;
    $scope.disableClick12 = function () {
        $scope.isDisabled2 = true;
        return false;
    };

    $scope.limitloadmorebrandsrec = 8;
    $scope.loadMore82 = function () {
        $scope.limitloadmorebrandsrec = 9999;
    };
    $scope.isDisabled = false;
    $scope.disableClick82 = function () {
        $scope.isDisabled = true;
        return false;
    };

    $scope.limitloadmorerecipesall = 8;
    $scope.loadMore11 = function () {
        $scope.limitloadmorerecipesall = 9999;
    };
    $scope.isDisabled = false;
    $scope.disableClick11 = function () {
        $scope.isDisabled = true;
        return false;
    };

    $scope.limitloadmoretax = 4;
    $scope.loadMore91 = function () {
        $scope.limitloadmoretax = 9999;
    };
    $scope.isDisabled = false;
    $scope.disableClick91 = function () {
        $scope.isDisabled = true;
        return false;
    };

}]);

app.controller("RecipesDetailCtrl", ["$scope", "$http", "$stateParams", '$location', function ($scope, $http, $stateParams, $location) {


    //shareclicker
    $scope.shareclicker = function () {
        document.getElementById("dropper").className = "dropdown hidden-xs hidden-sm icon-share-stamina open";
        // $("#dropper").toggleClass("open");
        // $("#dropper a.dropdown-toggle").attr('aria-expanded', true);
    };

    // PETICION 04
    $http({
        method: 'GET',
        url: './templates/recipes-landing.json'
        //15

    }).success(function (data) {
        $scope.recipeslanding = data;
        // console.log($scope.recipeslanding);
    }).error(function () {
        $('#myModalAlert').modal('show');
    });

    // armar url con parametros
    $scope.postRecipesId = '';
    $scope.postRecipesId = $stateParams.postRecipesId;

    $scope.showVideo = false;
    $scope.showImage = false;

    //RECIPE DETAIL

    //This will hide the DIV by default.
    $scope.IsVisibleLB = false;
    var done = false;
    $scope.ShowHideLB = function () {
        $scope.IsVisibleLB = $scope.IsVisibleLB ? false : true;
        if (!done) {
            done = true;
            console.log('Push Video');
            dataLayer.push({'event': 'youTubeShown'});
        }
        done = false;
    };

    $scope.getIframeSrc = function (src) {
        return 'https://www.youtube.com/embed/' + src;
    };

    setTimeout(function () {
        $('#show404').show();
    }, 4000);

    $http({
        url: config.janusUrl + '/recipe/' + $scope.postRecipesId + '?appid=thebar-us&limit=1page=1'
        //16

    }).then(function (res) {
            $scope.recipesjsonunique = res.data;

            for (i = 0; i < $scope.recipesjsonunique.attributes.length; i++) {
                var video_url = $scope.recipesjsonunique.attributes[i].WTBURL;

                //console.log('video_url video: ' + video_url);

                ///////////////////////////////////////////////////////
                if (video_url) {
                    var qq = extractVideoID(video_url);

                    function extractVideoID(url) {
                        var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
                        var match = url.match(regExp);
                        if (match && match[7].length == 11) {
                            return match[7];
                        }
                    }

                    //console.log('qq video: ' + video_url);
                }
            }

            ///////////////////////////////////////////////////////
            var apikey = 'AIzaSyDYwPzLevXauI-kTSVXTLroLyHEONuF9Rw';
            q = qq;

            $.get(
                "https://www.googleapis.com/youtube/v3/search", {
                    part: 'snippet,id',
                    q: q,
                    maxResults: 1,
                    type: 'video',
                    key: apikey
                },
                function (data) {
                    //console.log(data.items.length);
                    for (var i = 0; i < data.items.length; i++) {
                        var url1 = "https://www.googleapis.com/youtube/v3/videos?id=" + data.items[i].id.videoId + "&key=" + apikey + "&part=snippet,contentDetails";
                        $.ajax({
                            async: false,
                            type: 'GET',
                            url: url1,
                            success: function (data) {
                                if (data.items.length > 0) {
                                    $scope.VvideoID = data.items[0].id;
                                    //console.log('ID VvideoIDxURL: ' + $scope.VvideoIDxURL);
                                    $scope.Vtitle = data.items[0].snippet.title;
                                    $scope.Vdescription = data.items[0].snippet.description;
                                    $scope.Vduration = convert_time(data.items[0].contentDetails.duration);
                                    $scope.Vthumb = data.items[0].snippet.thumbnails.high.url;
                                }

                                $scope.VvideoIDxURL = "https://www.youtube.com/embed/" + data.items[0].id;
                            }
                        });
                    }
                }
            );

            function convert_time(duration) {
                var a = duration.match(/\d+/g);

                if (duration.indexOf('M') >= 0 && duration.indexOf('H') == -1 && duration.indexOf('S') == -1) {
                    a = [0, a[0], 0];
                }

                if (duration.indexOf('H') >= 0 && duration.indexOf('M') == -1) {
                    a = [a[0], 0, a[1]];
                }
                if (duration.indexOf('H') >= 0 && duration.indexOf('M') == -1 && duration.indexOf('S') == -1) {
                    a = [a[0], 0, 0];
                }

                duration = 0;

                if (a.length == 3) {
                    duration = duration + parseInt(a[0]) * 3600;
                    duration = duration + parseInt(a[1]) * 60;
                    duration = duration + parseInt(a[2]);
                }

                if (a.length == 2) {
                    duration = duration + parseInt(a[0]) * 60;
                    duration = duration + parseInt(a[1]);
                }

                if (a.length == 1) {
                    duration = duration + parseInt(a[0]);
                }
                var h = Math.floor(duration / 3600);
                var m = Math.floor(duration % 3600 / 60);
                var s = Math.floor(duration % 3600 % 60);
                return ((h > 0 ? h + ":" + (m < 10 ? "0" : "") : "") + m + ":" + (s < 10 ? "0" : "") + s);
            }

            ///////////////////////////////////////////////////////


            setTimeout(function () {
                $(function () {
                    var ratingDiv = $('#ratingDiv');
                    ratingDiv.empty();

                    var options = {
                        appId: 'thebar-us',
                        contentId: $scope.recipesjsonunique.rating.contentID,
                        showAverage: false,
                        showVotes: false,
                        showStars: true
                    };
                    $(this).hide();
                    ratingDiv.toRating(options, $(this));
                });

                $('#show404').hide();
            }, 1000);

            // remap received attributes
            attr = {};

            //console.log($scope.recipesjsonunique.recipeImage);
            if ($scope.recipesjsonunique.recipeImage != undefined) {
                $scope.showImage = true;
            }

            for (i = 0; i < $scope.recipesjsonunique.attributes.length; i++) {
                key = Object.keys($scope.recipesjsonunique.attributes[i]);

                if (key == 'WTBURL') {
                    if ($scope.recipesjsonunique.attributes[i].WTBURL != '') {
                        $scope.showVideo = true;
                    }
                }
                // $scope.recipeData[key]= $scope.recipesjsonunique.attributes[i][key];
                attr[key] = $scope.recipesjsonunique.attributes[i][key]
            }
            ////  console.log($scope.recipesjsonunique);
            attr['url'] = $location.absUrl();
            $scope.attr = attr;
            // console.log($scope.attr);
            var data = {};
            data.seoTitle = attr.SEOPageTitle;
            data.seoKeywords = attr.SEOHints;
            data.seoDescription = attr.SEOMetaDescription;
            angular.extend($scope, buildScopeSEO(data, $location));

            $http({
                url: config.janusUrl + '/recipe/list?appid=thebar-us&attributes=DrinkType:' + $scope.attr.DrinkType + ''
                //17

            }).then(function (res) {
                $scope.recipesjsonindrinktype = res.data;
            });

            $http.get({
                url: './wtbJson/' + $scope.recipesjsonunique.brandSlug[0] + '.json'}).then(function (res) {
                $scope.brandsDetailsList = res.data;
                $scope.brandsDetailsListwtbSlug = $scope.recipesjsonunique.brandSlug[0];
                $scope.brandsDetailsListwtbVariantSlug = $scope.recipesjsonunique.variantSlug[0];

                iniWTB($scope.brandsDetailsListwtbSlug, $scope.brandsDetailsListwtbVariantSlug);

                // obtenermos el listado de productos
                // buscamos el producto de la receta dentro del listado
                $http({
                    url: './list/products.json',
                    //19
                    cache: false
                }).then(function (res) {
                    $scope.products = res.data;

                    //console.log($scope.recipesjsonunique.variantSlug[0]);
                    //console.log('-');

                    angular.forEach($scope.products.list_our_products, function (value, key) {
                        var variantSlug = '';
                        // si existe y tiene el variant_slug distitno de null
                        //buscaamos el listado de recetas del producto con el variant_slug

                        //si devuelvo algo pisamos

                        //console.log(value.slug);

                        if (value.variant_slug == $scope.recipesjsonunique.variantSlug[0]) {

                            variantSlug = value.variant_slug;
                            //console.log(variantSlug);

                            if (variantSlug) {
                                $scope.brandsDetailsList.brandImg = value.image;
                                $scope.brandsDetailsList.name = value.name;
                                // console.log('1');
                                // console.log($scope.brandsDetailsList.brandImg);
                                //console.log($scope.brandsDetailsList.name);
                            }

                        }
                    });

                });

                // console.log($scope.brandsDetailsList.brandImg);
                document.querySelector("link[rel='canonical']").setAttribute("href", $location.absUrl());

            });

        }
    );

    //LOAD MORE
    $scope.limiteinrecipe = 4;
    $scope.loadMore5 = function () {
        $scope.limiteinrecipe = 9999;
    };
    $scope.isDisabled = false;
    $scope.disableClick5 = function () {
        //alert("Clicked!");
        $scope.isDisabled = true;
        return false;
    };

    setTimeout(function () {
        $('.nav-recipe a.dropdown-toggle').addClass('active')
    }, 1000);


}]);

app.controller("RecipesDrinkTypeCtrl", ["$scope", "$http", "$stateParams", "$location", function ($scope, $http, $stateParams, $location) {

    //console.log(':::::::::::: Entro a RecipesDrinkTypeCtrl');

    // PETICIONES

    //myDropDownSelect
    $scope.myDropDownSelect = 'one';

    // armar url con parametros
    $scope.postRecipesLandingId = '';
    $scope.postRecipesLandingId = $stateParams.postRecipesLandingId;

    // PETICION 03
    $http({
        method: 'GET',
        url: config.apiJanusUrl + '/recipe/list?appid=thebar-us&limit=99&page=1'
        //20
    }).success(function (data) {
        $scope.recipesjson = data;

        //PETICION EXACTA SLUG DE UNA RECETA
        arrayFeaturedRecipes = [];
        slugFR = '';

        $http({
            url: './templates/recipes-landing.json'
            //21
        }).then(function (res) {
            $scope.recipeslandingfeaturedrecipes = res.data;

            if ($scope.recipeslandingfeaturedrecipes.recipes_featured_list.length) {

                for (var i = 0; i < $scope.recipeslandingfeaturedrecipes.recipes_featured_list.length; i++) {
                    arrayFeaturedRecipes.push({
                        slugfr: $scope.recipeslandingfeaturedrecipes.recipes_featured_list[i].slug
                    });
                    filterFR(arrayFeaturedRecipes[i].slugfr);
                }

            }


            function filterFR(slugFR) {
                $http({
                    url: config.janusUrl + '/recipe/' + slugFR + '?appid=thebar-us&limit=1page=1',
                    //22
                    cache: false
                }).then(function (res) {
                    $scope.recipesjson[slugFR] = res.data;
                });
            }
        });

        //PETICION DINAMICA DRINK TYPE
        arrayDrinkType = [];
        nameFilterDrinkType = '';
        slugFilterDrinkType = '';

        $http({
            url: './taxonomy/drink-type-recipes.json'
            //23
        }).then(function (res) {
            $scope.taxonomydrinktyperecipes = res.data;

            //console.log($scope.postRecipesLandingId);
            //console.log($scope.taxonomydrinktyperecipes);

            for (var i = 0; i < $scope.taxonomydrinktyperecipes.taxonomy_drink_type_list.length; i++) {
                arrayDrinkType.push({
                    drink: $scope.taxonomydrinktyperecipes.taxonomy_drink_type_list[i].drink_type,
                    drinkslug: $scope.taxonomydrinktyperecipes.taxonomy_drink_type_list[i].drink_slug
                });
                if ($scope.taxonomydrinktyperecipes.taxonomy_drink_type_list[i].drink_slug == $stateParams.postRecipesLandingId) {
                    angular.extend($scope, buildScopeSEO($scope.taxonomydrinktyperecipes.taxonomy_drink_type_list[i], $location));
                    filterDrinkType(arrayDrinkType[i].drink, arrayDrinkType[i].drinkslug);
                    //console.log('::::::::::::11');
                }
            }

            function filterDrinkType(nameFilterDrinkType, slugFilterDrinkType) {
                $http({
                    url: config.apiJanusUrl + '/recipe/list?appid=thebar-us&attributes=DrinkType:' + nameFilterDrinkType + '&limit=99&page=1'
                    //24
                }).then(function (res) {
                    $scope.recipesjson[slugFilterDrinkType] = res.data;
                });
            }
        });

    }).error(function () {
        $('#myModalAlert').modal('show');
    });

    // PETICION 04
    $http({
        method: 'GET',
        url:  './templates/recipes-landing.json'
        //25
    }).success(function (data) {
        $scope.recipeslanding = data;
    }).error(function () {
        $('#myModalAlert').modal('show');
    });

    //LOAD MORE
    $scope.limitloadmorebrandsrec2 = 8;
    $scope.loadMore12 = function () {
        $scope.limitloadmorebrandsrec2 = 9999;
    };
    $scope.isDisabled2 = false;
    $scope.disableClick12 = function () {
        $scope.isDisabled2 = true;
        return false;
    };

    $scope.limitloadmorebrandsrec = 8;
    $scope.loadMore82 = function () {
        $scope.limitloadmorebrandsrec = 9999;
    };
    $scope.isDisabled = false;
    $scope.disableClick82 = function () {
        $scope.isDisabled = true;
        return false;
    };

    $scope.limitloadmorerecipesall = 8;
    $scope.loadMore11 = function () {
        $scope.limitloadmorerecipesall = 9999;
    };
    $scope.isDisabled = false;
    $scope.disableClick11 = function () {
        $scope.isDisabled = true;
        return false;
    };

    $scope.limitloadmoretax = 4;
    $scope.loadMore91 = function () {
        $scope.limitloadmoretax = 9999;
    };
    $scope.isDisabled = false;
    $scope.disableClick91 = function () {
        $scope.isDisabled = true;
        return false;
    };

}]);

app.controller("SpiritsCtrl", ["$scope", "$http", "$stateParams", "screenSize", "$modal", "$log", "$location", function ($scope, $http, $stateParams, screenSize, $modal, $log, $location) {

    // console.log('SPIRITS CRTL ENTER');

    // peticiones
    $http({
        method: 'GET',
        url: './templates/spirits.json'
        //26
    }).success(function (data) {
        $scope.spirits = data;
        angular.extend($scope, buildScopeSEO($scope.spirits, $location));
    }).error(function () {
        $('#myModalAlert').modal('show');
    });
}]);

app.controller("SpiritsDetailCtrl", ["$scope", "$http", "$stateParams", "screenSize", "$modal", "$log", "$location", function ($scope, $http, $stateParams, screenSize, $modal, $log, $location) {

    // armar url con parametros
    $scope.postSpiritsId = $stateParams.postSpiritsId;

    $scope.postBrandsId = '';
    $scope.postBrandsId = $stateParams.postBrandsId;

    $scope.postVariantId = '';
    $scope.postVariantId = $stateParams.postVariantId;

    // scroll to top
    $scope.scroll = function () {
        var scrollTop = 0;
        $('html, body').animate({scrollTop: scrollTop}, 'slow');
    };

    //paginator
    $scope.totalItems = 4;
    $scope.itemsPerPage = 1;
    $scope.currentPage = 1;
    $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
    };
    $scope.pageChanged = function () {
        $log.log('Page changed to: ' + $scope.currentPage);
    };


    // INSTANCIA DE MODAL
    $scope.open = function (size) {
        $scope.modalInstance = $modal.open({
            templateUrl: 'inc/flavor-map.html',
            controller: 'SpiritsDetailCtrl',
            size: size,
            resolve: {
                items: function () {
                    return $scope.items;
                }
            }
        });
        $scope.modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    // peticiones
    $http({
        method: 'GET',
        url: './templates/spirits.json'
        //27
    }).success(function (data) {
        $scope.spirits = data;
    }).error(function () {
        $('#myModalAlert').modal('show');
    });

    $http({
        method: 'GET',
        url: './detail/spirits.json'
        //28
    }).success(function (data) {
        $scope.detailspirits = data;

        //console.log($scope.detailspirits);

    }).error(function () {
        $('#myModalAlert').modal('show');
    });

    $http({
        url: './list/brands.json',
        //29
        cache: false
    }).then(function (res) {
        $scope.brandslist = res.data;

        //console.log($scope.brandslist);

        $scope.newSpiritsSlug = [];
        for (var i = 0; i < $scope.brandslist.list_brands.length; i++) {
            for (var j = 0; j < $scope.brandslist.list_brands[i].spirit_slug_name_array.length; j++) {
                $scope.newSpiritsSlug.push({
                    name: $scope.brandslist.list_brands[i].name,
                    slug: $scope.brandslist.list_brands[i].slug,
                    description: $scope.brandslist.list_brands[i].description,
                    image: $scope.brandslist.list_brands[i].image,
                    name_spirit_array: $scope.brandslist.list_brands[i].spirit_slug_name_array[j].post_title,
                    slug_spirit_array: $scope.brandslist.list_brands[i].spirit_slug_name_array[j].post_name
                });
            }
        }
        //alert('asgsdfgsdfgsdf');
        //console.log($scope.newSpiritsSlug);
    });

    $http({
        url: './products/list-flavor.json',
        //30
        cache: false
    }).then(function (res) {
        $scope.flavor = res.data;
    });

    spirtisArray = [];
    $http({
        method: 'GET',
        url: './list/spirits.json'
        //31
    }).success(function (data) {
        $scope.spiritslist = data;

        //console.log($scope.spiritslist);

        for (var i = 0; i < $scope.spiritslist.spirit_list.length; i++) {
            if ($scope.spiritslist.spirit_list[i].slug == $stateParams.postSpiritsId) {
                angular.extend($scope, buildScopeSEO($scope.spiritslist.spirit_list[i], $location));
            }
            spirtisArray.push({
                name: $scope.spiritslist.spirit_list[i].name,
                slug: $scope.spiritslist.spirit_list[i].slug,
                customslug: $scope.spiritslist.spirit_list[i].customslug
            });
        }
        for (var k = 0; k < spirtisArray.length; k++) {
            if ($scope.postSpiritsId == spirtisArray[k].slug) {
                searchSpirits(spirtisArray[k].customslug)
            }
        }
    });

    function searchSpirits(theNameSpirits) {
        $http({
            url: config.janusUrl + '/recipe/list/?appid=thebar-us&' + theNameSpirits + '&limit=16&page=1'
            //32
        }).then(function (res) {
            $scope.recipesjsoninspirits = res.data;
            // console.log($scope.recipesjsoninspirits);
        });
    }

    $http({
        method: 'GET',
        url: config.janusUrl + '/recipe/list/?appid=thebar-us&category=Whisk(e)y&limit=4&page=1'
        //33
    }).success(function (data) {
        $scope.recipesjsonwhisky = data;
        //console.log($scope.recipesjsonwhisky);
    }).error(function () {
        $('#myModalAlert').modal('show');
    });

    //LOAD MORE
    $scope.limitloadmorespirits = 20;
    $scope.loadMore3 = function () {
        $scope.limitloadmorespirits = 9999;
    };
    $scope.isDisabled = false;
    $scope.disableClick3 = function () {
        //alert("Clicked!");
        $scope.isDisabled = true;
        return false;
    };

    setTimeout(function () {
        $('.nav-spirits a.dropdown-toggle').addClass('active')
    }, 1000);
    // alert('aaaaaaaaaaa');
    //angular.extend($scope, buildScopeSEO(config.SEOglobal, $location));
    // console.log(config);

}]);

app.controller("BrandsCtrl", ["$scope", "$http", "$stateParams", "screenSize", "$location", function ($scope, $http, $stateParams, screenSize, $location) {

    // INFO NUTRICIONAL
    $scope.nutritionAccordion = {
        open: false
    };

    $scope.postBrandsId = $stateParams.postBrandsId;

    // PETICION BRANDS
    $http({
        url: './templates/brands.json',
        //34
        cache: false
    }).then(function (res) {
        $scope.brands = res.data;
        angular.extend($scope, buildScopeSEO($scope.brands, $location));

    });

    brandArray = [];
    $http({
        url: './list/brands.json'
        //35
    }).then(function (res) {
        $scope.brandslist = res.data;

        //console.log($scope.brandslist);

        for (var i = 0; i < $scope.brandslist.list_brands.length; i++) {
            brandArray.push({name: $scope.brandslist.list_brands[i].name, slug: $scope.brandslist.list_brands[i].slug});
        }
        for (var k = 0; k < brandArray.length; k++) {
            if ($scope.postBrandsId == brandArray[k].slug) {
                searchBrand(brandArray[k].slug)
            }
        }
    });

    function searchBrand(slugnameBrand) {
        $http({
            url: config.janusUrl + '/recipe/list?brand=' + slugnameBrand + '&appid=thebar-us',
            //36
            cache: false
        }).then(function (res) {
            $scope.brandInRecipe = res.data;
            //console.log($scope.brandInRecipe);
        });
    }

    // PETICION OUR PRODUCTS
    $http({
        url: './list/products.json',
        //37
        cache: false
    }).then(function (res) {
        $scope.products = res.data;
    });

    // if ($stateParams.postBrandsId) {
    //     $('#selectBrands').val($scope.postBrandsId);
    // }


    //LOAD MORE
    $scope.limitloadmorebrands = 12;
    $scope.loadMore4 = function () {
        $scope.limitloadmorebrands = 9999;
    };
    $scope.isDisabled = false;
    $scope.disableClick4 = function () {
        //alert("Clicked!");
        $scope.isDisabled = true;
        return false;
    };

    //LOAD MORE
    $scope.limitloadmorebrandsrec = 8;
    $scope.loadMore12 = function () {
        $scope.limitloadmorebrandsrec = 9999;
    };
    $scope.isDisabled = false;
    $scope.disableClick12 = function () {
        //alert("Clicked!");
        $scope.isDisabled = true;
        return false;
    };

}]);

app.controller("BrandsDetailCtrl", ["$scope", "$http", "$stateParams", "screenSize", "$location", function ($scope, $http, $stateParams, screenSize, $location) {

    // INFO NUTRICIONAL
    $scope.nutritionAccordion = {
        open: false
    };

    $http({
        url: './list/brands.json'
        //38
    }).then(function (res) {
        $scope.brandslist = res.data;
    });

    $scope.postBrandsId = '';
    $scope.postBrandsId = $stateParams.postBrandsId;

    $scope.postVariantId = '';
    $scope.postVariantId = $stateParams.postVariantId;
    //console.log($scope.postBrandsId);


    // PETICION BRANDS
    $http.get('./detail/brand/' + $stateParams.postBrandsId + '.json').then(function (res) {
        $scope.brandsdetails = res.data;
       // console.log(res.data);
        angular.extend($scope, buildScopeSEO($scope.brandsdetails, $location));

        $http({
            url: config.apiJanusUrl + '/recipe/list?brand=' + res.data.wtbSlug + '&appid=thebar-us&limit=99'
            //40
        }).then(function (res) {
            $scope.brandInRecipe = res.data;

            //console.log($scope.brandInRecipe);

            // PETICION OUR PRODUCTS
            nutriIDTableServeArray = [];
            $http({
                url:  './list/products.json',
                //41
                cache: false
            }).then(function (res) {
                $scope.products = res.data;
                $scope.toUseWTB = res.data;

                //console.log($scope.products);

                $scope.recipesTotals = $scope.brandInRecipe.totalcount;
                // $scope.brandInRecipeInVariantSlug = '';

                angular.forEach($scope.products.list_our_products, function (value, key) {

                    var variantSlug = '';
                    if (value.slug == $stateParams.postVariantId) {

                        variantSlug = value.variant_slug;
                        var nutriID = value.nutritioninfoid;

                        //console.log(nutriID);
                        //console.log(variantSlug);

                        //console.log(value.brand_slug, value.variant_slug);
                        iniWTB(value.brand_slug, value.variant_slug);

                        if (variantSlug) {
                            $http({
                                url: config.apiJanusUrl + '/recipe/list?variant=' + variantSlug + '&appid=thebar-us&limit=99'
                                //42
                            }).then(function (res) {
                                // $scope.brandInRecipeInVariantSlug = res.data;
                                $scope.recipesTotals = res.data.totalcount;
                                //console.log($scope.brandInRecipeInVariantSlug);

                                var counterio = 0;

                                //console.log(variantSlug);

                                for (var ii = 0; ii < $scope.products.list_our_products.length; ii++) {

                                    if ($scope.postBrandsId == $scope.products.list_our_products[ii].brand_slug) {

                                        if (variantSlug == $scope.products.list_our_products[ii].variant_slug) {
                                            //myDropDownSelect
                                            if ($scope.recipesTotals != 0) {
                                                //console.log('counterio: ' + counterio);
                                                $scope.myDropDownSelect = variantSlug;
                                            }

                                        }

                                        counterio = counterio + 1;
                                    }

                                }

                            });
                        }

                        if (nutriID) {
                            $http({
                                url: 'https://api.diageoapi.com/nutritioninfo/EN-US/nutrition/' + nutriID + '-35?appid=thebar-us'
                                //43
                            }).then(function (res) {
                                // $scope.brandInRecipeInVariantSlug = res.data;
                                $scope.nutriIDTable = res.data;
                                //console.log($scope.nutriIDTable.serveSizes[0]);
                                $scope.nutriIDTableServe = $scope.nutriIDTable.serveSizes[0];
                                //console.log($scope.nutriIDTable.serveSizes[0].length);
                            });
                        }


                    }

                });
            });
        });

    });


    // PETICION 03
    $http({
        method: 'GET',
        url: config.janusUrl + '/recipe/list?appid=thebar-us&limit=64page=1'
        //44
    }).success(function (data) {
        $scope.recipesjson = data;
        //  console.log($scope.recipesjson);
    }).error(function () {
        $('#myModalAlert').modal('show');
    });


    //myDropDownSelect
    $scope.myDropDownSelect = 'one';

    //LOAD MORE
    $scope.limitloadmorebrandsrec2 = 8;
    $scope.loadMore12 = function () {
        $scope.limitloadmorebrandsrec2 = 9999;
    };
    $scope.isDisabled2 = false;
    $scope.disableClick12 = function () {
        $scope.isDisabled2 = true;
        return false;
    };

    $scope.limitloadmorebrandsrec = 8;
    $scope.loadMore82 = function () {
        $scope.limitloadmorebrandsrec = 9999;
    };
    $scope.isDisabled = false;
    $scope.disableClick82 = function () {
        $scope.isDisabled = true;
        return false;
    };

    setTimeout(function () {
        $('.nav-brands a.dropdown-toggle').addClass('active')
    }, 1000);

}]);

app.controller("ArticlesCrtl", ["$scope", "$http", "$stateParams", "$location", function ($scope, $http, $stateParams, $location) {

        // PETICION ARTICLES LANDING
        $scope.articlesListVideos = [];
        $scope.articlesListVideosBkp = [];
        $scope.articlesListVideosTemp = [];

        $http({
            url: './templates/articles.json'
            //45
        }).then(function (res) {
            angular.extend($scope, buildScopeSEO(res.data, $location));
            $scope.articlestemplate = res.data;

            /* PAGER */
            $scope.viewby = 8;
            $scope.totalItems = $scope.articlestemplate.list_videos.length;
            $scope.currentPage = 1;
            $scope.itemsPerPage = $scope.viewby;
            $scope.maxSize = 99; //Number of pager buttons to show

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
                //console.log('Page changed to: ' + $scope.currentPage);
            };

            $scope.setItemsPerPage = function (num) {
                $scope.itemsPerPage = num;
                $scope.currentPage = 1; //reset to first paghe
            };
        });


        //This will hide the DIV by default.
        var done = false;
        $scope.ShowInfoLB = function () {
            if (!done) {
                done = true;
                console.log('Push Video');
                dataLayer.push({'event': 'youTubeShown'});
            }
            done = false;
        };


        //LOAD MORE
        $scope.limitloadmorearticles = 6;
        $scope.loadMore1 = function () {
            $scope.limitloadmorearticles = 9999;
        };
        $scope.isDisabled = false;
        $scope.disableClick1 = function () {
            $scope.isDisabled = true;
            return false;
        };


        // PETICION RECETAS
        $http({
            method: 'GET',
            url: config.janusUrl + '/recipe/list?appid=thebar-us&page=1'
            //46
        }).success(function (data) {
            $scope.recipesjson = data;
        }).error(function () {
            $('#myModalAlert').modal('show');
        });

        // PETICION BRANDS
        $http({
            url: './list/brands.json',
            //47
            cache: false
        }).then(function (res) {
            $scope.brandslist = res.data;
            brandlistx = $scope.brandslist.list_brands;

            if ($stateParams.postOccasionId) {
                bto(brandlistx);
            }
        });

    }]
);

app.controller("OccasionsCrtl", ["$scope", "$http", "$location", function ($scope, $http, $location) {

        // PETICION OCCASIONS
        occasionArray = [];
        $scope.occasionListVideos = [];

        $http({
            url: './templates/occasions.json',
            //48
        }).then(function (res) {
            $scope.occasionstemplate = res.data;

            angular.extend($scope, buildScopeSEO($scope.occasionstemplate, $location));

            for (var i = 0; i < $scope.occasionstemplate.list_occasions.length; i++) {
                occasionArray.push({
                    name: $scope.occasionstemplate.list_occasions[i].name,
                    slug: $scope.occasionstemplate.list_occasions[i].slug
                });
            }

            for (var k = 0; k < occasionArray.length; k++) {
                if ($scope.postOccasionId == occasionArray[k].slug) {
                    searchOccasion(occasionArray[k].name)
                }
            }


            /* PAGER */
            $scope.viewby = 8;
            $scope.totalItems = $scope.occasionstemplate.list_videos.length;
            $scope.currentPage = 1;
            $scope.itemsPerPage = $scope.viewby;
            $scope.maxSize = 99; //Number of pager buttons to show

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
                //console.log('Page changed to: ' + $scope.currentPage);
            };

            $scope.setItemsPerPage = function (num) {
                $scope.itemsPerPage = num;
                $scope.currentPage = 1; //reset to first paghe
            };


        });

        // PETICION CATEGORIES DE OCCASIONS
        $http({
            url: './list/categories.json',
            //49
            cache: false
        }).then(function (res) {
            $scope.categories = res.data;
        });

        // PETICION RECETAS
        $http({
            method: 'GET',
            url: config.janusUrl + '/recipe/list?appid=thebar-us&page=1'
            //50
        }).success(function (data) {
            $scope.recipesjson = data;
        }).error(function () {
            $('#myModalAlert').modal('show');
        });

        //LOAD MORE
        $scope.limitloadmoreoccasions = 5;
        $scope.loadMore2 = function () {
            $scope.limitloadmoreoccasions = 9999;
        };
        $scope.isDisabled = false;
        $scope.disableClick2 = function () {
            $scope.isDisabled = true;
            return false;
        };

        //This will hide the DIV by default.
        var done = false;
        $scope.ShowInfoLB = function () {
            if (!done) {
                done = true;
                console.log('Push Video');
                dataLayer.push({'event': 'youTubeShown'});
            }
            done = false;
        };

        //myDropDownSelect
        $scope.myDropDownSelect = 0;

        // filtersCat
        $scope.filtersCatBy = '!';
        $scope.filtersCat = function (params) {
            $scope.filtersCatBy = params;
            $scope.limitloadmoreoccasions = 99;
            $scope.isDisabled = true;

            //ul
            $(".oSelect").on("click", ".init", function () {
                $(this).closest(".oSelect").children('li.noinit').slideDown();
            });

            var allOptions = $(".oSelect").children('li.noinit');
            $(".oSelect").on("click", "li.noinit", function () {
                allOptions.removeClass('selected');
                $(this).addClass('selected');
                $(".oSelect").children('.init').html($(this).html());
                allOptions.slideUp();
            });
        };

        $scope.selected = 0;
        $scope.select = function (index) {
            $scope.selected = index;
        };

    }]
);

app.controller("OccasionsDetailCrtl", ["$scope", "$http", "$stateParams", "$state", "$modal", "$log", "$window", "screenSize", "$filter", "$location", function ($scope, $http, $stateParams, $state, $modal, $log, $window, screenSize, $filter, $location) {

        // armar url con parametros - occasion
        $scope.postOccasionId = '';
        $scope.postOccasionId = $stateParams.postOccasionId;

        $http.get(
            './detail/occasions/' + $stateParams.postOccasionId + '.json'
            //51
        ).then(function (res) {
            angular.extend($scope, buildScopeSEO(res.data, $location));
            ////  console.log(res.data);
            $http({
                url: config.apiJanusUrl + '/recipe/list?appid=thebar-us&limit=99&occasion=' + res.data.name + '',
                cache: false
            }).then(function (res) {
                $scope.occasionInRecipe = res.data;
                $scope.itemsOccasions = $filter('unique')($scope.occasionInRecipe.docs, 'brandSlug');
                //console.log($scope.occasionInRecipe.docs);
                //console.log($scope.itemsOccasions);

                // buscamos las marcas de la ocasion que estamos viendo
                $http({
                    url: './list/brands.json'
                    //52
                }).then(function (res) {
                    $scope.brandslist = res.data;
                    brandlistx = $scope.brandslist.list_brands;

                    // console.log($scope.postOccasionId);

                    if ($stateParams.postOccasionId) {
                        bto(brandlistx);
                    }

                    //console.log(brandsToOcassion);
                });

                function bto(listadebrands) {
                    // console.log('entro a bto');

                    // console.log($scope.postOccasionId);

                    $scope.brandsToOcassion = [];
                    brandsinOccasion = [];
                    brandsinOccasionTitle = [];
                    listatemp = [];

                    // console.log(listadebrands);
                    for (var i = 0; i < listadebrands.length; i++) {
                        listatemp.push({
                            name: listadebrands[i].name,
                            image: listadebrands[i].image,
                            description: listadebrands[i].description,
                            slug: listadebrands[i].slug
                        });
                    }

                    // console.log($scope.itemsOccasions);
                    if ($scope.itemsOccasions != undefined) {
                        for (var b = 0; b < $scope.itemsOccasions.length; b++) {
                            brandsinOccasion.push($scope.itemsOccasions[b].brandSlug[0]);
                        }
                    }

                    if ($scope.itemsOccasions != undefined) {
                        for (var t = 0; t < $scope.itemsOccasions.length; t++) {
                            brandsinOccasionTitle.push($scope.itemsOccasions[t].brand);
                        }
                    }

                    /*//  console.log(listatemp);
                     //  console.log(brandsinOccasion);*/
                    for (var c = 0; c < listatemp.length; c++) {

                        for (var d = 0; d < brandsinOccasion.length; d++) {

                            //console.log(listatemp[c].slug);
                            //console.log(brandsinOccasion[d]);

                            if (listatemp[c].slug === brandsinOccasion[d]) {

                                /*   //  console.log('matcheos brand en ocasiones con ocasion en brands');*/

                                $scope.brandsToOcassion.push({
                                    name: listatemp[c].name,
                                    image: listatemp[c].image,
                                    description: listatemp[c].description,
                                    slug: listatemp[c].slug
                                });

                                //console.log($scope.brandsToOcassion);
                            }

                        }

                    }


                }

            });
        });

        // PETICION OCCASIONS
        $http({
            url: './templates/occasions.json'
            //53
        }).then(function (res) {
            $scope.occasionstemplate = res.data;

        });


        //LOAD MORE 1
        $scope.limitloadmorearticles = 6;
        $scope.loadMore1 = function () {
            $scope.limitloadmorearticles = 9999;
        };
        $scope.isDisabled = false;
        $scope.disableClick1 = function () {
            //alert("Clicked!");
            $scope.isDisabled = true;
            return false;
        };

        //LOAD MORE 2
        $scope.limitloadmoreocca = 8;
        $scope.loadMore42 = function () {
            $scope.limitloadmoreocca = 9999;
        };
        $scope.isDisabled = false;
        $scope.disableClick42 = function () {
            //alert("Clicked!");
            $scope.isDisabled = true;
            return false;
        };

        //LOAD MORE 3
        $scope.limitrecinocca = 8;
        $scope.loadMore52 = function () {
            $scope.limitrecinocca = 9999;
        };
        $scope.isDisabled = false;
        $scope.disableClick52 = function () {
            $scope.isDisabled = true;
            return false;
        };

        //LOAD MORE 4
        $scope.limitloadmoreoccasions = 5;
        $scope.loadMore2 = function () {
            $scope.limitloadmoreoccasions = $scope.occasionstemplate.list_occasions.length;
        };
        $scope.isDisabled = false;
        $scope.disableClick2 = function () {
            $scope.isDisabled = true;
            return false;
        };

        setTimeout(function () {
            $('.nav-occasions a.dropdown-toggle').addClass('active')
        }, 1000);

    }]
);

app.controller("OccasionsCategoryCrtl", ["$scope", "$http", "$stateParams", "$state", "$modal", "$log", "$window", "screenSize", "$filter", "$location", function ($scope, $http, $stateParams, $state, $modal, $log, $window, screenSize, $filter, $location) {

        //OccasionsCategoryCrtl
        // armar url con parametros - occasion
        $scope.postOccasionId = '';
        $scope.postOccasionId = $stateParams.postOccasionId;

        // armar url con parametros - occasion
        $scope.postOccasionCat = '';
        $scope.postOccasionCat = $stateParams.postOccasionCat;

        //console.log($scope.postOccasionCat);


        // PETICION OCCASIONS
        occasionArray = [];
        $http({
            url: './templates/occasions.json',
            //54
        }).then(function (res) {
            $scope.occasionstemplate = res.data;

            angular.extend($scope, buildScopeSEO($scope.occasionstemplate, $location));

            for (var i = 0; i < $scope.occasionstemplate.list_occasions.length; i++) {
                occasionArray.push({
                    name: $scope.occasionstemplate.list_occasions[i].name,
                    slug: $scope.occasionstemplate.list_occasions[i].slug
                });
            }

            for (var k = 0; k < occasionArray.length; k++) {
                if ($scope.postOccasionId == occasionArray[k].slug) {
                    searchOccasion(occasionArray[k].name)
                }
            }


            /* PAGER */
            $scope.viewby = 8;
            $scope.totalItems = $scope.occasionstemplate.list_videos.length;
            $scope.currentPage = 1;
            $scope.itemsPerPage = $scope.viewby;
            $scope.maxSize = 99; //Number of pager buttons to show

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
                //console.log('Page changed to: ' + $scope.currentPage);
            };

            $scope.setItemsPerPage = function (num) {
                $scope.itemsPerPage = num;
                $scope.currentPage = 1; //reset to first paghe
            };


        });

        // PETICION CATEGORIES DE OCCASIONS
        $http({
            url: './list/categories.json',
            //55
            cache: false
        }).then(function (res) {
            $scope.categories = res.data;

            //myDropDownSelect
            $scope.myDropDownSelect = 0;
            $scope.filtersCatBy = '!';
            $scope.selected = 0;

            for (var i = 0; i < $scope.categories.length; i++) {
                if ($scope.postOccasionCat == $scope.categories[i].slug) {
                    // filtersCat
                    $scope.filtersCatBy = $scope.categories[i].slug;
                    $scope.selected = i;
                }
            }

            //console.log($scope.filtersCatBy);
            //console.log($scope.selected);

            $scope.filtersCat = function (params) {
                $scope.filtersCatBy = params;
                $scope.limitloadmoreoccasions = 99;
                $scope.isDisabled = true;

                //ul
                $(".oSelect").on("click", ".init", function () {
                    $(this).closest(".oSelect").children('li.noinit').slideDown();
                });

                var allOptions = $(".oSelect").children('li.noinit');
                $(".oSelect").on("click", "li.noinit", function () {
                    allOptions.removeClass('selected');
                    $(this).addClass('selected');
                    $(".oSelect").children('.init').html($(this).html());
                    allOptions.slideUp();
                });
            };

            //console.log($scope.categories[i].slug);
            $scope.select = function (index) {
                $scope.selected = index;
                //alert($scope.selected);
            };

            setTimeout(function () {
                $('#linkerio' + $scope.filtersCatBy + '').trigger("click");
            }, 1000);


        });

        // PETICION RECETAS
        $http({
            method: 'GET',
            url: config.janusUrl + '/recipe/list?appid=thebar-us&page=1'
            //56
        }).success(function (data) {
            $scope.recipesjson = data;
        }).error(function () {
            $('#myModalAlert').modal('show');
        });

        //LOAD MORE
        $scope.limitloadmoreoccasions = 5;
        $scope.loadMore2 = function () {
            $scope.limitloadmoreoccasions = 9999;
        };
        $scope.isDisabled = false;
        $scope.disableClick2 = function () {
            $scope.isDisabled = true;
            return false;
        };

    }]
);

app.controller("WTBPageCtrl", ["$scope", "$http", "screenSize", "$stateParams", "$location", "$rootScope", "$window", "anchorSmoothScroll", function ($scope, $http, screenSize, $stateParams, $location, $rootScope, $window, anchorSmoothScroll) {

    iniWTB('', '');

}]);

app.controller("RecipesPOCCtrl", ["$scope", "$http", "$stateParams", "$location", function ($scope, $http, $stateParams, $location) {

    //console.log(':::::::::::: Entro a RecipesCtrl');

    // PETICIONES

    //myDropDownSelect
    $scope.myDropDownSelect = 'one';

    // armar url con parametros
    $scope.postRecipesPOCLandingId = '';
    $scope.postRecipesPOCLandingId = $stateParams.postRecipesPOCLandingId;


    // PETICION 03
    $http({
        method: 'GET',
        url: 'https://eu-west-1.aws.webhooks.mongodb-stitch.com/api/client/v2.0/app/recipes-api-qmcfp/service/getRecipes/incoming_webhook/getRecipesHook?DrinkType=Test'
        //10

    }).success(function (data) {
        console.log(data);
        $scope.recipesjson = data;

    }).error(function () {
        $('#myModalAlert').modal('show');
    });

}]);

app.controller("RecipesPOCDetailCtrl", ["$scope", "$http", "$stateParams", '$location', function ($scope, $http, $stateParams, $location) {

    // armar url con parametros
    $scope.postRecipesPOCId = '';
    $scope.postRecipesPOCId = $stateParams.postRecipesPOCId;

    $scope.showVideo = false;
    $scope.showImage = false;

    //RECIPE DETAIL
  
    $http({
        url: 'https://eu-west-1.aws.webhooks.mongodb-stitch.com/api/client/v2.0/app/recipes-api-qmcfp/service/getRecipe/incoming_webhook/getRecipeHook?_id=' + $scope.postRecipesPOCId 
                //16

    }).then(function (res) {
        console.log(res.data);    
        $scope.recipesjsonunique = res.data;
    });
    attr = {};
    var data = {};
    data.seoTitle = attr.SEOPageTitle;
    data.seoKeywords = attr.SEOHints;
    data.seoDescription = attr.SEOMetaDescription;
    angular.extend($scope, buildScopeSEO(data, $location));

    $scope.isDisabled = false;
    $scope.disableClick5 = function () {
        //alert("Clicked!");
        $scope.isDisabled = true;
        return false;
    };

    setTimeout(function () {
        $('.nav-recipe a.dropdown-toggle').addClass('active')
    }, 1000);
}]);