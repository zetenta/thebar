app.directive('bindHtmlUnsafe', function ($compile) {
    return function ($scope, $element, $attrs) {

        var compile = function (newHTML) { // Create re-useable compile function
            newHTML = $compile(newHTML)($scope); // Compile html
            $element.html('').append(newHTML); // Clear and append it
        };

        var htmlName = $attrs.bindHtmlUnsafe; // Get the name of the variable
                                              // Where the HTML is stored

        $scope.$watch(htmlName, function (newHTML) { // Watch for changes to
            // the HTML
            if (!newHTML) return;
            compile(newHTML);   // Compile it
        });

    }
});

app.directive('lightboxDirective', function () {
    return {
        restrict: 'E', // applied on 'element'
        transclude: true, // re-use the inner HTML of the directive
        template: '<section ng-transclude></section>' // need this so that inner HTML will be used
    }
});

app.directive('script', function () {
    return {
        restrict: 'E',
        scope: false,
        link: function (scope, elem, attr) {
            if (attr.type === 'text/javascript-lazy') {
                var s = document.createElement("script");
                s.type = "text/javascript";
                var src = elem.attr('src');
                if (src !== undefined) {
                    s.src = src;
                }
                else {
                    var code = elem.text();
                    s.text = code;
                }
                document.head.appendChild(s);
                elem.remove();
            }
        }
    };
});

app.directive('title', ['$rootScope', '$timeout',
    function ($rootScope, $timeout) {
        return {
            link: function () {

                var listener = function (event, toState) {

                    $timeout(function () {
                        $rootScope.title = (toState.data && toState.data.pageTitle)
                            ? toState.data.pageTitle
                            : 'The Bar';
                    })
                };

                $rootScope.$on('$stateChangeSuccess', listener);
            }
        };
    }
]);

app.directive('relatedBrands', function () {
    return {
        restrict: 'E',
        controller: 'BrandsCtrl',
        scope: {
            limit: '=',
            filterBy: '=',
            data: '='
        },
        template: [
            '<div class="col-xs-12 products-boxes">',
            '<div class="related col-xs-12 col-sm-12 col-md-6 col-lg-3" ' +
            'ng-repeat="brandsx in data | filter: filterBy:true | limitTo: limit">',
            '<div class="brand-icon">',
            '<div class="brand-mask"><span class="brands-crop"></span>',
            '<img ng-src="{{ brandsx.image }}" alt="{{ brandsx.name }}">',
            '</div>',
            '</div>',
            '<div class="brand-text">',
            '<h1 class="text-center box-name">',
            '<a href="#" title="{{ brandsx.name }}">{{ brandsx.name }}</a>',
            '</h1>',
            '<div class="box-lear-more">',
            '<a ui-sref-opts="{reload:true}" ui-sref="brandsdetail({ postBrandsId: brandsx.slug })" title="{{ brands.brands_button_name }}">',
            'Get Brand</a>',
            '</div>',
            '</div>',

            '</div>',
            '</div>'
        ].join("")
    };
});

app.directive('relatedOccasions', function () {
    return {
        restrict: 'E',
        controller: 'GlobalHeaderSearchFooterCrtl',
        scope: {
            columns: '=',
            limit: '=',
            truncLimit: '='
        },
        link: function (scope) {
            scope.calcColumns = {
                'width': 'calc(100% /' + scope.columns + ')',
                'float': 'left',
                'height': '100%'
            };
        },
        template: [
            '<div ng-repeat="slide in occasions.slidesOccasions | limitTo: limit" ng-style="calcColumns">',
            '<div class="no-pad">',
            '<div class="hero-nofull" style="background-image: url({{ slide.hero_image }})"></div>',
            '<div class="block text-left mt0" style="min-height: 400px;">',
            '<div class="text-up-small gray">COLLECTION</div><br>',
            '<h2 class="text-left">{{ slide.name }}</h2><br>',
            '<p>{{ slide.description | truncate: truncLimit: "..."}}</p>',
            '<div class="box-lear-more"> <a href="" title="Get Recipe">Get {{ slide[0].post_type }}</a>',
            '</div></div></div></div>'
        ].join("")
    };
});

app.directive('relatedRecipes', function () {
    return {
        restrict: 'E',
        controller: 'GlobalHeaderSearchFooterCrtl',
        scope: {
            limit: '=',
            filterBy: '=',
            data: '='
        },
        template: [
            '<div ng-repeat="recipe in data | filter: filterBy:true | limitTo: limit" class="related col-md-3 col-sm-6 no-pad">',
            '<div class="recipe-img" style="background-image: url({{ recipe.BackgroundImage.large }})"></div>',
            '<div class="block-s"> <h2>{{ recipe.recipeTitle }}</h2><div class="box-lear-more"><a ui-sref-opts="{reload:true}" ui-sref="recipedetail({ postRecipesId: recipe.recipeSlug })" title="Get recipe">Get recipe</a></div></div>'
        ].join("")
    };
});

app.directive('myFocus', function ($timeout) {
    return function (scope, element, attrs) {
        attrs.$observe('myFocus', function () {
            if (scope.$eval(attrs.myFocus)) {
                // Element needs to be visible to focus
                $timeout(function () {
                    element[0].focus();
                })
            }
        })
    };
});

app.directive('myEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.myEnter);
                });

                event.preventDefault();
            }
        });
    };
});

// PARA QUE FUNCIONE YOUTUBE
app.config(function ($sceDelegateProvider) {
    $sceDelegateProvider.resourceUrlWhitelist([
        'self',
        'https://www.youtube.com/**'
    ]);
});

var $scope, $location;

app.service('anchorSmoothScroll', function () {
    this.scrollTo = function (eID) {

        // This scrolling function
        // is from http://www.itnewb.com/tutorial/Creating-the-Smooth-Scroll-Effect-with-JavaScript

        var startY = currentYPosition();
        var stopY = elmYPosition(eID);
        var distance = stopY > startY ? stopY - startY : startY - stopY;
        if (distance < 100) {
            scrollTo(0, stopY);
            return;
        }
        var speed = Math.round(distance / 100);
        if (speed >= 20) speed = 20;
        var step = Math.round(distance / 25);
        var leapY = stopY > startY ? startY + step : startY - step;
        var timer = 0;
        if (stopY > startY) {
            for (var i = startY; i < stopY; i += step) {
                setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
                leapY += step;
                if (leapY > stopY) leapY = stopY;
                timer++;
            }
            return;
        }
        for (var i = startY; i > stopY; i -= step) {
            setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
            leapY -= step;
            if (leapY < stopY) leapY = stopY;
            timer++;
        }

        function currentYPosition() {
            // Firefox, Chrome, Opera, Safari
            if (self.pageYOffset) return self.pageYOffset;
            // Internet Explorer 6 - standards mode
            if (document.documentElement && document.documentElement.scrollTop)
                return document.documentElement.scrollTop;
            // Internet Explorer 6, 7 and 8
            if (document.body.scrollTop) return document.body.scrollTop;
            return 0;
        }

        function elmYPosition(eID) {
            var elm = document.getElementById(eID);
            var y = elm.offsetTop;
            var node = elm;
            while (node.offsetParent && node.offsetParent != document.body) {
                node = node.offsetParent;
                y += node.offsetTop;
            }
            return y;
        }

    };
});

app.controller('ScrollCtrl', function ($scope, $location, anchorSmoothScroll) {

    $scope.gotoElement = function (eID) {
        // set the location.hash to the id of
        // the element you wish to scroll to.
        $location.hash('bottom');

        // call $anchorScroll()
        anchorSmoothScroll.scrollTo(eID);

    };
});

function Ctrl($scope) {
    $scope.limits = [
        {value: '1', text: '1'},
        {value: '2', text: '2'},
        {value: '3', text: '3'},
        {value: '4', text: '4'},
        {value: '5', text: '5'},
        {value: '6', text: '6'},
        {value: '7', text: '7'},
        {value: '8', text: '8'},
        {value: '9', text: '9'},
        {value: '10', text: '10'},
        {value: '15', text: '15'},
        {value: '20', text: '20'},
        {value: '25', text: '25'},
        {value: '30', text: '30'},
        {value: '35', text: '35'},
        {value: '40', text: '40'},
        {value: '45', text: '45'},
        {value: '50', text: '50'}
    ];
}

function buildScopeSEO(data, loc) {
    $result = {};
    $result['seoTitle'] = data.seoTitle;
    $result['seoKeywords'] = data.seoKeywords;
    $result['seoDescription'] = data.seoDescription;
    $result['fburl'] = loc.absUrl();

    //console.log(loc.absUrl());


    //modificamos la canonical URL
    document.querySelector("link[rel='canonical']").setAttribute("href", loc.absUrl());

    //agregamos las canonical alternativas
    angular.forEach(config.canonicalHosts, function (value, key) {
        elem = document.querySelector("link[hreflang='" + key + "']");
        if (elem != null) {
            elem.setAttribute('href', value + loc.url());
        } else {
            meta = '<link rel="alternate" hreflang="' + key + '" href="' + value + loc.url() + '" />';
            $('head').append(meta);
        }
    });
    return $result;
}

app.directive('preventerA', function () {
    return {
        restrict: 'E',
        link: function (scope, elem, attrs) {
            if (attrs.ngClick || attrs.href === '' || attrs.href === '#') {
                elem.on('click', function (e) {
                    e.preventDefault();
                });
            }
        }
    };
});

function sayHi(e) {
    e.preventDefault();
    //alert("hi");
}

function iniWTB(brandSlug, variantSlug) {
    brand = brandSlug;

    $(document).ready(function () {

        // Initialize the WTB Object
        var distance = 15;

        // customize using https://mapstyle.withgoogle.com/
        var styles = [{
            stylers: [{
                hue: "#bcc5d8"
            }, {
                saturation: -80
            }]
        }, {
            featureType: "road",
            elementType: "geometry",
            stylers: [{
                lightness: 100
            }, {
                visibility: "simplified"
            }]
        }, {
            featureType: "road",
            elementType: "labels",
            stylers: [{
                visibility: "off"
            }]
        }];

        styles = [{
            "featureType": "all",
            "elementType": "all",
            "stylers": [{"visibility": "on"}]
        }, {
            "featureType": "administrative",
            "elementType": "all",
            "stylers": [{"visibility": "simplified"}]
        }, {
            "featureType": "administrative",
            "elementType": "labels",
            "stylers": [{"visibility": "on"}]
        }, {
            "featureType": "administrative",
            "elementType": "labels.text.fill",
            "stylers": [{"color": "#444444"}, {"visibility": "on"}]
        }, {
            "featureType": "administrative",
            "elementType": "labels.text.stroke",
            "stylers": [{"visibility": "simplified"}]
        }, {
            "featureType": "administrative",
            "elementType": "labels.icon",
            "stylers": [{"visibility": "on"}]
        }, {
            "featureType": "landscape",
            "elementType": "all",
            "stylers": [{"color": "#ebebeb"}, {"visibility": "on"}]
        }, {
            "featureType": "landscape",
            "elementType": "geometry.stroke",
            "stylers": [{"visibility": "on"}]
        }, {
            "featureType": "landscape.natural",
            "elementType": "all",
            "stylers": [{"visibility": "on"}]
        }, {
            "featureType": "landscape.natural.terrain",
            "elementType": "all",
            "stylers": [{"visibility": "on"}]
        }, {"featureType": "poi", "elementType": "all", "stylers": [{"visibility": "off"}]}, {
            "featureType": "road",
            "elementType": "all",
            "stylers": [{"saturation": -100}, {"lightness": 45}, {"visibility": "on"}]
        }, {
            "featureType": "road.highway",
            "elementType": "all",
            "stylers": [{"visibility": "simplified"}]
        }, {
            "featureType": "road.highway",
            "elementType": "labels.text.fill",
            "stylers": [{"color": "#000000"}]
        }, {
            "featureType": "road.highway.controlled_access",
            "elementType": "all",
            "stylers": [{"visibility": "simplified"}, {"color": "#d6d6d6"}]
        }, {
            "featureType": "road.highway.controlled_access",
            "elementType": "labels.text.fill",
            "stylers": [{"color": "#b1b1b1"}]
        }, {
            "featureType": "road.highway.controlled_access",
            "elementType": "labels.text.stroke",
            "stylers": [{"color": "#ffffff"}]
        }, {
            "featureType": "road.arterial",
            "elementType": "all",
            "stylers": [{"visibility": "on"}, {"color": "#ffffff"}]
        }, {
            "featureType": "road.arterial",
            "elementType": "labels.text.fill",
            "stylers": [{"color": "#ababab"}]
        }, {
            "featureType": "road.arterial",
            "elementType": "labels.text.stroke",
            "stylers": [{"color": "#ffffff"}]
        }, {
            "featureType": "road.arterial",
            "elementType": "labels.icon",
            "stylers": [{"visibility": "off"}]
        }, {
            "featureType": "road.local",
            "elementType": "all",
            "stylers": [{"visibility": "on"}, {"color": "#c7c7c7"}]
        }, {
            "featureType": "road.local",
            "elementType": "labels.text",
            "stylers": [{"visibility": "on"}, {"color": "#ffffff"}]
        }, {
            "featureType": "road.local",
            "elementType": "labels.text.fill",
            "stylers": [{"color": "#787878"}]
        }, {
            "featureType": "road.local",
            "elementType": "labels.text.stroke",
            "stylers": [{"color": "#ffffff"}]
        }, {
            "featureType": "transit",
            "elementType": "all",
            "stylers": [{"visibility": "off"}]
        }, {
            "featureType": "transit.station",
            "elementType": "all",
            "stylers": [{"visibility": "off"}]
        }, {
            "featureType": "water",
            "elementType": "all",
            "stylers": [{"color": "#5880d6"}, {"visibility": "on"}, {"lightness": "-65"}, {"saturation": "-74"}, {"gamma": "10.00"}, {"weight": "1"}]
        }];
        var initialization = {
            appid: 'thebar-us',
            google_api_key: '',
            diageo_api_url: "https://api.diageoapi.com/wtb/v2/",
            locale: 'EN-US',
            defaultLat: 0,
            defaultLon: 0,
            defaultZoom: 12,
            autoZoom: true,
            autoZoomDefaultLevel: 14,
            refreshOnDrag: false,
            scrollwheel: false,
            animation: false,
            markerURL: 'dist/images/marker.png',
            markerActiveURL: 'dist/images/marker-active.png',
            showYou: true,
            youIcon: 'dist/images/marker-you.png',
            prefilledVariant: variantSlug,
            // prefilledBrand: 'crown-royal',
            prefilledZipCode: '',
            eComm: {
                enabled: true,
                idEComm: 'eComm'
            },
            marker: {
                color: 'white',
                fontFamily: 'elephantregular, serif',
                fontSize: '12px',
                showNumbers: true
            },
            phoneFormat: '###-###-####',
            showAddress: true,
            scrollToMap: false,
            locations: {
                locationsList: '#locationsList',
                loader: '#loaderLocations',
                variant: '',
                distance: 15,
                uom: 'mi',
                loctype: '',
                notFoundMessage: 'No Locations Found',
                results: '{{NUMBER}} locations found near {{LOCATION}}',
                distanceMessage: 'Distance',
                showDistance: true
            },
            infoWindow: {
                bgColor: '#fff',
                font: 'ralewayregular, sans-serif',
                fontColor: '#484848',
                directionsStyle: 'background-color: #fff; color: #fff;',
                directions: 'Get directions',
                borderRadius: '0'
            },
            //styles: styles,
            callback: wtbCallBack

        };

        $(this).wtbInitialize(initialization);

        function wtbCallBack(response) {
            $('#locationsList dl').length;
            // $('#selectVariants').trigger('change');

            $('.locationDirections a').addClass('squareBtn');
            $('#loading').css('display', 'none')
            $('#eComm a').each(function () {
                $(this).find('img').wrap("<div class='img '></div>")
            })
            $('.locationItem').each(function () {
                $(this).find('.locationNumber').remove();
                $(this).find(".locationName,.locationAddress,.locationCity").wrapAll("<div class='left'></div>");
                var _tmpTxt = $(this).find('.locationAddress').text();
                $(this).find('.locationAddress').text(_tmpTxt + ',\xa0')
            });
            if (response.ecommerce.length > 0) {
                $('.eCommBG').removeClass('hidden');
            } else {
                $('.eCommBG').addClass('hidden');
            }
            if (response.locations.length > 5) {
                $('.wtbPage .btnLoadMore').css('display', 'block');
            }
            $('#locationsFound').css('max-height', '300px');
        }


        // brand = 'crown-royal';
        if (brand == '') {
            $('#selectBrands').getBrandsFromAPI({
                loader: '#loaderBrands'
            });
        }


        $('#selectBrands').change(function () {
            console.log('Push brands');
            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
                'event': 'pushBrands'
            });

            $('#selectVariants').getVariantsFromAPI({
                loader: '#loaderVariants',
                brand: $(this).val()
            });
        });

        $('#selectVariants').change(function () {
            console.log('Push variants');
            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
                'event': 'pushVariants'
            });

            $loctypes = $('#selectLoctype').val();
            if ($loctypes.length > 1) $loctypes = '';

            $('#locationsList').readFromAPI({
                variant: $(this).val(),
                loctype: $('#selectLoctype').val(),
            });
            $('#locations').show();
            $('.map-wrapper').addClass('col-xs-12 col-sm-12 col-md-12 col-lg-10');
        });

        $('#selectVariants').getVariantsFromAPI({
            loader: '#loaderVariants',
            brand: brand
        });

        $('#searchTextField').change(function () {
            console.log('Push location');
            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
                'event': 'pushLocation'
            });
            //$('#selectVariants').trigger('change');
            //$('#locationsList').readFromAPI({loctype: ''});
        });

        $('input[name=onPrem]').on('change', function () {
            $('#locationsFound').readFromAPI({
                loctype: $('input[name=onPrem]:checked').val(),
                variant: $('#productSelect').val()
            });
        });

        // CREATE URL

        var my_condition = true;
        var lastSel = $("#selectVariants option:selected");
        // Aplies the brands to the destination object
        $('button.btn.wtb-button').on('click', function () {
            console.log('Push WTB Search');
            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
                'event': 'pushWTBSearch'
            });

            $('#selectVariants').trigger('change');
            $("#selectVariants").change(function () {
                if (my_condition) {
                    lastSel.prop("selected", true);
                }
            });

        });


    });

}